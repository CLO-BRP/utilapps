function fastMerge(inFile, outFile)
%
%   Merge all files in directory
%
%       Inputs
%           inFile: full path of files to be merged (cell vector)
%           outFile: full path of merged output file (char)

% if ispc
%     cmd = cellfun(@(x,y) sprintf(...
%         'type "%s" >> "%s"', x, y), inFile, outFileC, 'UniformOutput', false);
% else
%     cmd = cellfun(@(x,y) sprintf(...
%        'cat "%s" >> "%s"', x, y), inFile, outFileC, 'UniformOutput', false);
% end
if ispc
    fun = {'type'};
else
    fun = {'cat'};
end
fun(1:length(inFile),1) = fun;
outFileC(1:length(inFile),1) = {outFile};
cmd = cellfun(@(x,y,z) sprintf(...
    '%s "%s" >> "%s"', x, y, z), fun, inFile, outFileC, 'UniformOutput', false);
cellfun(@(x) dos(x), cmd);
