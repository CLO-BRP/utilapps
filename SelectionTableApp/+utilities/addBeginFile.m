function newC = addBeginFile(C)
%
% Description:
%   Add "Begin File" field to selection table, if it doesn't already exist
%
% Input
%   C - selection table (cell array of char vectors)
%
% Output
%   newC - selection table (cell array of char vectors)

% Initializations
import utilities.*
hasBeginFile = any(ismember(C(1, :), 'Begin File'));
hasBeginPath = any(ismember(C(1, :), 'Begin Path'));
assert(hasBeginFile || hasBeginPath, ...
    'Selection Table Must have either "Begin File" or "Begin Path" field')

% If selection table already has "Begin File" measurement, do nothing
if hasBeginFile
    newC = C;
    
% If selection does not have "Begin File" measurement, add "Begin File" measurement
else
    beginPath = get_field(C, 'Begin Path');
    if ~isempty(beginPath) % if log has no events
        [~, beginFile, ext] = cellfun(@fileparts, beginPath, 'Unif', false);
        newC = [C , ['Begin File' ; strcat(beginFile , ext)]];
    else
        newC = [C, 'Begin File'];
    end
end
