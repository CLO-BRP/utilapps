function writeCSV(C, outPathFull)
%
% Description:
%   Writes CSV file using contents of cell array
%
% Inputs:
%   C - rectangular cell array of char vectors
%   outPathFull - full path of csv file to be written (char)
%
% History
%   msp2  Aug-15-2017   Initial
%   msp2  Jun-06-2018   Change from writeTSV to writeCSV

% open output file in text write mode
fid = fopen(outPathFull,'wt');
assert(~isequal(fid,-1),'\n\nWARNING: CSV file could not be written:\n  %s\n',outPathFull)

% make format string for output
[~,n] = size(C);
f = [repmat('%s,',1,n-1),'%s\n'];

% write output file
C=C';
fprintf(fid,f,C{:,:});

% close output file
fclose(fid);