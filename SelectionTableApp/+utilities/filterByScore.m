function C2 = filterByScore(C,name,thresh)
%
% Filter selection table by score threshold
%   Input
%       C: selection table (cell array of strings)
%       name: name of annotation field with scores
%       thresh: threshold to be applied to score field
%   Output
%       C2: filtered selection table (cell array of strings)

    % Initializations
    import utilities.*
    
    % Pull score vector out of selection table.
    score = str2double(get_field(C, name));
    
    % Test for valid scores
    assert(~isempty(score),'"%s" is not a column name in selection table',name);
    badScoreIDX = isnan(score);
    if any(badScoreIDX)
        fail(sprintf('%.0f bad scores\n',sum(badScoreIDX)),'WARNING');
    end

    % Filter by score threshold
    idx = [true; score >= thresh];
    C2 = C(idx,:);
    