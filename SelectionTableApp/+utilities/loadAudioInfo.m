function info = loadAudioInfo(listfile)
%
% Retrieve audioinfo for listfile if it has already been cached.
%   Input
%       listfile: Raven listfile (cell vector)
%   Output
%       info = audioinfo for all listfiles cached (struct)

% Initializations
info = {};
infoC = {};
listfileC = {};

% create metadata folder if it does not already exist
p = fullfile(fileparts(mfilename('fullpath')),'cache');
funp = which('isfolder');
if ~isempty(funp)
    if ~isfolder(p)
        mkdir(p)
        return;
    end
else
    if ~exist(p,'dir')
        mkdir(p)
        return;
    end
end

% stop execution if no cache
funp = which('isfile');
if ~isempty(funp)
    if ~isfile(fullfile(p,'cache.mat'))
        return;
    end
else
    if ~exist(fullfile(p,'cache.mat'),'file')
        return;
    end
end

% Retrieve cached audioinfo
load(fullfile(p,'cache.mat'), 'infoC', 'listfileC');
i = 1;
while i <= length(listfileC) && ~isequal(listfileC{i},listfile)
    i = i + 1;
end
if i <= length(listfileC)
    info = infoC{i};
end
