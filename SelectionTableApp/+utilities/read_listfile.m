function [fnSound,cumDur] = read_listfile(fn_full)
%
% Report cumulative duration of sound fiels in Raven listfile
%   Input
%       fn_full: full path to Raven listfile (string)
%   Output
%       fnSound: names of sound files in listfile (nx1 cell array of strings)
%       cumDur: cumulative duration (s) of sound files in listfile (nx1 double)

    % Import utilities package
    import utilities.*

    % Read listfile
    fid = fopen(fn_full,'rt');
    assert(~isequal(fid, -1), 'Listfile could not be opened:\n   %s', fn_full);
    C = textscan(fid,'%s','Delimiter','\n');
    fclose(fid);
    listfile=C{1};
    
    %delete extra columns,
    listfile = listfile(:, 1);
    
    %delete extra rows
    idx = find(cellfun(@isempty, listfile), 1);
    if ~isempty(idx)
        if isequal(idx, 1)
            listfile = {};
        else
            listfile = listfile(1:idx-1, 1);
        end
    end
    
    % Test that sound files exist
    file2(1:length(listfile),1) = {'file'};
    assert(all(cellfun(@exist, listfile, file2)), ...
        'Not all sound files in listfile exist on path specified.')

    % Test if audio info has already been cached for this listfile
    info = loadAudioInfo(listfile);

    % If no matching audioinfo cache, find and cache audioinfo for this listfile
    if isempty(info)
        info = cellfun(@audioinfo, listfile); %%%%% TAKES A LONG TIME!!! %%%%%%
        saveAudioInfo(info, listfile)
    end

    % Find cumulate duration of sound files in listfile
    Fs = [info.SampleRate];
    assert(all(Fs == Fs(1)), 'Sample rate of sound files must be the same')
    nbits = [info.BitsPerSample];
    assert(all(nbits == nbits(1)), 'Bit depth of sound files must be the same')
    nChannels = [info.NumChannels];
    assert(all(nChannels == nChannels(1)), 'Number of channels in sound files must be the same')
    sizeinfo = [info.TotalSamples];
    cumDur = cumsum(sizeinfo) ./ Fs;
    
    % Make vector of sound file names
    [~, fnSound, ext] = cellfun(@fileparts, listfile, 'UniformOutput', false);
    fnSound = strcat(fnSound, ext);
    