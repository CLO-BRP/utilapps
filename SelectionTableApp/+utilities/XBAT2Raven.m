function XBAT2Raven(inFilesFull, outpath)
% Export XBAT logs as Raven selection atbles
%   Input
%       inFilesFull: full path of XBAT logs to be exported (cell vector)
%       outpath: output path for exported logs

% Initializations
import utilities.*

% For each log
for i = 1:length(inFilesFull)
    
    % Read log
    log = load(inFilesFull{i});
    assert(isstruct(log) && length(fields(log))==1, ...
        sprintf('File is not an XBAT log:\n   %s', inFilesFull{i}))
    fieldName = fields(log);
    log = log.(fieldName{1});
    assert(isfield(log,'type') && strcmp(log.type,'log'), ...
        sprintf('File is not an XBAT log:\n   %s', inFilesFull{i}))
    
    
    id = strtrim(cellstr(num2str([log.event(:).id]')));
    t = [log.event(:).time]';
    beginTime = strtrim(cellstr(num2str(t(1:2:end),'%.6f')));
    endTime = strtrim(cellstr(num2str(t(2:2:end),'%.6f')));
    freq = [log.event(:).freq]';
    lowFreq = strtrim(cellstr(num2str(freq(1:2:end),'%.3f')));
    hiFreq = strtrim(cellstr(num2str(freq(2:2:end),'%.3f')));
    chan = cellstr(num2str([log.event(:).channel]'));
    
    % Deal with fields that might be empty
    score = cell(log.length,1);
    tagsC = cell(log.length,1);
    notesC = cell(log.length,1);
    for j = 1:log.length
        score(j) = strtrim(cellstr(num2str([log.event(j).score]','%.6f')));   
        tags = log.event(j).tags;
        if isempty(tags)
            tagsC(j) = {''};
        elseif ischar(tags)
            tagsC(j) = {tags};
        elseif iscell(tags)
            tagsC(j) = tags;
        end
        notes = log.event(j).notes;
        if isempty(notes)
            notesC(j) = {''};
        elseif ischar(notes)
            notesC(j) = {notes};
        elseif iscell(notes)
            notesC(j) = notes;
        end
    end
    
    %---
    % Calculate Begin Path and File Offset
    %---
    fpath = fullfile(log.sound.path,log.sound.file);
    cumDuration = log.sound.cumulative ./ log.sound.samplerate;
    if length(cumDuration > 1)
        cumDuration = [0 ; cumDuration(1:end-1)];
    else
        cumDuration = 0;
    end
    t1 = t(1:2:end);
% %     beginPath = cell(log.length,1);
% %     fileOffset = cell(log.length,1);
    idx = zeros(log.length,1);
    for j = 1:log.length
        idx(j) = find(t1(j) >= cumDuration,1,'last');
% %         beginPath(j) = fpath(idx);
% %         fileOffset(j) = {num2str(t1(j) - cumDuration(idx),'%.6f')};
    end
    beginPath = fpath(idx);
    fileOffset = strtrim(cellstr(num2str(t1 - cumDuration(idx),'%.6f')));
        
    % Export as Raven selection table
    headers = { ...
        'Selection', ...
        'View', ...
        'Channel', ...
        'Begin Time (s)', ...
        'End Time (s)', ...
        'Low Freq (Hz)', ...
        'High Freq (Hz)', ...
        'Begin Path', ...
        'File Offset (s)', ...
        'Score', ...
        'Tags', ...
        'Notes', ...
    };
    view = {'Spectrogram 1'};
    view(1:log.length) = view;
    if isequal(log.length,0)
        C = {};
    else
        C = [ ...
            id, ...
            view', ...
            chan, ...
            beginTime, ...
            endTime, ...
            lowFreq, ...
            hiFreq, ...
            beginPath, ...
            fileOffset, ...
            score, ...
            tagsC, ...
            notesC, ...
        ];
    end
    C = [headers ; C];
    [~,inFile] = fileparts(inFilesFull{i});
    outFileFull = fullfile(outpath,[inFile, '.selections.txt']);
    write_selection_table(C,outFileFull)
end
