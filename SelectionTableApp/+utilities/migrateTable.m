function C2 = migrateTable(C,fnSound,cumDur)
%
% Migrate Raven selection table by updating Begin Time and End Time 
%   to make compatible with sound file stream.
%
% Inputs
%   C: selection table (cell array of strings)
%   fnSound: names of sound files in listfile (nx1 cell array of strings)
%   cumDur: cumulative duration of sound files in stream (numeric vector)
%
% Output
%   C2: migrated selection table (cell array of strings)

%---
% Initializations
%---
import utilities.*
headers = C(1,:);
body = C(2:end,:);
m = size(body,1);
if isequal(m,0)
    C2 = C;
    return;
end
selection = get_field(C,'Selection');
[begin_time,begin_time_idx] = get_field(C,'Begin Time (s)');
begin_time = str2double(begin_time);
[end_time,end_time_idx] = get_field(C,'End Time (s)');
end_time = str2double(end_time);
C = addBeginFile(C);
begin_file = get_field(C,'Begin File');
[file_offset,file_offset_idx] = get_field(C,'File Offset (s)');
file_offset = str2double(file_offset);

%---
% Test that required fields are present
%---
assert(~isempty(selection),'ERROR: Not a selection table')
assert(~isempty(begin_time),'ERROR: No "Begin Time" measurement')
assert(~any(isnan(begin_time)),'ERROR: "Begin Time" field has some invalid values')
assert(~isempty(end_time),'ERROR: No "End Time" measurement')
assert(~any(isnan(end_time)),'ERROR: "End Time" field has some invalid values')
assert(~isempty(begin_file),'ERROR: No "Begin File" measurement')
assert(~isempty(file_offset),'ERROR: No "File Offset" measurement')
assert(~any(isnan(file_offset)),'ERROR: "File Offset" field has some invalid values')

%---
% Test that there are no duplicate file names in selection table
%---
assert(size(fnSound,1) == size(unique(fnSound),1),'ERROR: Listfile contains duplicate file names')

%---
% Set negative file offset to 0 file offset
%---
if any(file_offset < 0)
    file_offset(file_offset < 0) = 0;
    body(:,file_offset_idx) = cellstr(num2str(file_offset,'%.6f'));
end

%---
% Find number of seconds of recording before each file in stream
%---
if length( cumDur ) > 1
    time_before = [0, cumDur(1:end-1)];
else
    time_before = 0;
end

%---
% Calculate new "Begin Time" and "End Time" for each selection
%--
new_begin_time = zeros( m, 1 );
new_end_time = new_begin_time;
j = 0;
skipped_lines = [];
for i = 1:m
   
    % find begin file in sound file stream
    idx = strcmp( fnSound, begin_file{ i } );
    
    % if no matching sound file found, skip selection
    if ~any( idx )
        skipped_lines = [ skipped_lines ; i ];
        fprintf(2,'WARNING:\nEvent deleted because Begin File not found in listfile:\n  %s\n\n',...
            begin_file{ i });
        continue;
    end
    
    % find number of seconds in file stream before begin file
    file_time = time_before( idx );
    
    % calculate new Begin Time
    curr_begin_time = file_time + file_offset( i );
    curr_end_time = curr_begin_time + diff( [ begin_time( i ), end_time( i ) ] );
    
    % put times in vector
    j = j + 1;
    
try
    new_begin_time( j ) = curr_begin_time;
catch ME
    keyboard;
end

    new_end_time( j ) = curr_end_time;    
end

% trim skipped lines from "Begin Time" and "End Time" vectors
new_begin_time = new_begin_time( 1:j );
new_end_time = new_end_time( 1:j );

%---
% If any lines weren't skipped, replace "Begin Time" and "End Time" columns in table
%---
if ~isempty( new_begin_time )
    
    % If any lines were skipped, delete them from output table
    if ~isempty( skipped_lines )
        body( skipped_lines, : ) = [];
    end
    
    % Update "Begin Time" and "End Time"
    new_begin_time_str = num2str( new_begin_time, '%.6f' );
    new_begin_time_cell = strtrim(cellstr( new_begin_time_str ));
    body( :, begin_time_idx ) = new_begin_time_cell;
    new_end_time_str = num2str( new_end_time, '%.6f' );
    new_end_time_cell = strtrim(cellstr( new_end_time_str ));
    body( :, end_time_idx ) = new_end_time_cell;
    
    % Sort by Begin Time
    [~, sortIDX] = sort(new_begin_time);
    body = body(sortIDX, :);

%---
% If all lines were skipped, table is empty
%---
else
    body = {};
end

%---
% Prepend headers to body of migrated selection table
%---
C2 = [headers ; body];
