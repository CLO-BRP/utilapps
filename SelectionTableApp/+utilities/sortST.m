function sortST(inFiles,stdViews)
% Sort Selection by BeginTime
%   Input
%       inFiles - selection tables to be sorted (cell vector of strings)
%       stdViews - views found in first selection table read

import utilities.*

for i = 1:length(inFiles)
    fn = inFiles{i};
    C = read_selection_table(fn,{},false);
    
    % sort by Begin TIme
    beginTime = str2double(get_field(C,'Begin Time (s)'));
    [~,idx] = sort(beginTime);
    idx = [1 ; idx+1];
    C = C(idx,:);
    
    C = renumberSelectionTable(C,1,stdViews);
    write_selection_table(C,fn);
end
