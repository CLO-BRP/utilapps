function C2 = filterByDate(C,sched,chan)
%
% Filter selection table by date
%   Input
%       C: selection table (cell array of strings)
%       sched: table of date-times for filtering (nx4 double)
%       chan: vector of channels (nx1 double)
%   Output
%       C2: filtered selection table (cell array of strings)

    % Initializations
    import utilities.*
    filterByChans = ~isempty(chan);
        
    %---
    % Test that required fields are present
    %---
    headers = C(1, :);
    body = C(2:end, :);
    begin_file_idx = strcmp( headers, 'Begin File' );
    begin_path_idx = strcmp( headers, 'Begin Path' );
    assert(any(begin_file_idx) || any(begin_path_idx), ...
        'No "Begin File" or "Begin Path" field present')

    file_offset_idx = strcmp( headers, 'File Offset (s)' );
    assert(any(file_offset_idx), 'No "File Offset" field present')

    file_offset_str = body( :, file_offset_idx );
    file_offset = str2double( file_offset_str );
    assert(~isempty(file_offset) && all(~isnan(file_offset)), ...
        'Bad File Offset values')
    
    if filterByChans
        channel_idx = strcmp( headers, 'Channel');
        assert(any(channel_idx),'No "Channel" field preseent')
        channel_str = body( :, channel_idx);
        channel = str2double(channel_str);
        assert(~isempty(channel) && all(~isnan(channel)), ...
            'Bad File Offset values')
    end
        
    %---
    % Calculate date and time of event based on Begin File and File Offset
    %---
    C = addBeginFile(C);
    beginDatetime = calcRealClockTime(C);        

    %---
    % Filter events within schedule
    %---
    filteredBody = cell(size(body));

    start = 1;
    stop = 0;
    for i = 1:size(sched, 1)
        currRange = sched(i, 1:2);
        idx = beginDatetime >= currRange(1) & beginDatetime < currRange(2);
        if filterByChans
            currChan = chan(i);
            idx = idx & channel == currChan;
        end
        numEvents = sum(idx);
        if numEvents > 0
            stop = start + numEvents - 1;
            filteredBody(start:stop, :) = body(idx, :);
            start = stop + 1;
        end
    end

    if stop > 0 % there is at least one filtered event
        filteredBody  = filteredBody (1:stop, :);
        C2 = [headers ; filteredBody];
    else % there are no filtered events
        C2 = headers;
    end
    