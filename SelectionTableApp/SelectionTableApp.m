function varargout = SelectionTableApp(varargin)
% selectionTableApp MATLAB code for selectionTableApp.fig
%      selectionTableApp, by itself, creates a new selectionTableApp or raises the existing
%      singleton*.
%
%      H = selectionTableApp returns the handle to a new selectionTableApp or the handle to
%      the existing singleton*.
%
%      selectionTableApp('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in selectionTableApp.M with the given input arguments.
%
%      selectionTableApp('Property','Value',...) creates a new selectionTableApp or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SelectionTableApp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SelectionTableApp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help selectionTableApp

% Last Modified by GUIDE v2.5 12-Dec-2018 09:47:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SelectionTableApp_OpeningFcn, ...
                   'gui_OutputFcn',  @SelectionTableApp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before selectionTableApp is made visible.
function SelectionTableApp_OpeningFcn(hObject, eventdata, handles, varargin)
try
    
    % Set version number in GUI
    handles.version.String = 'version 1.4';    
    
    % Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))

    % Choose default command line output for selectionTableApp
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    % Load icon into UI
% %     set_start_in_dir
    handles.selectionTableApp.CurrentAxes = handles.icon;
    set_start_in_dir;
    icon = imread('splash.png');
    imshow(icon);
% %     ih.YData = ih.XData;
% %     handles.icon.YLim = handles.icon.XLim;

    % Disable default TEX interpreter for text objects
    set(0,'defaulttextInterpreter','none')

    % UIWAIT makes selectionTableApp wait for user response (see UIRESUME)
    % uiwait(handles.selectionTableApp);
catch err
    reportErr(err)
end


% --- Executes when user attempts to close selectionTableApp.
function selectionTableApp_CloseRequestFcn(hObject, eventdata, handles)

    %Delete GUI
    delete(hObject);
    
    %Remove application root directory from search paths
    rmpath(fileparts(mfilename('fullpath')))


% --- Outputs from this function are returned to the command line.
function varargout = SelectionTableApp_OutputFcn(hObject, eventdata, handles)

    %Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))

    % Get default command line output from handles structure
    varargout{1} = handles.output;


% ----
function helpMenu_Callback(hObject, eventdata, handles)

    % Display help document
    open(fullfile(fileparts(mfilename('fullpath')), '+utilities', 'help.html'))

% ---
function inFiles_ButtonDownFcn(hObject, eventdata, handles)

    % Import Utilities
    import utilities.*

    %--
    % Find current path for input selection tables
    %--
    inFilesPath = char(handles.inFiles.UserData);
    if ~isdir(inFilesPath)
        inFilesPath = pwd;
    end
    
    %---
    % Set file type
    %---
    inputType = cellstr(handles.inputType.String);
    inputType = inputType{handles.inputType.Value};
    switch inputType
        case 'Raven Selection Tables'
            ext = 'txt';
        case 'XBAT Logs'
            ext = 'mat';
    end
    
    % Select root of file tree with selection tables
    if handles.fileTree.Value
        inFilesPath = uigetdir(inFilesPath, 'Select root of file tree with selection tables');
        if inFilesPath == 0
            return;
        end
        h = waitbar(0, 'Finding files in tree.');
        inFilesFull = fastDir(inFilesPath, 'r', ext);
        [inFilesPath2,inFiles, ext] = cellfun(@fileparts, inFilesFull, 'UniformOutput', false);
        inFiles = strcat(inFiles, ext);
        delete(h)
        handles.inFiles.UserData = inFilesFull;
        inFilesPath2 = inFilesPath2{1};
        
    % Select invidual selection tables in a folder
    else

        %--
        % Select selection tables for input
        %--
        filterSpect = fullfile(inFilesPath, sprintf('*.%s',ext));
        [inFiles, inFilesPath2] = uigetfile( ...
            filterSpect, ...
            'Select selection tables for input', ...
            'Multiselect', 'on');
        if isequal(inFiles, 0)
            return;
        end
        handles.inFiles.UserData = inFilesPath2;
    end
    if ~iscell(inFiles)
        inFiles = {inFiles};
    end
    handles.inFiles.String = inFiles;
    
    % Populate "Score Name" and "Annotation Name" pop up menus
    firstInfileFull = fullfile(inFilesPath2,inFiles{1});
    fid = fopen(firstInfileFull,'rt');
    assert(~isequal(fid,-1), 'Selection table could not be read:\n  %s\n',inFiles{1});
    C = textscan(fid,'%s',1,'Delimiter','\n');
    fclose(fid);
    header = strsplit(C{1}{1},'\t');
    handles.scoreNameEdit.String = header;
    handles.filterNameEdit.String = header;


% ---
function listfile_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current handbrowsed schedule file
    %--
    listfileFull = char(handles.listfile.UserData);
    if ~exist(listfileFull, 'file')
        listfileFull = fullfile(pwd, 'HandbrowseSchedule.xlsx');
    end

    %--
    % Select new handbrowsed schedule file
    %--
    filterSpec =  '*.txt';
    title = 'Select listfile for input';
    [listfileFile, inPath] = uigetfile(filterSpec, title, listfileFull);
    if isequal(listfileFile, 0)
        return;
    end
    handles.listfile.String = listfileFile;
    listfileFull = fullfile(inPath, listfileFile);
    handles.listfile.UserData = listfileFull;
    handles.listfileFileTitle.TooltipString = listfileFull;
    
    
% --- Executes on button press in fileTree.
function fileTree_Callback(hObject, eventdata, handles)

    % Clear "Input Selection Tables" field
    handles.inFiles.String = {};


% --- Executes on selection change in inputType.
function inputType_Callback(hObject, eventdata, handles)

    % Clear "Input Selection Tables" field
    handles.inFiles.String = {};

    % Make "Input Listfile" visible or invisible, depending on input type
    inputType = cellstr(get(hObject,'String'));
    inputType = inputType{get(hObject,'Value')};
    switch inputType
        case 'Raven Selection Tables'
            handles.listfilePanel.Visible = 'on';
            handles.mergeEnable.Visible = 'on';
            handles.filterNameEdit.String = '';
            handles.filterValueEdit.String = '';
            handles.filterValueEditLabel.String = 'Annotation Value';
            if handles.filterEnable.Value
                handles.filterNameEdit.Visible = 'on';
                handles.filterNameEditLabel.Visible = 'on';
            end
        case 'XBAT Logs'
            handles.listfilePanel.Visible = 'off';
            handles.mergeEnable.Visible = 'off';
            handles.filterNameEdit.String = 'Tags';
            handles.filterValueEdit.String = '';
            handles.filterValueEditLabel.String = 'Tag';
            if handles.filterEnable.Value
                handles.filterNameEdit.Visible = 'off';
                handles.filterNameEditLabel.Visible = 'off';
            end
    end
    handles.listfile.String = '';
    handles.listfile.UserData = {};

% --- Executes during object creation, after setting all properties.
function inputType_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


function filterNameEdit_Callback(hObject, eventdata, handles)

    
% --- Executes during object creation, after setting all properties.
function filterNameEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on button press in filterEnable.
function filterEnable_Callback(hObject, eventdata, handles)
    
    % Toggle filter controls visible/invisible depending on checkbox value
	if hObject.Value
        inputType = cellstr(handles.inputType.String);
        inputType = inputType{handles.inputType.Value};
        switch inputType
            case 'Raven Selection Tables'
                handles.filterNameEdit.Visible = 'On';
                handles.filterNameEditLabel.Visible = 'On';
            case 'XBAT Logs'
                handles.filterNameEdit.Visible = 'Off';
                handles.filterNameEditLabel.Visible = 'Off';
        end
        handles.filterValueEdit.Visible = 'On';
        handles.filterValueEditLabel.Visible = 'On';        
        handles.filterButton.Visible = 'On';
	else
        handles.filterNameEdit.Visible = 'Off';
        handles.filterNameEditLabel.Visible = 'Off';
        handles.filterValueEdit.Visible = 'Off';
        handles.filterValueEditLabel.Visible = 'Off';     
        handles.filterButton.Visible = 'Off';
	end


% --- Executes on button press in filterButton.
function filterButton_Callback(hObject, eventdata, handles)
    
    % Find annotation fields present in all selected input logs
    
    % Replace edit boxes with dropdown menus
    
    fail('Feature not yet active.')
    

function filterValueEdit_Callback(hObject, eventdata, handles)

    % Do nothing
    

% --- Executes during object creation, after setting all properties.
function filterValueEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- 
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path
    %--
    outpath = char(handles.outpath.UserData);
    if ~isdir(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    handles.outpath.UserData = outpath;
    handles.outpathTitle.TooltipString = outpath;
    [~, outpathDisplay] = fileparts(outpath);
    handles.outpath.String = outpathDisplay;
    

% ---
function migrateEnable_Callback(hObject, eventdata, handles)

    % Toggle visibility of "Input Listfile" field
    if handles.migrateEnable.Value
        handles.listfile.Visible = 'On';
        if ~exist(handles.listfile.UserData,'file')
            listfile_ButtonDownFcn([], [], handles)
            if ~exist(handles.listfile.UserData,'file')
                handles.listfile.Visible = 'Off';
                handles.migrateEnable.Value = 0;
            end
        end
    else
        handles.listfile.Visible = 'Off';
    end


% ---
function maxLogEnable_Callback(hObject, eventdata, handles)

    % Toggle "Maximum Log Size" field (enable/disable)
    if handles.maxLogEnable.Value
        handles.maxLogSize.Enable = 'On';
    else
        handles.maxLogSize.Enable = 'Off';
    end
    

% --- 
function maxLogSize_Callback(hObject, eventdata, handles)

    % Make sure "Maximum Log Size" is set to an integer
	maxLogSize = ceil(str2double(hObject.String));
    if isnan(maxLogSize) || maxLogSize < 1
        maxLogSize = 10000;
    end
    hObject.String = sprintf('%.0f', round(maxLogSize));
    

% --- 
function reportErr(err,p,data)
% Make MATLAB error reporting more readible for non-programmers
%   Inputs
%     err: MATLAB error reporting object
%     p: output path for data processing
%     data: optional data

    import utilities.*

    if isa(err,'MException')
    	txt = sprintf('%s\n%s\n',err.identifier,err.message);
        if exist('data','var')
            txt = sprintf('%s\nSelection Table:\n   %s\n',txt,data);
        end
        txt = sprintf('%s\n----------------------\nSTACK:\n',txt);
        C = struct2cell(err.stack);
        C2 = strcat(C(2,:)',{' (line '}, cellstr(num2str(cell2mat(C(3,:))')),')');
        txt = sprintf('%s\n%s\n',txt,sprintf('   %s\n',C2{:}));        
    elseif ischar(err)
        txt = err;
    end    
    fprintf(2,'%s',txt);
    warndlg(txt, 'ERROR', 'modal')
    
    % Delete any output files that have been written already
    if exist('p','var')
        d = fastDir(p);
        cellfun(@delete,d)
    end
    
    % Delete all waitbars
    delete(findall(0,'Tag','TMWWaitbar'))

% --- 
function set_start_in_dir

    try
        if isdeployed && ispc
            % User is running an executable in standalone mode. 
            [status, result] = dos('set PATH');
            executableFolder = char(regexpi(result, 'Path=(.*?);', 'tokens', 'once'));
            cd(executableFolder)
        end
    catch ME
        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
            ME.stack(1).name, ME.stack(1).line, ME.message);
        uiwait(warndlg(errorMessage));
    end


function scoreNameEdit_Callback(hObject, eventdata, handles)


function scoreThresholdEnable_Callback(hObject, eventdata, handles)
    
    % Toggle filter controls visible/invisible depending on checkbox value
	if hObject.Value
        handles.scoreNameEdit.Visible = 'On';  
        handles.scoreNameEditLabel.Visible = 'On';
        handles.scoreThresholdEdit.Visible = 'On';  
        handles.scoreThresholdEditLabel.Visible = 'On'; 
        handles.scoreFieldButton.Visible = 'On';
	else
        handles.scoreNameEdit.Visible = 'Off';  
        handles.scoreNameEditLabel.Visible = 'Off';
        handles.scoreThresholdEdit.Visible = 'Off';  
        handles.scoreThresholdEditLabel.Visible = 'Off'; 
        handles.scoreFieldButton.Visible = 'Off';
	end


function scoreFieldButton_Callback(hObject, eventdata, handles)


function scoreThresholdEdit_Callback(hObject, eventdata, handles)

    % test that threshold is a number
    scoreStr = hObject.String;
    try
        score = str2double(scoreStr);
        if isnan(score) || isinf(score)
            score = [];
        end
        scoreStr = num2str(score);
        hObject.String = scoreStr;
    catch
        hObject.String = '';
    end


function dateFilterEnable_Callback(hObject, eventdata, handles)

    % Toggle visibility of "Filter by Date" field
    if handles.dateFilterEnable.Value
        handles.dateFilterSchedule.Visible = 'On';
        if ~exist(handles.dateFilterSchedule.UserData,'file')
            dateFilterSchedule_ButtonDownFcn([], [], handles)
            if ~exist(handles.dateFilterSchedule.UserData,'file')
                handles.dateFilterSchedule.Visible = 'Off';
                handles.dateFilterEnable.Value = 0;
            end
        end
    else
        handles.dateFilterSchedule.Visible = 'Off';
    end


function dateFilterSchedule_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current date schedule file
    %--
    dateFilterScheduleFull = char(handles.dateFilterSchedule.UserData);
    if ~exist(dateFilterScheduleFull, 'file')
        dateFilterScheduleFull = fullfile(pwd, 'HandbrowseSchedule.xlsx');
    end

    %--
    % Select new date schedule file
    %--
    filterSpec =  '*.xlsx';
    title = 'Select Excel worksheet with date filter schedule';
    [dateFilterScheduleFile, inPath] = uigetfile(filterSpec, title, dateFilterScheduleFull);
    if isequal(dateFilterScheduleFile, 0)
        return;
    end
    handles.dateFilterSchedule.String = dateFilterScheduleFile;
    dateFilterScheduleFull = fullfile(inPath, dateFilterScheduleFile);
    handles.dateFilterSchedule.UserData = dateFilterScheduleFull;
    handles.dateFilterScheduleFileTitle.TooltipString = dateFilterScheduleFull;
    

% --- 
function runButton_Callback(hObject, eventdata, handles)

disp('TO DO')
disp('1. Consider multithreading the filter loop')
disp('2. Allow different column orders.')
disp('3. Rename output log names to affect how they were processed.')

% Initializations
tic
s = 0;
import utilities.*
kk = 86400;
stdViews = '';
fprintf('\n\n');

%Set up waitbar
h = waitbar(0, 'Reading user input');
set(h,'Units','characters')
pos = get(h,'Position');
pos(3) = 120;
set(h,'Position',pos)
ah = get(h,'Children');
set(ah,'Units','characters')
aPos = get(ah,'Position');
aPos(3) = 120;
set(ah,'Position',aPos)    

% User input
fileTree = handles.fileTree.Value;
inputType = cellstr(handles.inputType.String);
inputType = inputType{handles.inputType.Value};
listfileFull = char(handles.listfile.UserData);
outpath = char(handles.outpath.UserData);
migrateEnable = handles.migrateEnable.Value;
mergeEnable = handles.mergeEnable.Value;
maxLogEnable = handles.maxLogEnable.Value;
maxLogSize = str2double(handles.maxLogSize.String);
filterEnable = handles.filterEnable.Value;
if filterEnable
    filterName = handles.filterNameEdit.String{handles.filterNameEdit.Value};
    filterValue = handles.filterValueEdit.String;
end
dateFilterScheduleFull = char(handles.dateFilterSchedule.UserData);
dateFilterEnable = handles.dateFilterEnable.Value;
scoreEnable = handles.scoreThresholdEnable.Value;
if scoreEnable
    scoreName = handles.scoreNameEdit.String{handles.scoreNameEdit.Value};
    scoreThreshold = str2double(handles.scoreThresholdEdit.String);
else
    scoreName = '';
end
sortEnable = handles.sortEnable.Value;

try    
    if fileTree
        inFilesFull = handles.inFiles.UserData;
    else
        inFiles = handles.inFiles.String;
        inFilesPath = char(handles.inFiles.UserData);
        inFilesFull = fullfile(inFilesPath, inFiles);
    end
    
    %---
    % Test for valid inputs
    %---
    if isempty(inFilesFull)
        delete(h)
        fail('Please select one or more input files.');
        return;
    end
    inFilesFull = cellstr(inFilesFull);
    
    % Stop execution if no input files are selected
    if isempty(inFilesFull)
        delete(h);
        fail('Please select input logs and try again.')
        return;
    end
    
    % Stop execution if outpath path does not exist
    if ~isfolder(outpath)
        delete(h)
        fail(sprintf('Output path no longer exists:\n  %s',outpath));
        return;
    end
    
    % Stop execution if full path of input path is too long
    if ispc && any(cellfun(@length,inFilesFull)>259)
        delete(h);
        fail(sprintf(...
          ['Maximum path length for Windows 259 characters.\n'...
           'You have selection tables with path lengths up to %.0f characters.'],...
           max(cellfun(@length,inFilesFull))))
        return;
    end
    
    % Stop execution if full path of output path is too long
    [~,inFiles,ext] = cellfun(@fileparts,inFilesFull,'UniformOutput',false);
    if ispc && max(cellfun(@length,fullfile(outpath,strcat(inFiles,ext))))>259
        delete(h);
        fail(sprintf(...
          ['Maximum path length for Windows 259 characters.\n'...
           'Your selections will be output to paths up to %.0f characters long.'],...
           max(cellfun(@length,fullfile(outpath,strcat(inFiles,ext))))))
        return;
    end
    
    % Stop execution if no output path is selected
    if isempty(outpath)
        delete(h);
        fail('Please select output folder and try again.')
        return;
    end
    
    % Read date filter schedule, stop execution if none specified
    if dateFilterEnable
        if ~exist(dateFilterScheduleFull,'file')
            t = ['No valid filter schedule file specified. Please disable ', ...
                '"Filter by Date" or select a valid filter schedule file'];
            delete(h);
            fail(t)
            return;
        end
        % % [~, ~, C] = xlsread(truthSchedFullfile, '', '', 'basic');
        [~, ~, C] = xlsread(dateFilterScheduleFull);
        [sched,chan] = readSched(C);
    end
    
    % Stop execution if score filtering enabled but not score field and/or
    % threshold specified
    if scoreEnable
       if isempty(scoreName)
            t = ['No Score Field specified. Please disable ', ...
            '"Score Threshold" or type in a score field name.'];
            delete(h);
            fail(t)
            return;
       end
       if isnan(scoreThreshold)
            t = ['No Score Field specified. Please disable ', ...
            '"Score Threshold" or type in a Threshold.'];
            delete(h);
            fail(t)
            return;
       end
    end

    % If output directory has files in it, invite user to delete files or change output directory
    d = fastDir(outpath);
    if ~isempty(d)
        fail('Delete files in output folder or change output folder.')
        delete(h);
        return;
    end
    
    % If Raven selection table output specified
    switch inputType
        case 'Raven Selection Tables'
            
            % Stop execution if "Migrate to Listfile" enabled but no listfile selected
            if migrateEnable && isempty(listfileFull)
                fail('Please disable "Migrate to Listfile" or select a listfile, and try again')
                delete(h)
                return;
            end

            % Warn user if "Merge Output" is enabled but not "Migrate to Listfile"
            if mergeEnable && ~migrateEnable
                h.Visible = 'off';
                t = sprintf(['"Merge Output" is enabled but not "Migrate to Listfile".', ...
                    '\n', ...
                    'This could result in inappropriate Begin Time and End Time for selections.', ...
                    '\n\n', ...
                    'Proceed anway?\n']);
                answer = questdlg(t,'WARNING','Yes','No','No');
                if strcmp(answer,'No')
                    delete(h);
                    return;
                end
                h.Visible = 'on';
            end
            
            % test that header lines are the same in all selection tables
            if mergeEnable
                testHeaders(inFilesFull)
                fprintf('%s: testHeaders\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
            end
            
            % copy input selection tables to output directory
            waitbar(0.03, h, 'Copying selection tables to output folder.')
            copyfile2(inFilesFull, outpath)
            fprintf('%s: copy selection tables\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
            
            % read listfile
            if migrateEnable
                waitbar(0.06, h, sprintf('Reading listfile'))
                [fnSound,cumDur] = read_listfile(listfileFull);
                fprintf('%s: read_listfile\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
            end
            
        % If XBAT log input, export logs to output directory
        case 'XBAT Logs'
            waitbar(0.03, h, 'Exporting XBAT logs as Raven selection tables.')
            XBAT2Raven(inFilesFull, outpath)
            fprintf('%s: XBAT2Raven\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
    end
catch ME
    reportErr(ME,outpath)
    return;
end

%---
% Filter events in each output selection table
%---
waitbar(0.1, h, sprintf('Filtering selection tables'))
inFilesFull = fastDir(outpath,false,'txt');
numST = length(inFilesFull);
numEvents = zeros(numST,5);
deleteIDX = false(numST,1);
startID = 1;
j = 0.6/numST;
last_x = 0;
for i = 1:numST
    try
        currFN = inFilesFull{i};
        [~,fn,ext] = fileparts(currFN);
        fn = [fn,ext];
        
        % increment waitbar
        x = round(100*i/numST);
        if ~isequal(x,last_x)
            waitbar(0.1+j*(i-1), h, sprintf('Filtering:\n  %s',fn))
            last_x = x;
        end

        % read selection table
        [C,stdViews] = read_selection_table(currFN,stdViews,mergeEnable);
        N = size(C,1) - 1;
        numEvents(i,1) = N;

        % Filter output by score
        if scoreEnable && N > 0
            C = filterByScore(C,scoreName,scoreThreshold);
            N = size(C,1) - 1;
            numEvents(i,2) = N;
        end

        % Filter output by date
        if dateFilterEnable && N > 0
            C = filterByDate(C,sched,chan);
            N = size(C,1) - 1;
            numEvents(i,3) = N;
        end

        % Filter output by annotation
        if filterEnable && N > 0
            C = filterByAnnotation(C,filterName,filterValue);
            N = size(C,1) - 1;
            numEvents(i,4) = N;
        end

        % Migrate output selection tables to new sound file stream, if requested
        if migrateEnable && N > 0
            try
                C = migrateTable(C,fnSound,cumDur);
                N = size(C,1) - 1;
                numEvents(i,5) = N;
            catch ME
                reportErr(ME,outpath,fn)
                return;
            end
        end

        % Renumber selection table
        if mergeEnable && ~sortEnable && numST > 1 && N > 0
            [C,startID] = renumberSelectionTable(C,startID,stdViews);
        end

        % Write filtered selection table
        if mergeEnable
            if isequal(i,1)
                write_selection_table(C,currFN);
            elseif N > 0
                write_selection_table(C(2:end,:),currFN);
            else
                deleteIDX(i) = true;
            end
        else
            if (scoreEnable||dateFilterEnable||filterEnable||migrateEnable)
                write_selection_table(C,currFN);
            end
        end
    catch ME
        reportErr(ME,outpath,fn)
        return;
    end
end
fprintf('%s: event filters\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
    
try
    
    % Delete selection tables with no events
    cellfun(@delete,inFilesFull(deleteIDX))
    fprintf('%s: delete ST with no events\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
    
    % List files to be processed
    inFilesFull2 = fastDir(outpath,false,'txt');
    
    % Merge selection tables, if requested
    if mergeEnable && numST > 1
        waitbar(0.8, h, 'Merging')
        outfileName = 'merged.selections.txt';
        outfilesFull = fullfile(outpath, outfileName);
        fastMerge(inFilesFull2, outfilesFull);
        cellfun(@delete,inFilesFull2)
        outfilesFull = {outfilesFull};
        fprintf('%s: fastMerge\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
    else
        outfilesFull = inFilesFull2;
    end
    
    if sortEnable
        waitbar(0.85, h, 'Sorting')
        inFilesFull3 = fastDir(outpath,false,'txt');
        sortST(inFilesFull3,stdViews)
        fprintf('%s: sortST\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
    end

    % Split logs so that none exceeds user-specified maximum length
    if maxLogEnable
        waitbar(0.9, h, 'Splitting merged selection table to requested maximum size.')
        fastSplit(outfilesFull, maxLogSize)
        cellfun(@delete,outfilesFull)
        fprintf('%s: fastSplit\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
    end
    
    % Report log size
    waitbar(0.95, h, 'Reporting number of events in selection tables')
    reportLogSize(numEvents,inFilesFull,outpath,scoreEnable,...
        dateFilterEnable,filterEnable,migrateEnable);
    fprintf('%s: reportLogSize\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;
        
    % Let user know that processing is done
    t = sprintf('Processing complete in %s', ...
        datestr((toc)/86400, 'HH:MM:SS'));
    fprintf('\n\n%s\n', t);
    waitbar(1, h, t)
catch ME
    reportErr(ME,outpath)
    return;
end
