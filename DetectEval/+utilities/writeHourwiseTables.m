function writeHourwiseTables(truthFile, testFile, truthC, testC, sched, TPtruthScore, isTPtruth, score, input)
%
% Inputs:
%   truthC - truth log (cell array of char vectors)
%   testC - test log (cell array of char vectors)

%---
% initializations
%---
import utilities.*
outPathROC = input.outPathROC;
timeOverlap = input.timeOverlap;
freqOverlap = input.freqOverlap;

%---
% Make hour vectors
%---

% make vector with beginning and end of each whole handbrowsed hours
schedHour = [];
for i = 1:size(sched, 1)
    schedHour = [schedHour , ...
        ceil(24 .* sched(i,1)):floor(24 .* sched(i,2)) - 1];
end
schedHour = sort(schedHour');
numHours = length(schedHour);

% Make logical vector indicating presence of truth events in each hour
hourTruth = floor(calcRealClockTime(truthC) .* 24);
[~,hourTruthIDX] = ismember(hourTruth, schedHour);
presence = false(numHours,1);
presence(hourTruthIDX) = true;

% make numeric vector of maximum match score in each hour
scoreMaxHourMatch = -inf(numHours, 1);
scoreMaxHourMatch(hourTruthIDX(isTPtruth)) = TPtruthScore;

% Make logical vector indicating presence of test events in each hour
hourTest = floor(calcRealClockTime(testC) .* 24);
[~,hourTestIDX] = ismember(hourTest, schedHour);

% Make numeric vector of maximum test score in each hour
scoreMaxHour = -inf(numHours, 1);
% % scoreMaxHour(hourTestIDX) = max(score, scoreMaxHour(hourTestIDX)); %This is messed up!!!
for i = 1:numHours
    scoreMaxHour(i) = max(score(hourTestIDX==i));
end

%---
% Calculate TP, FN, FP, TN, TPR, and FNR over a series of thresholds
%---
topScore = 10^ceil(log10(max(score)));
threshold = 0:input.inc:topScore;
threshold = threshold';
P = sum(presence); %truth presence
A = numHours - P; %truth absence
TPtruthCount = histcounts(scoreMaxHourMatch, threshold);
TP = cumsum(TPtruthCount, 'reverse')'; %TP = hit
FN = P - TP; %FN = miss = Type II error
len = length(threshold) - 1;
TN = zeros(len ,1);
for i = 1:len
    TN(i) = sum(~presence & scoreMaxHour < threshold(i));
end
FP = numHours - TP - FN - TN; %FP = false alarm = Type I error
testCount = histcounts(scoreMaxHour, threshold); 
testP = cumsum(testCount, 'reverse')'; %test presence
TPR = TP ./ P;
TNR = TN ./ A;
PPV = TP ./ testP;
FPR = 1 - TNR;

%---
% Write sheet header
%---
sheet = 'Hour-wise';

% Define header
M = {'Hour-wise Detector Validation';
     '';
     sprintf('Run Time: %s', datestr(now));
     '';
     sprintf('Time Overlap: %f', timeOverlap);
     sprintf('Frequency Overlap: %f', freqOverlap);
     '';
     sprintf('Number of whole hours hand browsed: %.0f', numHours)
     };
 
% Output header
start = 1;
stop = size(M, 1);
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write truth log names
M = ['Truth Logs:' ; truthFile];
start = stop + 2;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write test log names
M = ['Test Logs:' ; testFile];
start = stop + 2;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

%---
% Write table
%---

% Write column headers for table
M = {'Threshold', ...
     'TPR', ...
     'TNR', ...
     'FPR', ...
     'Precision', ...
     'TP', ...
     'FN', ...
     'FP', ...
     'TN', ...
    };
% %      sprintf('TPR (%.0f%% CI)', plevel * 100), ...
start = stop + 2;
stop = start;
rng = sprintf('A%.0f:I%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write body of table
thresholdStr = cellstr(num2str(threshold(1:end-1), '%.2f'));

TPRstr = cellstr(num2str(TPR, '%0.3f'));
% % TPRciStr = cellstr(num2str(ci, '(%.3f - %.3f)'));
TNRstr = cellstr(num2str(TNR, '%0.3f'));
FPRstr = cellstr(num2str(FPR, '%0.3f'));
PPVstr = cellstr(num2str(PPV, '%.3f'));
TPstr = cellstr(num2str(TP, '%.0f'));
FNstr = cellstr(num2str(FN, '%.0f'));
FPstr = cellstr(num2str(FP, '%.0f'));
TNstr = cellstr(num2str(TN, '%.0f'));

M = [thresholdStr, ...
     TPRstr, ...
     TNRstr, ...
     FPRstr, ...
     PPVstr, ...
     TPstr, ...
     FNstr, ...
     FPstr, ...
     TNstr, ...
    ];
% %      TPRciStr, ...

start = stop + 1;
stop = start + size(TPR, 1) - 1;
rng = sprintf('A%.0f:I%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% pause to avoid fatal error on system that are slow to write XLS files
pause(1)
