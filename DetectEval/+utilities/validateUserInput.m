function status = validateUserInput(input)
%
%  Description:
%	Validate user input
%
% Inputs
%   input - user input copied from GUI
%
% Outputs
%   status - 0 if OK, 1 if bad
%
% History
%   msp2  Aug-24-2017   Initial

    import utilities.*

    status = true;
    if isempty(input.truthSchedPath) || ~exist(input.truthSchedPath, 'file')
        fail('Handbrowsed Schedule File does not exist.')
        return;
    end
    if ~exist(input.outPath, 'dir')
        fail('Output Folder does not exist')
        return;
    end
    if isempty(input.timeOverlap) || input.timeOverlap<0 || input.timeOverlap>1
        fail('Choose a Time Overlap between 0 and 1')
        return;
    end
    if isempty(input.freqOverlap) || input.freqOverlap<0 || input.freqOverlap>1
        fail('Choose a Frequency Overlap between 0 and 1')
        return;
    end
% %     if isempty(input.inc) || input.inc<=0 || input.inc>1
% %         fail('Choose Threshold Increment between 0 and 1')
% %         return;
% %     end
    if isempty(input.scoreFieldName)
        fail('Fill in Score Field')
        return;
    end
    if input.classCheckbox
        if isempty(input.classFieldName)
            fail('Fill in Classification Field')
            return;
        end
    end
    if any(strcmpi(input.snrMode, {'Bins', 'Thresholds'}))
        if isempty(input.snrFieldName)
            fail('Disable "Output in SNR bins" or fill in SNR Field')
            return;
        end
    end
    status = false;
