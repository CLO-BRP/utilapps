function writeTables(truthFile, testFile, filterParam, threshold, TPtruth, FN, TPtest, FP, truthDur, durSound, input)
%
% Description:
%   Report results of binary classification by threshold increment
%
% Inputs:
%   truthFile - names of truth logs (cell array of char vectors)
%   testFile - names of test logs (cell array of char vectors)
%   filterParam - struct with fields:
%      value: name of field by which events will be filtered (string)
%        (if "clock time", use "Begin File" and "File Offset (s)"
%      range: filter events if values are within range (nx2 numeric array)
%   threshold - thresholds used for tabulating event classes (numeric vector)
%   TPtruth - number of TP in truth log at each threshold (numeric vector)
%   FN  - number of FN in truth log at each threshold (numeric vector)
%   TPtest - number of TP in test log at each threshold (numeric vector)
%   FP - number of FP in test log at each threshold (numeric vector)
%   truthDur - cumulative duration of TP in days (scalar)
%   durSound - duration of truthed recording for each truth log in days (scalar)
%   input - struct with fields
%       outPathROC - full path for output (string)
%       timeOverlap (scalar)
%       freqOverlap (scalar)
%
% History
%   msp2  Aug-15-2017   Initial

% initializations
import utilities.*
outPathROC = input.outPathROC;
timeOverlap = input.timeOverlap;
freqOverlap = input.freqOverlap;

%---
% Write sheet header
%---
if isequal(size(filterParam.range, 1), 1)
    
    % Find sheet name from filterParam
    switch filterParam.dataType
        case 'date-time'
            sheet = datestr(filterParam.range(1,1),'mm/dd/yyyy');
            if size(filterParam.range,2)==3
                sheet=sprintf('%s-ch%.0f',sheet,filterParam.range(1,3));
            end
        case 'double'
            sheet = sprintf('%s=%.2f-%.2f', filterParam.field, filterParam.range);
        case 'string'
            sheet = sprintf('%s=%s', filterParam.field, filterParam.range);
        otherwise
            fail('filterParam set up incorrectly for writeTables', 'ERROR')
            return;
    end
    
    % Remove illegal characters from sheet name
    sheet = strrep(sheet, '[', '(');
    sheet = strrep(sheet, ']', ')');
    sheet(ismember(sheet, ' :/?*')) = [];
    sheet = sheet(1:min(31, length(sheet)));
else
    sheet = 'SUMMARY';
end

% Define header
M = {'Event-wise Detector Validation';
     '';
     sprintf('Run Time: %s', datestr(now));
     '';
     'Unit of Analysis: event';
     '';
     sprintf('Time Overlap: %f', timeOverlap);
     sprintf('Frequency Overlap: %f', freqOverlap);
     '';
     sprintf('Duration of hand browsing for this table (days): %.3f', durSound)
     };
 
% Output header
start = 1;
stop = size(M, 1);
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write truth log names
M = ['Truth Logs:' ; truthFile];
start = stop + 2;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write test log names
M = ['Test Logs:' ; testFile];
start = stop + 2;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write values used to filter events
switch filterParam.dataType
    case 'date-time'
        headers = {'Extent of hand browsing for this table:'};
        headers(2,2:5) = {'Begin Date','Begin Time','End Date','End Time'};
        m = size(filterParam.range,1);
        M = cell(m,5);
        M(:,2:5) = [cellstr(datestr(filterParam.range(:,1),'mmm dd, yyyy')),...
                      cellstr(datestr(filterParam.range(:,1),'HH:MM:SS')),...
                      cellstr(datestr(filterParam.range(:,2),'mmm dd, yyyy')),...
                      cellstr(datestr(filterParam.range(:,2),'HH:MM:SS'))...
       ];
        if size(filterParam.range,2)==3
            headers(2,6) = {'Channel'};
            M = [M,cellstr(num2str(filterParam.range(:,3)))];
        end
        M = [headers ; M];
    case 'double'
        M = {sprintf('%s: %.2f - %.2f', filterParam.field, filterParam.range)};
    case 'string'
        M = {sprintf('%s: %s', filterParam.field, filterParam.range)};
    otherwise
        fail('Bad filterParam.dataTYpe for writeTables.')
        return;
end
[m,n] = size(M);
start = stop + 2;
stop = start + m - 1;
if n==1
    rng = sprintf('A%.0f:A%.0f', start, stop);
elseif n==5
    rng = sprintf('A%.0f:E%.0f', start, stop);
else
    rng = sprintf('A%.0f:F%.0f', start, stop);
end
xlswrite(outPathROC, M, sheet, rng)

%---
% Write table
%---

% Write column headers for table
M = {'Threshold', ...
     'TPR', ...
     'Precision', ...
     'FP/hr', ...
     'TP Truth', ...
     'FN Truth', ...
     'Total Truth', ...
     'TP Test', ...
     'FP Test', ...
     'Total Test', ...
    };
% %      sprintf('TPR (%.0f%% CI)', plevel * 100), ...
start = stop + 2;
stop = start;
rng = sprintf('A%.0f:J%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write body of table
thresholdStr = cellstr(num2str(threshold, '%.2f'));
TPtruthStr = cellstr(num2str(TPtruth, '%.0f'));
FNstr = cellstr(num2str(FN, '%.0f'));
lenTruth = TPtruth + FN;
lenTruthStr = cellstr(num2str(lenTruth, '%.0f'));
TPtestStr = cellstr(num2str(TPtest, '%.0f'));
FPstr = cellstr(num2str(FP, '%.0f'));
lenTest = TPtest + FP;
lenTestStr = cellstr(num2str(lenTest, '%.0f'));

TPR = TPtruth ./ lenTruth;
TPRstr = cellstr(num2str(TPR, '%0.3f'));
% % TPRciStr = cellstr(num2str(ci, '(%.3f - %.3f)'));
PPV = TPtest ./ lenTest;
PPVstr = cellstr(num2str(PPV, '%.3f'));
FPperHr = FP ./ (durSound * 24 - truthDur / 3600); %FP per hour
FPperHrStr = cellstr(num2str(FPperHr, '%.2f'));
M = [thresholdStr, ...
     TPRstr, ...
     PPVstr, ...
     FPperHrStr, ...
     TPtruthStr, ...
     FNstr, ...
     lenTruthStr, ...
     TPtestStr, ...
     FPstr, ...
     lenTestStr, ...
    ];
% %      TPRciStr, ...
start = stop + 1;
stop = start + size(TPR, 1) - 1;
rng = sprintf('A%.0f:J%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% pause to avoid fatal error on system that are slow to write XLS files
pause(1)
