function status = check_fields(C, fn, type, input)
%
%  Description:
%    Check if all required fields present
%
% Inputs
%   C - selection table to be checked (cell array of char vectors)
%   fn - file name of log (string)
%   type - truth/detect (string)
%   input - struct with fields
%       scoreFieldName - (string)
%       snrFieldName - (string)
%
% Outputs
%   status - false if OK, true if problem (logical)
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Initializations
%---
status = false;
import utilities.*
scoreFieldName = input.scoreFieldName;
snrFieldName = input.snrFieldName;
field = { ...
  'Begin Time (s)', ...
  'End Time (s)', ...
  'Low Freq (Hz)', ...
  'High Freq (Hz)', ...
  'File Offset (s)', ...
};
fileField = { ...
    'Begin File', ...
    'Begin Path', ...
};

% Check presence of fields that must be present in both test and truth logs
for currField = field
    
    % assert fields exist in truth log and test log
    if ~any(ismember(C(1, :), currField))
        fail(sprintf('Please add the "%s" measurement to:\n  "%s"', currField{:}, fn))
        status = true;
        return;
    end
    
    % assert fields are populated with data in truth log and test log
    if size(C,1) > 1 && isempty(char(get_field(C, currField)))
        fail(sprintf('There must be "%s" values for all selections in:\n  "%s"', currField{:}, fn))
        status = true;
        return;
    end
end

% Check presence of either "Begin File" or "Begin Path" in test log and truth log
if ~any(ismember(C(1, :), fileField))
    fail(sprintf('Please add either the "Begin File" or "Begin Path" measurement to:\n  "%s"', currField{:}, fn))
        status = true;
        return;
end

% Check presence of score field in test log
switch type
    case 'detect'
    
        % determine if score field exists in test log
        if ~any(ismember(C(1, :), scoreFieldName))
            fail(sprintf('Please add a "%s" field to:\n  "%s"', scoreFieldName, fn))
            status = true;
            return;
        end

        % determine if the score field has values in it.
        scoreTest = str2double(get_field(C, scoreFieldName));
        if size(C,1) > 1 && (isempty(scoreTest) || any(isnan(scoreTest)))
            fail(sprintf('There must be "%s" values for all selections in:\n  "%s"', scoreFieldName, fn))
            status = true;
            return;
        end
    case 'truth'
        if ~isempty(snrFieldName)
            
            % determine if SNR field exists in truth log
            if ~any(ismember(C(1, :), snrFieldName))
                fail(sprintf('Please add a "%s" field to:\n  "%s"', snrFieldName, fn))
                status = true;
                return;
            end
            
            % determine of SNR field has values in it
            snrTruth = str2double(get_field(C, snrFieldName));
            if size(C,1) > 1 && (isempty(snrTruth) || any(isnan(snrTruth)))
                fail(sprintf('There must be "%s" values for all selections in:\n  "%s"', snrFieldName, fn))
                status = true;
                return;
            end
        end
end
