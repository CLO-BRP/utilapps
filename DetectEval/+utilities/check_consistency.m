function status = check_consistency(truthC, testC)
%
%  Description:
%    Check if sounds for test log and truth log are from same deployment
%
%  Inputs:
%    truthC - truth log (cell array of char vectors)
%    testC - test log (cell array of char vectors)
%
% Outputs
%   status - false if OK, true if problem (logical)
%
% History
%   msp2  Aug-15-2017   Initial

% Initializations
import utilities.*
templateCollection = {'_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d.'; % for _yyyymmdd_HHMMSS.
            '_\d\d\d\d\d\d_\d\d\d\d\d\d.'; % for _yymmdd_HHMMSS.
            '_D\d\d\d\d\d\d\d\dT\d\d\d\d\d\dp\d\d\d\d\d\dZ.'; % for _DyyyymmddTHHMMSSp000000Z.
            '.\d\d\d\d\d\d\d\dT\d\d\d\d\d\dZ.'}; % for .yyyymmddTHHMMSSp000000Z.
numTemplates = length(templateCollection);
status = true;

% Check that truth log refences sound files from only one deployment
if size(truthC, 1) > 1
    beginFileTruth = get_field(truthC, 'Begin File');
    i = 1;
    prefixTruth = {};
    while i<=numTemplates
        template = templateCollection{i};
        C = regexp(beginFileTruth, template, 'split');
        prefixTruth = cellfun(@(x) x{1}, C, 'UniformOutput', false);
        uniquePrefixTruth = unique(prefixTruth);
        sz = cell2mat(cellfun(@size,C,'Unif',0));
        i = i + 1;
        
        % stop looping if template worked on all Begin File
        if all(sz(1,2)~=1) && all(sz(1,2)==sz(:,2))
            break;
        end
    end
    
    % if Begin File is from multiple deployments, give user chance to stop execution
    if ~isequal(size(uniquePrefixTruth, 1), 1)
        title = 'WARNING: Detector Validation';
        quest = [ ...
            'Truth logs reference sound files from multiple deployments.', ...
            sprintf('\n   %s', uniquePrefixTruth{:}), ...
            sprintf('\n\nProceed anyway?') ...
        ];
        choice = questdlg(quest,title,'Yes','No','No');
        switch choice
            case 'Yes'
                %Do nothing
            case 'No'
                %Stop execution
                status = 1;
                return;
        end
    end
end

% Check that test log refences sound files from only one deployment
if size(testC, 1) > 1
    beginFileTest = get_field(testC, 'Begin File');
    i = 1;
    prefixTest = {};
    while i<=numTemplates
        template = templateCollection{i};
        C = regexp(beginFileTest, template, 'split');
        prefixTest = cellfun(@(x) x{1}, C, 'UniformOutput', false);
        uniquePrefixTest = unique(prefixTest);
        sz = cell2mat(cellfun(@size,C,'Unif',0));
        i = i + 1;
        
        % stop looping if template worked on all Begin File
        if all(sz(1,2)~=1) && all(sz(1,2)==sz(:,2))
            break;
        end
    end
    
    % if Begin File is from multiple deployments, give user chance to stop execution
    if ~isequal(size(uniquePrefixTest, 1), 1)
        title = 'WARNING: Detector Validation';
        quest = [ ...
            'Test logs reference sound files from multiple deployments.', ...
            sprintf('\n   %s', uniquePrefixTest{:}), ...
            sprintf('\n\nProceed anyway?') ...
        ];
        choice = questdlg(quest,title,'Yes','No','No');
        switch choice
            case 'Yes'
                %Do nothing
            case 'No'
                %Stop execution
                status = 1;
                return;
        end
    end
end

% Check that truth log and test log reference sound files from the same deployment
if size(truthC, 1) > 1 && size(testC, 1) > 1
    if ~isequal(length(uniquePrefixTruth),length(uniquePrefixTest)) ...
    || ~all(strcmp(uniquePrefixTruth, uniquePrefixTest))
        title = 'WARNING: Detector Validation';
        quest = [ ...
            sprintf('Test logs and truth logs reference different set of deployments.\n'), ...
            sprintf('\n'), ...
            sprintf('   Test log deployments:\n'), ...
            sprintf('      %s\n', uniquePrefixTest{:}), ...
            sprintf('\n'), ...
            sprintf('   Truth log deployments:\n'), ...
            sprintf('      %s\n', uniquePrefixTruth{:}), ...
            sprintf('\n'), ...
            sprintf('Proceed anyway?') ...
        ];
        choice = questdlg(quest,title,'Yes','No','No');
        switch choice
            case 'Yes'
                %Do nothing
            case 'No'
                %Stop execution
                status = 1;
                return;
        end
    end
end
status = false;
