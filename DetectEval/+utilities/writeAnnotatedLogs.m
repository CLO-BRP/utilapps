function writeAnnotatedLogs(truthC, testC, TPtruthIDX, isTPtest, outPath)
%
% Inputs:
%   truthC - truth log (cell array of char vectors)
%   testC - test log (cell array of char vectors)
%   TPtruthIDX - indexes of TP events in the truth log (numeric vector)
%   isTPtest- indexes of TP events in the test log (logical vector)
%   outPath - path for output (string)

%---
% Set output paths
%---
outPath = fullfile(outPath, 'logs');
if ~isdir(outPath)
    mkdir(outPath)
end
fmt = 'yyyymmdd_HHMMSS';
fnTruth = sprintf('truthAnnotated.%s.selections.txt', datestr(now, fmt));
outTruthFull = fullfile(outPath, fnTruth);
fnTest = sprintf('testAnnotated.%s.selections.txt', datestr(now, fmt));
outTestFull = fullfile(outPath, fnTest);

%---
% Initializations
%---
import utilities.*

%---
% Write annotated truth log
%---
lenTruth = size(TPtruthIDX, 1);
if lenTruth > 1
    
    % Add TP/FN column to truth log
    annotations = {'FN truth'};
    annotations(1:lenTruth, 1) = annotations;
    annotations(TPtruthIDX) = {'TP truth'};
    annotations = ['TP truth/FN truth' ; annotations];
    truthC = [truthC , annotations];

    % Write annotated truth log
    write_selection_table(truthC, outTruthFull)
end

%---
% Write annotated test log
%---
lenTest = size(isTPtest, 1);
if lenTest > 1
    
    % Add TP/FP column
    annotations = {'FP test'};
    annotations(1:lenTest, 1) = annotations;
    annotations(isTPtest) = {'TP test'};
    annotations = ['TP test/FP test' ; annotations];
    testC = [testC , annotations];

    % write annotated test log
    write_selection_table(testC, outTestFull)
end