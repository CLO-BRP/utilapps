function writeChannelDaywiseTable(truthFile, testFile, truthC, testC, sched, TPtruthScore, isTPtruth, score, input)
%
% Inputs:
%   truthFile - file name of truth log(s) (string)
%   testFile - file name of test log(s) (string)
%   truthC - merged truth logs (cell array of strings)
%   testC - merged test logs (cell array of strings)
%   sched - begin and end date and time of handbrowse segments 
%            (<nx4> cell array of doubles)
%   TPtruthScore - highest score from test logs that correspond to each
%                  match in the truth logs
%   isTPtruth - logical index of TP's in merged truth log
%   score - scores from merged test log (double vector)
%   input - user input (struct)

%---
% initializations
%---
import utilities.*
outPathROC = input.outPathROC;
timeOverlap = input.timeOverlap;
freqOverlap = input.freqOverlap;

%---
% Make hour vectors
%---
numSegments = size(sched,1);
tTruth = calcRealClockTime(truthC);
lenTruth = length(tTruth);
truthIDX = zeros(lenTruth,1);
isChan = size(sched,2)==3;
if isChan
    chan = sched(:,3);
    chanTruth = str2double(get_field(truthC,'Channel'));
    for i = 1:lenTruth
        truthIDX(i) = find(tTruth(i)>=sched(:,1) & tTruth(i)<sched(:,2) & chanTruth(i)==chan);
    end
else
    for i = 1:lenTruth
        truthIDX(i) = find(tTruth(i)>=sched(:,1) & tTruth(i)<sched(:,2));
    end
end
TPidx = truthIDX;
TPidx(~isTPtruth) = [];
presence = false(numSegments,1);
presence(truthIDX) = true;

% Make logical vector indicating presence of test events in each segment
tTest = calcRealClockTime(testC);
lenTest = length(tTest);
testIDX = zeros(lenTest,1);
if isChan
    chanTest = str2double(get_field(testC,'Channel'));
    for i = 1:lenTest
        currTime = tTest(i);
        testIDX(i) = find(currTime>=sched(:,1) & currTime<sched(:,2) & chanTest(i)==chan);
    end
else
    for i = 1:lenTest
        currTime = tTest(i);
        testIDX(i) = find(currTime>=sched(:,1) & currTime<sched(:,2));
    end
end

% Make numeric vector of maximum test score in each day
scoreMaxDay = -inf(numSegments,1);
scoreMaxDayMatch = -inf(numSegments, 1);
for i = 1:numSegments
    currScore = max(score(testIDX==i));
    if ~isempty(currScore)
        scoreMaxDay(i) = max(score(testIDX==i));
    end
    tpDayScore = max(TPtruthScore(TPidx==i));
    if ~isempty(tpDayScore)
        scoreMaxDayMatch(i) = tpDayScore;
    end
end

%---
% Calculate TP, FN, FP, TN, TPR, and FNR over a series of thresholds
%---
topScore = 10^ceil(log10(max(score)));
threshold = 0:input.inc:topScore;
threshold = threshold';
P = sum(presence); %truth presence
% % A = numSegments - P; %truth absence
TPtruthCount = histcounts(scoreMaxDayMatch, threshold);
TP = cumsum(TPtruthCount, 'reverse')'; %TP = hit
FN = P - TP; %FN = miss = Type II error
% % len = length(threshold) - 1;
% % TN = zeros(len ,1);
% % for i = 1:len
% %     TN(i) = sum(~presence & scoreMaxDay < threshold(i));
% % end
% % FP = numSegments - TP - FN - TN; %FP = false alarm = Type I error
% % testCount = histcounts(scoreMaxDay, threshold); 
% % testP = cumsum(testCount, 'reverse')'; %test presence
TPR = TP ./ P;
% % TNR = TN ./ A;
% % PPV = TP ./ testP;
% % FPR = 1 - TNR;

%---
% Write sheet header
%---
sheet = 'TimeSegment';

% Define header
if isChan
    unit = 'channel-time segment';
else
    unit = 'time-segment';
end
M = {'Detector Validation by Handbrowsed Time Segment';
     '';
     sprintf('Run Time: %s', datestr(now));
     '';
     sprintf('Unit of Analysis: %s',unit);
     '';
     sprintf('Time Overlap: %f', timeOverlap);
     sprintf('Frequency Overlap: %f', freqOverlap);
     '';
     sprintf('Number of handbrowsed segments: %.0f', numSegments)
     };
 
% Output header
start = 1;
stop = size(M, 1);
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write truth log names
M = ['Truth Logs:' ; truthFile];
start = stop + 2;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write test log names
M = ['Test Logs:' ; testFile];
start = stop + 2;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:A%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

%---
% Write ROC table
%---

% Write column headers for table
M = {'Threshold', ...
     'TPR', ...
     'TP', ...
     'FN', ...
    };
% % M = {'Threshold', ...
% %      'TPR', ...
% %      'TNR', ...
% %      'FPR', ...
% %      'Precision', ...
% %      'TP', ...
% %      'FN', ...
% %      'FP', ...
% %      'TN', ...
% %     };
% %      sprintf('TPR (%.0f%% CI)', plevel * 100), ...
start = stop + 2;
stop = start;
rng = sprintf('A%.0f:D%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)

% Write body of table
thresholdStr = cellstr(num2str(threshold(1:end-1), '%.2f'));

TPRstr = cellstr(num2str(TPR, '%0.3f'));
% % TPRciStr = cellstr(num2str(ci, '(%.3f - %.3f)'));
% % TNRstr = cellstr(num2str(TNR, '%0.3f'));
% % FPRstr = cellstr(num2str(FPR, '%0.3f'));
% % PPVstr = cellstr(num2str(PPV, '%.3f'));
TPstr = cellstr(num2str(TP, '%.0f'));
FNstr = cellstr(num2str(FN, '%.0f'));
% % FPstr = cellstr(num2str(FP, '%.0f'));
% % TNstr = cellstr(num2str(TN, '%.0f'));

M = [thresholdStr, ...
     TPRstr, ...
     TPstr, ...
     FNstr, ...
    ];
% % M = [thresholdStr, ...
% %      TPRstr, ...
% %      TNRstr, ...
% %      FPRstr, ...
% %      PPVstr, ...
% %      TPstr, ...
% %      FNstr, ...
% %      FPstr, ...
% %      TNstr, ...
% %     ];
% %      TPRciStr, ...
start = stop + 1;
stop = start + size(TPR, 1) - 1;
rng = sprintf('A%.0f:D%.0f', start, stop);
xlswrite(outPathROC, M, sheet, rng)
pause(1) % pause to avoid fatal error with 2 simultaneous xlswrite calls

%---
% Write table with maximum TP score for each day
%---
header(1,1) = {'Maximum Score per Sampling Unit'};
beginDate = cellstr(datestr(sched(:,1),'mm/dd/yyyy'));
beginTime = cellstr(datestr(sched(:,1),'HH:MM:SS'));
endDate = cellstr(datestr(sched(:,2),'mm/dd/yyyy'));
endTime = cellstr(datestr(sched(:,2),'HH:MM:SS'));
status = cell(numSegments,1);
status(presence) = {'FN'};
status(presence & ~isinf(scoreMaxDayMatch)) = {'TP'};
scoreMaxDayMatchC = cellstr(num2str(scoreMaxDayMatch));
scoreMaxDayMatchC(isinf(scoreMaxDayMatch))={''};
if isChan
    header(3,1:7) = {'Begin Date','Begin Time','End Date','End Time','Channel','TP/FN','Maximum Score'};
    chanStr = cellstr(num2str(sched(:,3)));
    M = [beginDate,beginTime,endDate,endTime,chanStr,status,scoreMaxDayMatchC];
    lastCol = 'G';
else
    header(3,1:6) = {'Begin Date','Begin Time','End Date','End Time','TP/FN','Maximum Score'};
    M = [beginDate,beginTime,endDate,endTime,status,scoreMaxDayMatchC];
    lastCol = 'F';
end
M = [header;M];
start = stop + 3;
stop = start + size(M, 1) - 1;
rng = sprintf('A%.0f:%s%.0f', start, lastCol, stop);
xlswrite(outPathROC, M, sheet, rng)
pause(1) % pause to avoid fatal error with 2 simultaneous xlswrite calls
