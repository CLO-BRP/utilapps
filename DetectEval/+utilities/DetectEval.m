function DetectEval(handles)
%
% Description:
%   Reads truth log and test log, and returns detector validation 
%   statistics for each log pair (and for all log pairs) in the form of 
%   tables and plots. Also creates annotated logs.
%
% Inputs
%   handles - handles for uielements in GUI
%
% History
%   msp2  Aug-15-2017   Initial

% To Do
% disp('1. Report using "days" or "channel-days" rather than "time segment".')
% disp('2. Add DET curve.')
% disp('     The DET graph plots false reject rate (FRR) by false accept rate (FAR), ')
% disp('     which are calculated the same as the familiar false negative rate (FNR) ')
% disp('     and false positive rate (FPR).')
% disp('     The DET graph typically uses logit scales rather than linear scales, ')
% disp('     which makes the most interest part of the curve occupy more of the plot ')
% disp('     than the ROC curve.  The logit function is calculated as:')
% disp('     logit(p) = loge(p/(1-p) = -loge(1/p - 1), where p could be FRR or FAR.')
% disp('3. Talk to Holger about SNR binning method depending on #FP much great than #TP')
% disp('     If that isn''t true, then the #FP in each SNR bin is driving by number of events rather than FP/TP')

%---
% Initializations
%---
import utilities.*

%---
%User input
%---
input.truthSchedPath = char(handles.inpathSchedule.UserData);
truthFile = handles.inTruth.String;
if ~iscell(truthFile)
    truthFile = {truthFile}; 
end
truthFileFull = fullfile(handles.inTruth.UserData, handles.inTruth.String);
if ~iscell(truthFileFull)
    truthFileFull = {truthFileFull}; 
end
testFile = handles.inTest.String;
if ~iscell(testFile)
    testFile = {testFile}; 
end
testFileFull = fullfile(handles.inTest.UserData, handles.inTest.String);
if ~iscell(testFileFull)
    testFileFull = {testFileFull}; 
end
input.outPath = handles.outpath.UserData;
input.outPathROC = fullfile(input.outPath, sprintf('DetectEval_%s.xlsx', datestr(now, 30)));
input.timeOverlap = str2double(handles.timeOverlap.String);
input.freqOverlap = str2double(handles.freqOverlap.String);
input.inc = str2double(handles.increment.String);
input.scoreFieldName = handles.scoreField.String;
input.classCheckbox = handles.classCheckbox.Value;
input.classFieldName = handles.classFieldName.String;

C = cellstr(handles.snrPopup.String);
snrMode = C{handles.snrPopup.Value};
input.snrMode = snrMode;
switch snrMode
    case {'Bins', 'Thresholds'}
        input.snrFieldName = handles.snrField.String;
        input.snrBin(1,1) = str2double(handles.snr1a.String);
        input.snrBin(2,1) = str2double(handles.snr2a.String);
        input.snrBin(3,1) = str2double(handles.snr3a.String);
        input.snrBin(4,1) = str2double(handles.snr4a.String);
        input.snrBin(1,2) = str2double(handles.snr1b.String);
        input.snrBin(2,2) = str2double(handles.snr2b.String);
        input.snrBin(3,2) = str2double(handles.snr3b.String);
        input.snrBin(4,2) = str2double(handles.snr4b.String);
        input.snrBin = input.snrBin(~isnan(input.snrBin(:,1)),1:2);
    case '(disabled)'
        input.snrFieldName = '';
end

% Validate user input
status = validateUserInput(input);
if status; return; end

%---
% Read handbrowsing schedule
%---
truthSchedFullfile = input.truthSchedPath;
% % [~, ~, C] = xlsread(truthSchedFullfile, '', '', 'basic');
[~, ~, C] = xlsread(truthSchedFullfile);
sched = readSched(C);
if isempty(sched); return; end

%---
% Read and merge truth logs
%---
h = waitbar(0, '', 'Name', 'Truth Logs');
truthC = {};
header0 = {};
lenTruth = length(truthFileFull);
for i = 1:lenTruth
    waitbar((i-1)/lenTruth, h, 'Reading')
    C = read_selection_table(truthFileFull{i});
    if isempty(C); close(h); return; end
    if isequal(i, 1)
        header0 = C(1, :);
        truthC = header0;
    end
    status = checkLog(C, truthFile{i}, 'truth', header0, input);
    if status; close(h); return; end
    
    % Concatenate current selection table with previous selection tables
    if size(C, 1) > 1
        truthC = [truthC ; C(2:end, :)];
    end
end
truthC = fixEventId(truthC); %Make selection id consecutive integers
truthC = addBeginFile(truthC); %Add "Begin File" field if doesn't exist
delete(h)

%---
% Read and merge test logs
%---
h = waitbar(0, '', 'Name', 'Test Logs');
testC = {};
header0 = {};
lenTest = length(testFileFull);
for i = 1:lenTest
    waitbar((i-1)/lenTest, h, 'Reading')
    C = read_selection_table(testFileFull{i});
    if isempty(C); close(h); return; end
    if isequal(i, 1)
        header0 = C(1, :);
        testC = header0;
    end
    C = removeNaNscores(C, testFile{i}, input);
    status = checkLog(C, testFile{i}, 'detect', header0, input);
    if status; close(h); return; end
    if size(C, 1) > 1
        testC = [testC ; C(2:end, :)];
    end
end
testC = fixEventId(testC);
testC = addBeginFile(testC); %Add "Begin File" field if doesn't exist
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % if true
% %     testC = addFreq(testC); %Add "Low Freq"=0 and "High Freq"=200,000
% % end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % if true
% %     testC = addFreq(testC); %Add "Score" with random numbers 0 - 1  
% % end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delete(h)

%---
% Check deployment consistency between truth logs and test logs
%---
h = waitbar(0, '', 'Name', 'Checking deployment consistency between truth logs and test logs');
status = check_consistency(truthC, testC);
if status; close(h); return; end
delete(h)

%---
% Output summary results
%---

% Filter events
filterParam.field = 'clock time';
filterParam.logType = 'both';
filterParam.dataType = 'date-time';
filterParam.range = sched;
filterParam.warn = true;
[truthC, testC] = filterLogs(truthC, testC, filterParam);

% Match selections
h = waitbar(0, 'Classifying events', 'Name', 'Summary Output');
[TPtruthScore, isTPtruth, isTPtest, truthDur, score] = match(truthC, testC, input);

% Calculations
waitbar(0, h, 'Counting events')
lenTruth = size(truthC, 1) - 1;
[threshold, TPtruth, FN, TPtest, FP] = ROCcounts(input.inc, score, TPtruthScore, isTPtest, lenTruth);

    
% % % If handbrowsed all channels, write day-wise tables
% % if size(sched,2)==2
% %     waitbar(0, h, 'Writing day-wise table')
% %     writeDaywiseTable(truthFile, testFile, truthC, testC, sched, TPtruthScore, isTPtruth, score, input)
% %     
% % % If handbrowsed single channels, write day-wise tables
% % else
    waitbar(0, h, 'Writing channel-day-wise table')
    writeChannelDaywiseTable(truthFile, testFile, truthC, testC, sched, TPtruthScore, isTPtruth, score, input)
% % end

% % % Write hour-wise tables
% % waitbar(0, h, 'Writing hour-wise tables')
% % writeHourwiseTables(truthFile, testFile, truthC, testC, sched, TPtruthScore, isTPtruth, score, input)

% Write event-wise tables
waitbar(0, h, 'Writing event-wise tables')
durSoundTotal = sum(diff(sched(:,1:2), 1, 2)); % total number of days handbrowsed
writeTables(truthFile, testFile, filterParam, threshold, TPtruth, FN, TPtest, FP, truthDur, durSoundTotal, input)

% Write plots
waitbar(0, h, 'Creating plots')
holdState = 'off';
ah = {};
ah = writePlots(threshold, TPtruth, FN, TPtest, FP, truthDur, durSoundTotal, holdState, ah);
savePlots(ah, input.outPath, 'Summary')

% Write annotated selection tables (consider specifying score cutoff)
waitbar(0, h, 'Writing annotated logs')
writeAnnotatedLogs(truthC, testC, isTPtruth, isTPtest, input.outPath)
    
% Delete waitbar
delete(h)


%---
% Validate detector one handbrowse interval at a time
%---

% If more than one time period hand browsed
ah = {};
filterParam.field = 'clock time';
filterParam.logType = 'both';
filterParam.dataType = 'date-time';
filterParam.warn = false;
holdState = 'on';
if size(sched, 1) > 1
    delete(h)
    h = waitbar(0, '', 'Name', 'Output for hand browsed time segments');
    
    % Report results separately for each time period
    for i = 1:size(sched, 1)

        % Filter events
        filterParam.range = sched(i, :);
        [truthCfilter, testCfilter] = filterLogs(truthC, testC, filterParam);
        currSchedCell = cellstr(datestr(filterParam.range(1,1:2), 'mmm dd, yyyy  HH:MM:SS'));
        currSchedStr = sprintf('%s -- %s', currSchedCell{:});

        % Match selections
        waitbar(i, h, currSchedStr, 'Name', 'Classifying Events')
        [TPtruthScore, isTPtruth, isTPtest, truthDur, score] = match(truthCfilter, testCfilter, input);

        % Calculations
        waitbar(i, h, currSchedStr, 'Name', 'Counting Events')
        lenTruth = size(isTPtruth, 1);
        [threshold, TPtruth, FN, TPtest, FP] = ROCcounts(input.inc, score, TPtruthScore, isTPtest, lenTruth);

        % Output classification table
        waitbar(i, h, currSchedStr, 'Name', 'Writing Output Table')
        durSound = diff(filterParam.range(1,1:2), 1, 2);
        writeTables(truthFile, testFile, filterParam, threshold, TPtruth, FN, TPtest, FP, truthDur, durSound, input)

        % Output plots
        ah = writePlots(threshold, TPtruth, FN, TPtest, FP, truthDur, durSound, holdState, ah);
    end
    
    % Add legend to plots
    filterParam.range = sched;
    addLegend(ah, filterParam)
    
    % Save plots and delete
    savePlots(ah, input.outPath, 'HandbrowseSched')
    
    % Delete waitbar
    delete(h)
end


%---
% Validate detector by SNR bin
%---
if any(strcmpi(input.snrMode, {'Bins', 'Thresholds'}))
    
    % Report results separately for each SNR bin
    ah = {};
    filterParam.field = input.snrFieldName;
    filterParam.logType = 'truth';
    filterParam.dataType = 'double';
    filterParam.warn = false;
    holdState = 'on';
    h = waitbar(0, '', 'Name', 'Output for hand browsed time segments');
    for i = 1:size(input.snrBin, 1)

        % Filter events
        filterParam.range = input.snrBin(i, 1:2);
        [truthCfilter, testCfilter] = filterLogs(truthC, testC, filterParam);
        currSNRbinStr = sprintf('SNR bin: %.2f - %.2f', filterParam.range); 
        currSNRbinStr = strrep(currSNRbinStr, '_', '\_'); %Escape underscores to prevent TeX formatting

        % Match selections
        waitbar(i, h, currSNRbinStr, 'Name', 'Classifying Events')
        [TPtruthScore, isTPtruth, isTPtest, truthDur, score] = match(truthCfilter, testCfilter, input);

        % Calculations
        waitbar(i, h, currSNRbinStr, 'Name', 'Counting Events')
        lenTruth = size(isTPtruth, 1);
        [threshold, TPtruth, FN, TPtest, FP] = ROCcounts(input.inc, score, TPtruthScore, isTPtest, lenTruth);

        % Output classification table
        waitbar(i, h, currSNRbinStr, 'Name', 'Writing Output Table')
        durSoundTotal = sum(diff(sched(:,1:2), 1, 2)); % total number of days handbrowsed
        writeTables(truthFile, testFile, filterParam, threshold, TPtruth, FN, TPtest, FP, truthDur, durSoundTotal, input)

        % Output plots
        ah = writePlots(threshold, TPtruth, FN, TPtest, FP, truthDur, durSoundTotal, holdState, ah);
    end
    
    % Add legend to plots
    filterParam.range = input.snrBin;
    addLegend(ah, filterParam)
    
    % Save plots and delete
    savePlots(ah, input.outPath, 'SNR')
    
    % Delete waitbar
    delete(h)
end


%---
% Validate detector by user-defined categories in truth log field
%---
if input.classCheckbox
    
    % Report results separately for each SNR bin
    ah = {};
    filterParam.field = input.classFieldName;
    filterParam.logType = 'test';
    filterParam.dataType = 'string';
    filterParam.warn = false;
    categories = unique(get_field(testC, input.classFieldName));
    holdState = 'on';
    h = waitbar(0, '', 'Name', 'Output for user field categories');
    for i = 1:size(categories, 1)

        % Filter events
        filterParam.range = categories{i};
        [truthCfilter, testCfilter] = filterLogs(truthC, testC, filterParam);
        currUserBinStr = sprintf('%s: %s', filterParam.field, filterParam.range);
        currUserBinStr = strrep(currUserBinStr, '_', '\_'); %Escape underscores to prevent TeX formatting

        % Match selections
        waitbar(i, h, currUserBinStr, 'Name', 'Classifying Events')
        [TPtruthScore, isTPtruth, isTPtest, truthDur, score] = match(truthCfilter, testCfilter, input);

        % Calculations
        waitbar(i, h, currUserBinStr, 'Name', 'Counting Events')
        lenTruth = size(isTPtruth, 1);
        [threshold, TPtruth, FN, TPtest, FP] = ROCcounts(input.inc, score, TPtruthScore, isTPtest, lenTruth);

        % Output classification table
        waitbar(i, h, currUserBinStr, 'Name', 'Writing Output Table')
        durSoundTotal = sum(diff(sched, 1, 2)); % total number of days handbrowsed
        writeTables(truthFile, testFile, filterParam, threshold, TPtruth, FN, TPtest, FP, truthDur, durSoundTotal, input)

        % Output plots
        ah = writePlots(threshold, TPtruth, FN, TPtest, FP, truthDur, durSoundTotal, holdState, ah);
    end
    
    % Add legend to plots
    filterParam.range = categories;
    addLegend(ah, filterParam)
    
    % Save plots and delete
    savePlots(ah, input.outPath, filterParam.field)
    
    % Delete waitbar
    delete(h)
end

% Final waitbar
waitbar(1, 'Done', 'Name', 'Detector Validation');
disp(' ')
disp(' ')
disp('***** Finished processing *****')
