function status= checkLog(C, fn, type, header0, input)
%
%  Description:
%    Check that Raven selection table has all requirements for application
%
% Inputs
%   C - selection table to be checked (cell array of char vectors)
%   fn - file name of log (string)
%   type - truth/detect (string)
%   header0 - column headers for first log (cell vector of char vectors)
%   input - struct with fields
%       scoreFieldName - (string)
%       snrFieldName - (string)
%
% Outputs
%   status - false if OK, true if problem (logical)
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Initializations
%---
import utilities.*
status = true;

% check that header is same as that in previous log
sz = size(C);
if sz(1) > 1 && (~isequal(sz(2), length(header0)) || ~all(strcmp(C(1,:), header0)))
    t = sprintf('\n\nHeaders of %s logs must match those for previous %s logs:\n  %s\n', type, type, fn);
    fail(t)
    return;
end

% Check all required fields present and populated with values
status = check_fields(C, fn, type, input);
if status; return; end

% % % Check real time feature set properly
% % status = check_clock_times(C, fn);
% % if status; return; end

status = false;