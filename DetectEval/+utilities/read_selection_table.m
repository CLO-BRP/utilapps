function C = read_selection_table(inPathFull)
%
% Description:
%   Reads specified Raven seletion table and returns a table variable.
%
% Usage:
%   read_selection_table
%
% Inputs:
%   inPathFull - full path of selection table (char)
% Outputs:
%   C - selection table (cell array of char vectors)
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Initialiations
%---

% Import utilities package
import utilities.*

%---
% Read selection table
%---

% Open selection table text read mode
fid = fopen(inPathFull,'rt');
if isequal(fid,-1)
    fail(sprintf('Selection table could not be read:\n  %s\n',inPathFull));
    C = {};
    return;
end

% Parse selection table by cell into cell vector
C = textscan(fid,'%s','Delimiter','\n');

% Close selection table
fclose(fid);

%---
% Reshape cell vector into cell array
%---
C = regexp(C{1},'\t','split');
m = size(C,1);
n = size(C{1,:},2);
try
    C = reshape([C{:,:}],[n,m])';
catch ME
    
    % If any rows in selection table have number of columns different than the header
    if strcmp(ME.identifier, 'MATLAB:getReshapeDims:notSameNumel')
        
        % Delete those rows
        [~, fn, ext] = fileparts(inPathFull);
        fnExt = [fn , ext];
        numCols = cellfun(@length, C);
        errIDX = find(numCols ~= n);
        C(errIDX) = [];
        m = size(C,1);
        
        % Warn the user
        errIDXstr = sprintf('%.0f, ', errIDX);
        t1 = sprintf('Lines deleted in\n  "%s"\n', fnExt);
        t2 = sprintf('because the number of columns is different than that of the header: %s', errIDXstr);
        fail([t1 , t2])
    end    
    C = reshape([C{:,:}],[n,m])';
end

%---
% Delete lines in selection table for all views but one
%---
view = get_field(C, 'View');
uniqueView = unique(view);

% If more than one line per event
if length(uniqueView) > 1
    
    % If there are spectrogram views, use the first one
    isSpecgram = find(~cellfun(@isempty, regexp(uniqueView, {'Spectrogram \d+'})), 1, 'first');
    if ~isempty(isSpecgram)
        idx = [false; ~strcmp(view, uniqueView(isSpecgram))]; % add false for header row
    
    % Otherwise, use the first view, whatever it is
    else
        idx = [false; ~strcmp(view, uniqueView(1))]; % add false for header row        
    end
    C(idx,:) = [];
end

%---
% Sort selection table in time order
%---
beginTime = get_field(C, 'Begin Time (s)');
if ~isempty(beginTime)
    [~, idx] = sort(str2double(beginTime));
    idx = [1; idx + 1];
    C = C(idx, :);
end
