function varargout = MigrateTriton(varargin)
% MIGRATETRITON MATLAB code for MigrateTriton.fig
%      MIGRATETRITON, by itself, creates a new MIGRATETRITON or raises the existing
%      singleton*.
%
%      H = MIGRATETRITON returns the handle to a new MIGRATETRITON or the handle to
%      the existing singleton*.
%
%      MIGRATETRITON('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MIGRATETRITON.M with the given input arguments.
%
%      MIGRATETRITON('Property','Value',...) creates a new MIGRATETRITON or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MigrateTriton_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MigrateTriton_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MigrateTriton

% Last Modified by GUIDE v2.5 14-Nov-2017 12:38:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MigrateTriton_OpeningFcn, ...
                   'gui_OutputFcn',  @MigrateTriton_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MigrateTriton is made visible.
function MigrateTriton_OpeningFcn(hObject, eventdata, handles, varargin)

    %Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))

    % Choose default command line output for MigrateTriton
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);


% --- Executes when user attempts to close MigrateTriton.
function MigrateTriton_CloseRequestFcn(hObject, eventdata, handles)

    %Delete GUI
    delete(hObject);
    
    %Remove application root directory from search paths
    rmpath(fileparts(mfilename('fullpath')))


% --- Outputs from this function are returned to the command line.
function varargout = MigrateTriton_OutputFcn(hObject, eventdata, handles) 

    % Get default command line output from handles structure
    varargout{1} = handles.output;


%--------------------------------------------------------------------------
function fnTriton_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current file list
    %--
    fnTritonPath = char(handles.fnTriton.UserData);
    if ~isdir(fnTritonPath)
        fnTritonPath = pwd;
    end

    %--
    % Select new input path
    %--
    filterSpect = fullfile(fnTritonPath, '*.xls');
    [fnTriton, fnTritonPath] = uigetfile( ...
        filterSpect, ...
        'Select handbrowsed logs', ...
        'Multiselect', 'on');
    if isequal(fnTriton, 0)
        return;
    end
    handles.fnTriton.UserData = fnTritonPath;
    handles.fnTriton.String = fnTriton;
    

%--------------------------------------------------------------------------
function fnListfile_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current file list
    %--
    fnListfilePath = char(handles.fnListfile.UserData);
    if ~isdir(fnListfilePath)
        fnListfilePath = pwd;
    end

    %--
    % Select new input path
    %--
    filterSpect = fullfile(fnListfilePath, '*.txt');
    [fnListfile, fnListfilePath] = uigetfile( ...
        filterSpect, ...
        'Select handbrowsed logs', ...
        'Multiselect', 'on');
    if isequal(fnListfile, 0)
        return;
    end
    handles.fnListfile.UserData = fnListfilePath;
    handles.fnListfile.String = fnListfile;


%--------------------------------------------------------------------------
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path
    %--
    outpath = char(handles.outpath.UserData);
    if ~isdir(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    handles.outpath.UserData = outpath;
    handles.outpathTitle.TooltipString = outpath;
    [~, outpathDisplay] = fileparts(outpath);
    handles.outpath.String = outpathDisplay;


%--------------------------------------------------------------------------
function cancelButton_Callback(hObject, eventdata, handles)
    delete(hObject.Parent)


%--------------------------------------------------------------------------
function runButton_Callback(hObject, eventdata, handles)
    utils.migrate(handles)
