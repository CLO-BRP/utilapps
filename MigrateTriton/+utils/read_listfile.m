function [listfile,status]=read_listfile(fn_full)
% Read Raven listfile
%   Input
%       fn_full - full path to Raven listfile
%   Output
%       listfile - nx1 cellstr with contents of listfile
%       status - 0=ok, 1=problem

    status=1;
    fid = fopen(fn_full,'rt');
    if isequal(fid,-1)
        t=sprintf('Listfile could not be opened:\n   %s',fn_full);
        utils.fail(t,'WARNING')
        return;
    end
    C = textscan(fid,'%s','Delimiter','\n');
    fclose(fid);
    listfile=C{1};
    
    %delete extra columns,
    listfile = listfile(:, 1);
    
    %delete extra rows
    idx = find(cellfun(@isempty, listfile), 1);
    if ~isempty(idx)
        if isequal(idx, 1)
            listfile = {};
        else
            listfile = listfile(1:idx-1, 1);
        end
    end
    
    % set status OK
    status=0;
    