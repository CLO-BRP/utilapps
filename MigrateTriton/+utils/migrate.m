function migrate(handles)

%---
% Import utilities
%---
import utils.*

%---
%User input
%---
logNameFull = fullfile(handles.fnTriton.UserData, handles.fnTriton.String);
if ~iscell(logNameFull)
    logNameFull = {logNameFull}; 
end
listNameFull = fullfile(handles.fnListfile.UserData, handles.fnListfile.String);
outpath = handles.outpath.UserData;

tic

% If no file names input, ask user to select files
if ~exist('logNameFull', 'var')
    filter = '*.xls';
    title = 'Select one or more Triton logs to migrate to Raven';
    [f, logName] = uigetfile(filter, title, 'Multiselect', 'on');
    if isequal(f, 0)
        return;
    end
    logNameFull = fullfile(logName, f);
end
if ~iscell(logNameFull)
    logNameFull = {logNameFull};
end
[~, logName] = cellfun(@fileparts, logNameFull, 'UniformOutput', false);

% If no listfile name input, ask user to select file
if ~exist('listNameFull', 'var')
    filter = '*.txt';
    title = 'Select listfile to migrate Triton log to';
    [listName, p] = uigetfile(filter, title, 'Multiselect', 'on');
    if isequal(listName, 0)
        return;
    end
    listNameFull = fullfile(p, listName);
end

%---
% Read listfile and extract header information
%---

% Read listfile
h = waitbar(0, 'Reading Listfile');
listfileFull = read_listfile(listNameFull);
[~, listfile] = cellfun(@fileparts, listfileFull, 'UniformOutput', false);
pat(1:length(listfile), 1) = {'yymmdd_HHMMSS'};
fileTime = cellfun(@file_datenum, listfile, pat);

% Extract file headers
try
    C = cellfun(@audioinfo, listfileFull, 'UniformOutput', false);
catch
    fail('Sound files specified in list file not found.','WARNING')
    delete(h);
    return;
end
if isempty(C)
    fail('Sound file headers cannot be read.','WARNING')
    delete(h);
    return;
end
M = cell2mat(C);
NumChannels = [M.NumChannels]';
SampleRate = [M.SampleRate]';
TotalSamples = [M.TotalSamples]';
CumSamples = cumsum(TotalSamples);
CumDuration = CumSamples ./ SampleRate;
BeginDuration = [0 ; CumDuration];
BeginDuration(end) = [];
BitsPerSample = M.BitsPerSample;

% Check files for consistent number of channels and sample rate
if ~all(NumChannels == 1)
    fail('Sound files do not all have the same number of channels.','WARNING')
    delete(h);
    return;
else
    NumChannels = NumChannels(1);
end
if length(unique(SampleRate)) > 1
    fail('Sound files do not all have the same sample rate.','WARNING')
    delete(h);
    return;
else
    SampleRate = SampleRate(1);
end

%---
% Migrate Triton logs to Raven selection tables
%---
header = { ...
    'Selection', ...
    'View', ...
    'Channel', ...
    'Begin Time (s)', ...
    'End Time (s)', ...
    'Low Freq (Hz)', ...
    'High Freq (Hz)', ...
    'Begin File', ...
    'File Offset (s)', ...
};
numLogs = length(logNameFull);
k = 86400;
for i = 1:numLogs
    
    % Read Triton log
    currLogName = logNameFull{i};
    waitbar((i-1)/numLogs, h, strrep(sprintf('Migrating %s', logName{i}), '_', '\_'))
    [~, ~, tritonC] = xlsread(currLogName);
    startTimeC = get_field(tritonC, 'Start time');
    if isempty(startTimeC)
        fail('Unable to find "Start time" in Triton log:\n  %s', currLogName)
        return;
    end
    startTime = datenum(startTimeC);
    stopTimeC = get_field(tritonC, 'End time');
    if isempty(stopTimeC)
        fail('Unable to find "End time" in Triton log:\n  %s', currLogName)
        return;
    end
    stopTime = datenum(stopTimeC);
    
    % Test that log does not reference times outside those defined by listfile
    if min([startTime ; stopTime])< min(fileTime) || max([startTime ; stopTime]) > max(fileTime)
        t = 'Events in Triton log are beyond times in list file:';
        fail(sprintf('%s\n  %s', t, logName{i}))
        return;
    end
    
    % Find Begin File and File Offset (s)
    numEvents = length(startTime);
    beginFile = cell(numEvents, 1);
    fileOffsetN = zeros(numEvents, 1);
    beginTimeN = zeros(numEvents, 1);
    endTimeN = zeros(numEvents, 1);
    for j = 1:numEvents
        idx = find(startTime(j) >= fileTime, 1, 'last');
        beginFile(j) = listfile(idx);
        currFileOffset = (startTime(j) - fileTime(idx)) * k;
        fileOffsetN(j) = currFileOffset;
        currBeginDuration = BeginDuration(idx);
        beginTimeN(j) = currBeginDuration + currFileOffset;
        endTimeN(j) = currBeginDuration + currFileOffset + (stopTime(j) - startTime(j)) * k;
    end
    
    % Make migrated Raven selection table
    id = 1:numEvents;
    id = cellstr(num2str(id'));
    view(1:numEvents, 1) = {'Spectrogram 1'};
    channel(1:numEvents, 1) = cellstr(num2str(NumChannels));
    beginTime = cellstr(num2str(beginTimeN));
    endTime = cellstr(num2str(endTimeN));
    lowFreq(1:numEvents, 1) = {'0'};
    hiFreq(1:numEvents, 1) = cellstr(num2str(SampleRate/2));
    fileOffset = cellstr(num2str(fileOffsetN));
    body = [id, view, channel, beginTime, endTime, lowFreq, hiFreq, beginFile, fileOffset];
    ravenC = [header ; body];
    outC = [ravenC, tritonC];
    
    % Write migrated Raven selection table
    [~, f] = fileparts(currLogName);
    outFileFull = fullfile(outpath, [f, '.selections.txt']);
    write_selection_table(outC, outFileFull);
end

% Tell user processing is done
disp(' ')
disp(' ')
disp('**********************************')
disp(' ')
fprintf('Processing complete in %.0f seconds.\n', toc);
disp(' ')
disp('**********************************')
delete(h)


















