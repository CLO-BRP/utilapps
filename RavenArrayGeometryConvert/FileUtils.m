% Utility functions that extend MATLAB base functionality for files.
%
% Copyright (c) 2013-2014, Cornell University, Lab of Ornithology
% All rights reserved.
%
% License:
%
% Author:    M Pitzrick 

classdef FileUtils

% =============================================================================
%                             Constructor Methods
% =============================================================================
    methods
        %% --------------------------------------------------------------------
        % FileUtils constructor
        %
        %    Description: constructor for this class
        %
        %    Input:  
        %       none
        %
        %    Output:
        %       this (object) - the new object instance of this class
        %% --------------------------------------------------------------------
        function this = FileUtils()
        end                
    end % constructor methods %

% =============================================================================
%                             Static Methods
% =============================================================================
    methods (Static)

        %% --------------------------------------------------------------------
        %   txt_read
        %% --------------------------------------------------------------------
        function txt_cell = txtread(URI, f, delim)
            %txtread - reads text file and creates mxn cell array of strings, 
            % here m is the number of records and n is the number of 
            % character-delimited fields
            %   
            %  input 
            %
            %    URI    - path of text file
            %
            %    f      - read format for textscan
            %
            %    delim  - delimiter(s) for field separation
            %
            %  output
            %
            %    txt_cell = mxn cell array of strings, where 
            %                 m is the number of records
            %                 n is the number of character-delimited fields

            fid = fopen(URI, 'r');

%             assert(fid ~= -1, '\nFile not found:\n%s\n', URI)

            txt_cell_in = textscan(fid, f, 'Delimiter', delim,'ReturnOnError',0);

            fclose(fid);

            len = length(txt_cell_in);

            len2 = length(txt_cell_in{1,1});

            txt_cell = cell(len2,len);

            for i = 1:len

              txt_cell(1:len2,i) = txt_cell_in{1,i};

            end                        
        end % txtread %
        
    end % methods %

end % classdef %
