function table2raven(infile, outfile)
% table2raven converts a sensor geometry table in XLS format to Raven ArrayGeometry.txt.
    
% %     %initializations
% %     tab = sprintf('\t');
    
    %read in sensor geometry table in XLS format
    Min = readcell(infile);
    [m, ~] = size(Min);
    
    %open output file
    if exist(outfile, 'file')
        txt = sprintf('"%s" already exists. Pick another name or path.', outfile);
        fprintf(2, '\n%s\n', '****************************************************************************');
        fprintf(2, '\n%s\n', txt);
        fprintf(2, '\n%s\n', '****************************************************************************');
        h = warndlg(txt, 'WARNING', 'modal');
        movegui(h, 'center')
        return;
    end
    fid = fopen(outfile, 'wt');
    
    %add title to output matrix
    fprintf(fid, '%s\n\n', ['# ', Min{1,1}]);
    
    %add georeference to output matrix
    fprintf(fid, '%s\n', '$header$');
    fprintf(fid, '\t%s\n', 'Georeferenced');
    fprintf(fid, '\t%s\n', ['Georeference', sprintf(' %f', Min{3,2:3})]);
    fprintf(fid, '%s\n', '$end$');
    
    %for each sensor geolocation
    i = 7; %first line of table body
    while i<=m
        
        %add begin token and geolocation date-time
        fprintf(fid, '%s\n', '$begin$');
        fprintf(fid, '\t%s\n', sprintf('%.0f %s %s', Min{i,1}, datestr(Min{i,2}, 'HH:MM:SS'), Min{i,3}));
         
        %---
        %if first sensor geolocation, add sensor geometry format to output matrix
        %---
        if isequal(i, 7)
            
            %find number of sensors
            format1 = Min(5,:);
            format1 = format1(cellfun('isclass', format1, 'char'));
            temp = regexp(format1{end}, 'Sensor-', 'split');    
            numSensors = str2double(temp(end));

            %add sensor geometry format to output matrix
            format2 = Min(6,:);
            format2 = format2(cellfun('isclass', format2, 'char'));
            numFields = floor(length(format2) / numSensors);
            format = format2(1:numFields);
            fprintf(fid, '\t%s\n', '# Sensor Geolocated coordinates.');
            fprintf(fid, '\t%s\n', ['# Format is:', sprintf(' %s', format{:})]);
            fprintf(fid, '\t%s\n', '# Values should be in decimal degrees and meters');
            
        end %if isequal(i, 7)

        %add sensor locations with uncertainty to output matrix
        start = 4;
        stop = 4-1+numFields;
        for j = 1:numSensors
            loc = Min(i,start:stop);
            line = sprintf('%f ', loc{:});
            fprintf(fid, '\t%s\n', line(1:end-1));
            start = start + numFields;
            stop = stop + numFields;
        end %for j = 1:numSensors
        
        %add end token
        fprintf(fid, '%s\n', '$end$');

        i = i + 1;
    end %while i<=m
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %     disp(Mout)
% %     keyboard;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fclose(fid);
end

