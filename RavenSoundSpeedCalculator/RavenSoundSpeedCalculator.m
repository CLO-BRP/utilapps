function varargout = RavenSoundSpeedCalculator(varargin)
% RAVENSOUNDSPEEDCALCULATOR MATLAB code for RavenSoundSpeedCalculator.fig
%      RAVENSOUNDSPEEDCALCULATOR, by itself, creates a new RAVENSOUNDSPEEDCALCULATOR or raises the existing
%      singleton*.
%
%      H = RAVENSOUNDSPEEDCALCULATOR returns the handle to a new RAVENSOUNDSPEEDCALCULATOR or the handle to
%      the existing singleton*.
%
%      RAVENSOUNDSPEEDCALCULATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RAVENSOUNDSPEEDCALCULATOR.M with the given input arguments.
%
%      RAVENSOUNDSPEEDCALCULATOR('Property','Value',...) creates a new RAVENSOUNDSPEEDCALCULATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RavenSoundSpeedCalculator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RavenSoundSpeedCalculator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
%
%History
%   msp2  12/22/2016  Original
%   msp2  04/21/2017  Report which CTD logs are missing data
%                     Report number of sensors for each computation in output
%                     Add /r before /n to each line so Notepad can display proper line breaks

% Edit the above text to modify the response to help RavenSoundSpeedCalculator

% Last Modified by GUIDE v2.5 08-Apr-2019 16:01:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RavenSoundSpeedCalculator_OpeningFcn, ...
                   'gui_OutputFcn',  @RavenSoundSpeedCalculator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RavenSoundSpeedCalculator is made visible.
function RavenSoundSpeedCalculator_OpeningFcn(hObject, eventdata, handles, varargin)

    % Choose default command line output for RavenSoundSpeedCalculator
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = RavenSoundSpeedCalculator_OutputFcn(hObject, eventdata, handles)

    % Get default command line output from handles structure
    varargout{1} = handles.output;

% --- 
function infile_list_ButtonDownFcn(hObject, eventdata, handles)
    
    %--
    % Find current path for input CTD logs
    %--
    inFilesPath = char(handles.infile_list.UserData);
    if ~isfolder(inFilesPath)
        inFilesPath = pwd;
    end
    
    %--
    % Select CTD logs for input
    %--
    filterSpect = fullfile(inFilesPath, sprintf('*.%s','txt'));
    [inFiles, inFilesPath2] = uigetfile( ...
        filterSpect, ...
        'Select CTD logs for input', ...
        'Multiselect', 'on');
    if isequal(inFiles, 0)
        return;
    end
    handles.infile_list.UserData = inFilesPath2;

    if ~iscell(inFiles)
        inFiles = {inFiles};
    end
    handles.infile_list.String = inFiles;

% ---
function outfile_txt_ButtonDownFcn(hObject, eventdata, handles)
    
    %--
    % Find last path for output
    %--
    outfileFull = char(handles.outfile_txt.UserData);
    if ~isfolder(outfileFull)
        outpath = pwd;
    end
    
    %--
    % Suggest file name and path for output
    %--
    title = 'Select output directory';
    outpath = uigetdir(fileparts(outfileFull),title);
    if isequal(outpath, 0)
        return;
    end
    [~,dirname] = fileparts(outpath);
    outfileFull = fullfile(outpath,sprintf('SoundSpeed_%s.txt',datestr(now,'yyyymmdd_HHMMSS')));
    handles.outfile_txt.UserData = outfileFull;
    handles.outfile_txt.String = dirname;

% --- Executes on button press in cancel_button.
function cancel_button_Callback(hObject, eventdata, handles)
    close(handles.RavenSoundSpeedCalculator)
    
% ---
function help_menu_Callback(hObject, eventdata, handles)
    open(fullfile(fileparts(mfilename('fullpath')), '+util', 'help.html'))

% --- Executes on button press in run_button.
function run_button_Callback(hObject, eventdata, handles)

    % initializations
    import util.*
    infile = handles.infile_list.String;
    inpath = handles.infile_list.UserData;
    infileFull = fullfile(inpath,infile);
    outfileFull = handles.outfile_txt.UserData;
    description = handles.description_txt.String;
    timeCorrectionList = handles.time_correction.String;
    timeCorrectionIdx = handles.time_correction.Value;
    timeCorrection = timeCorrectionList{timeCorrectionIdx};
    zone = handles.time_zone.String;
    
    % function calls
    [time, temp, sal, numSensorsUsed] = CTDlogReader(infileFull);
    if numSensorsUsed == 0
        return;
    end
    [soundSpeed, soundSpeedUncertainty] = calcSoundSpeed(temp, sal);
    time3 = correctTime(time, timeCorrection);
    writeSoundSpeed(infileFull,outfileFull,time3,zone,soundSpeed, ...
      soundSpeedUncertainty,numSensorsUsed,description,timeCorrection)
    
    % all done
    msgbox('Sound speed document written', 'Success');
