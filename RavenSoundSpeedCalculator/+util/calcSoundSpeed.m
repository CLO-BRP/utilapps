function [meanSoundSpeed, soundSpeedUncertainty] = calcSoundSpeed(temp, sal)
% % function [time, soundSpeed, soundSpeedUncertainty] = calcSoundSpeed(timeC, tempC, salC)

% Calculate sound speed using 
%   input:  temp - temperatures
%           sal  - salinities
%   output: soundSpeed - speeds of sound

    %---
    % Calculate sound speed (m/s) based on temperature (�C) and salinity (psu)
    %---
    soundSpeed = 1449.2 ...
                    + 4.6 .* temp ...
                    - 0.055 .* temp .^ 2 ...
                    + 0.00029 .* temp .^ 3 ...
                    + (1.34 - 0.01 .* temp) .* (sal - 35);
    meanSoundSpeed = mean(soundSpeed, 2, 'omitnan'); %ignore NaN in mean calculations
    soundSpeedUncertainty = meanSoundSpeed .* 0.005;    
