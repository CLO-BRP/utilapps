function dt = calcRealClockTime(C)
%
% Description:
%   Calculate real clock time of the beginning of each event
%
% Inputs:
%   C - selection table(cell array of char vectors)
%
% Outputs:
%   beginDatetime - begin date-time for each event (numeric vector)
%
% History
%   msp2  Aug-15-2017   Initial

% initializations
% % import utilities.*
% % dt = [];

% calculations
if size(C, 1) > 1
    fileDateTemplate = {'_\d\d\d\d\d\d\d\d_\d\d\d\d\d\dZ' ;
                        '_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d' ;
                        '_\d\d\d\d\d\d_\d\d\d\d\d\d' ;
                        '\d\d\d\d\d\d\d\dT\d\d\d\d\d\dp' ;
                        '.\d\d\d\d\d\d\d\dT\d\d\d\d\d\dZ'};
    fileDateFormat = {'_yyyymmdd_HHMMSSZ' 
                      '_yyyymmdd_HHMMSS';
                      '_yymmdd_HHMMSS';
                      'yyyymmddTHHMMSSp';
                      '.yyyymmddTHHMMSSZ'};
% %     secPerDay = 86400;
% %     beginFile = get_field(C, 'Begin File');
    dt = {''};
    i = 0;
    while i<length(fileDateTemplate) && any(cellfun(@isempty, dt))
        i = i + 1;
        dt = regexp(C, fileDateTemplate{i}, 'match');
    end
    if i<=length(fileDateTemplate)
        dt = cellfun(@(x) x{1}, dt, 'UniformOutput', false);
        dt = datenum(dt, fileDateFormat{i});
% %         fileOffsetDT = str2double(get_field(C, 'File Offset (s)')) ./ secPerDay;
% %         dt = beginFileDT + fileOffsetDT;
% %         if strcmp(fileDateTemplate{i}(end),'p')
% %             fracSec = regexp(beginFile, 'p\d\d\d\d\d\dZ', 'match');
% %             fracSec = cellfun(@(x) x{1}, fracSec, 'UniformOutput', false);
% %             fracSec = str2double(regexprep(fracSec,{'p', 'Z'},''));
% %             fracSec = fracSec ./ 10^6 ./ (24*60*60); 
% %             dt = dt + fracSec;
% %         end
    end
end