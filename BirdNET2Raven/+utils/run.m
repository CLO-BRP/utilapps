function [] = run(app)

fprintf([...
'TO DO\n'...
'1. Add option to write output to single folder.\n'...
'2. Add option to append selection tables into one table.\n'...
'3. Add option to process multiple sound streams like BirdNET V1.0?\n'...
'4. Add code to process max table size option?\n'...
'5. Add code to subset selection tables by species?\n'...
'6. Add option to filter by score threshold?\n'...
'7. Add option to process multiple deployments in one step. (Add loop to cycle through 1st level directories for sounds and birdnet output.)\n'...
'8. multi-thread (see DTD re: numbering selections, etc\n'...
'\n'...
'TESTING for alpha\n'...
'1. Release to Dan, Larissa, and Ben G for testing.\n'...
'2. Release to Yang Center staff for testing. Mention Selection Table App.\n'...
'\n'...
'COMPILING CODE\n'...
'1. Compile with console option so error messages are visible.\n'...
'2. Should we set up a testing process so we can share compiled code externally?\n\n'...
]);

% initializations
clc
tic
s = 0;
kk = 86400;
rng('shuffle')
set(0,'defaulttextInterpreter','none')
import utils.*
inpathSound = char(app.inSoundPath.UserData);
inpathTable = char(app.inTablePath.UserData);
outpathRoot = char(app.outPath.UserData);
outfileRoot = char(app.inSoundPath.Value);
outpathRootST = fullfile(outpathRoot,outfileRoot);
scorethreshold = app.ScoreEditField.Value;
if ~app.EnableCheckBox.Value
    scorethreshold = 0;
end
maxTableSize = app.MaxTableSizeEditField.Value;
switch app.ByMaxTableSizeButtonGroup.SelectedObject.Tag
    case 'subdivideNo'
        subdivide = false;
        randomize = false;
    case 'subdivideSequentially'
        subdivide = true;
        randomize = false;
    case 'subdivideRandomly'
        subdivide = true;
        randomize = true;
end
switch app.BySpeciesButtonGroup.SelectedObject.Tag
    case 'subdivideSppNo'
        bySpecies = false;
    case 'subdivideSppYes'
        bySpecies = true;
end

%waitbar
f = waitbar(0);
f.Name = 'BirdNET 2 Raven';
f.Position(4) = f.Position(4) + 25;
oh = findall(f);
for i = 1:length(oh)
     try
         oh(i).Units = 'normalized';
     catch
     end
end
f.Units = 'pixels';
f.Position(3) = f.Position(3) + 600;
movegui(f, 'center')
waitbar(0,f,sprintf('Reading GUI\n\n'));

% verify valid paths
if ~isfolder(inpathSound)
    h = warndlg('Sound file root directory not found','WARNING');
    uiwait(h)
    return;
end
if ~isfolder(inpathTable)
    h = warndlg('BirdNET table root directory not found','WARNING');
    uiwait(h)
    return;
end
if ~isfolder(outpathRoot)
    h = warndlg('Output directory not found','WARNING');
    uiwait(h)
    return;
end

% recursive search of file names
if ~isempty(fastDir(outpathRoot, true))
    h = warndlg('Output directory must be empty','WARNING');
    uiwait(h)
    return;
end
stFullIn = fastDir(inpathTable,true,'txt');
if isempty(stFullIn)
    h = warndlg('No BirdNET tables found','WARNING');
    uiwait(h)
    return;
end
soundfileFull = [fastDir(inpathSound,true,'flac');fastDir(inpathSound,true,'wav');fastDir(inpathSound,true,'aif')];
if isempty(soundfileFull)
    h = warndlg('No sound files found','WARNING');
    uiwait(h)
    return;
end
[~,soundfile,ext] = fileparts(soundfileFull);
soundfile = strcat(soundfile,ext);
fprintf('%s: reading GUI\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;

% find sound file durations
waitbar(0.2,f,'Finding sound file duration.')
fnListfile = sprintf('%s.listfile.txt',outfileRoot);
fnListfileFull = fullfile(outpathRoot,fnListfile);
info = cellfun(@audioinfo,soundfileFull); %%%%% TAKES A LONG TIME!!! %%%%%%
Fs = [info.SampleRate];
assert(all(Fs == Fs(1)), 'Sample rate of sound files must be the same')
nbits = [info.BitsPerSample];
assert(all(nbits == nbits(1)), 'Bit depth of sound files must be the same')
nChannels = [info.NumChannels];
assert(all(nChannels == nChannels(1)), 'Number of channels in sound files must be the same')
sizeinfo = [info.TotalSamples];
cumDur = cumsum(sizeinfo) ./ Fs;
fprintf('%s: find sound file duration\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;

% write listfile
waitbar(0.3,f,'Writing listfile.')
writecell(soundfileFull,fnListfileFull)
fprintf('%s: writing listfile\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;

% Copy BirdNET output to output directory
waitbar(0.4, f, 'Copying original selection tables to output folder.')
copyfile2(stFullIn,outpathRootST)
stFullOut = fastDir(outpathRootST,true,'txt');
fprintf('%s: copying selection tables\n',datestr((toc-s)/kk,'HH:MM:SS')); s=toc;

% for each selection table output
    for i = 1:length(stFullOut)

        % Read selection table
        C = read_selection_table(stFullOut{i});
        [~,n] = size(C);
        
        % Add a Begin File and File Offset fields
        headers = C(1,:);
        idx = find(ismember(headers,'Begin Path'),1);
        if isempty(idx)
            idx = n+1;
            C{1,idx} = 'Begin Path';
        end
        C(2:end,idx) = soundfileFull(i);
        [~,n] = size(C);
        idx = find(ismember(headers,'File Offset (s)'),1);
        if isempty(idx)
            idx = n+1;
            idx2 = find(ismember(headers,'Begin Time (s)'),1);
            C(:,idx) = C(:,idx2);
            C{1,idx} = 'File Offset (s)';
        end

        % Update Begin File and End File values for use in file stream
        C = migrateTable(C,soundfile,cumDur);

        % Write selection table
        write_selection_table(C, stFullOut{i});
    end

% Merge if requested

% Subdivide if requested

% See other input parameters

waitbar(1,f,sprintf('Done processing\n\nRun time: %s\n',datestr((toc)/86400,'HH:MM:SS')))
% % fprintf('%s TOTAL RUN TIME\n',datestr((toc)/kk,'HH:MM:SS'));

end
