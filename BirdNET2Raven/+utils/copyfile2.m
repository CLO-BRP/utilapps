function copyfile2(inFile, outFile)
%
%   Copy files to specified directory.
%
%       Inputs
%           inFile: full path of files to be merged (cell vector)
%           outFile: full path of merged output file (char)

if ~isfolder(outFile)
    mkdir(outFile)
end
if ispc
    fun = {'copy'};
else
    fun = {'cp'};
end
fun(1:length(inFile),1) = fun;
outFileC(1:length(inFile),1) = {outFile};
cmd = cellfun(@(x,y,z) sprintf(...
    '%s "%s" "%s"', x, y,z), fun, inFile, outFileC, 'UniformOutput', false);
[status,msg] = cellfun(@(x) dos(x), cmd, 'UniformOutput', false);
if any(cell2mat(status))
    txt = '';
    for i = 1:length(msg)
        if status{i}
            txt = [txt ; sprintf('%s\n   %s\n',msg{i}(1:end-2),inFile{i})];
        end
    end
    error('WARNING:\n%s',txt')
end

