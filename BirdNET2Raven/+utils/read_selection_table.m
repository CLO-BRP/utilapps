function C = read_selection_table(inPathFull)
% % function [C,stdViews] = read_selection_table(inPathFull,stdViews,mergeEnable)
%
% Description:
%   Reads specified Raven seletion table and returns a table variable.
%
% Usage:
%   read_selection_table
%
% Inputs:
%   inPathFull - full path of selection table (char)
%   stdViews - views found in first selection table read (cell vector of strings)
%   mergeEnable - merge selection table feature enabled (logical)
% Outputs:
%   C - selection table (cell array of char vectors)
%   stdViews - views found in first selection table read
%
% History
%   msp2  Aug-15-2017   Initial

% % %---
% % % Initialiations
% % %---
% % import utilities.*

%---
% Read selection table
%---

% Open selection table text read mode
fid = fopen(inPathFull,'rt');
assert(~isequal(fid,-1), 'Selection table could not be read:\n  %s\n',inPathFull);

% Parse selection table by cell into cell vector
C = textscan(fid,'%s','Delimiter','\n');

% Close selection table
fclose(fid);

%---
% Reshape cell vector into cell array
%---
C = regexp(C{1},'\t','split');
m = size(C,1);
n = size(C{1,:},2);
try
    C = reshape([C{:,:}],[n,m])';
catch ME
    
    % If any rows in selection table have number of columns different than the header
    if strcmp(ME.identifier, 'MATLAB:getReshapeDims:notSameNumel')
        
        % Delete those rows
        [~, fn, ext] = fileparts(inPathFull);
        fnExt = [fn , ext];
        numCols = cellfun(@length, C);
        errIDX = find(numCols ~= n);
        C(errIDX) = [];
        m = size(C,1);
        
        % Warn the user
        errIDXstr = sprintf('%.0f, ', errIDX);
        t1 = sprintf('Lines deleted in\n  "%s"\n', fnExt);
        t2 = sprintf('because the number of columns is different than that of the header: %s', errIDXstr);
        fail([t1 , t2])
    end    
    C = reshape([C{:,:}],[n,m])';
end

% % %---
% % % Test that Raven views are the same for all selection tables
% % %---
% % if isempty(stdViews)
% %     view = get_field(C, 'View');
% %     uniqueViews = unique(view)';
% %     stdViews = uniqueViews;
% % end
% % if mergeEnable
% %     view = get_field(C, 'View');
% %     uniqueViews = unique(view)';
% %     pattern = 'Selection Spectrum \d+|Spectrogram |Spectrogram \d+|Spectrogram Slice \d+|Waveform \d+';
% % 
% %     % test if there are any views that are not Raven-compliant
% %     badViews = cellfun(@isempty,regexp(uniqueViews,pattern));
% % 
% %     assert(~any(badViews),...
% %         'Selection table has view(s) not allowed by Raven\n    "%s"\n',...
% %         uniqueViews{badViews})
% % 
% %     % if not first selection table read, test views same as first selection table
% %     if ~isempty(stdViews)
% %         newViews = uniqueViews(~ismember(uniqueViews,stdViews));
% %         assert(isempty(newViews),...
% %           'Selection table has view(s) not found in previous selection tables\n    "%s"\n',...
% %           newViews{:})
% %     end
end