function writePlots(M,sched,outpath,C,tagList)
% Writes figures with callcounts and presence
%   Input
%     M: callcount matrix <numDays  x numChans>
%     sched: site, deploy date, retrieve date, begin analysis date, and 
%             end analysis date for each deployment (1x5 cell array)
%     outpath: path to write figures to
%     C: cell vector where each cell is a callcount matrixes for one of 
%        the unique values of tagName<#channels x #days x #hrsPerDay>
%     tagList: list of tags present in the "tagName" field in Raven selection tables 
%          (<nx1> cell)

%---
% Initializations
%---

import utilities.mtit

% Channels
chan = sched{3};
numChan = max(chan);
startDate = sched{4};
stopDate = sched{5};
firstDay = datetime(min(startDate),'ConvertFrom','datenum');
lastDay = datetime(max(stopDate),'ConvertFrom','datenum');
numDays = caldays(between(firstDay,lastDay,'days')) + 1;
numHrs = numDays*24;

% Unique site names in channel order
site = sched{2};
uniqueSite = cell(1,numChan);
for i = 1:numChan
    siteForChan = site(chan==i);
	if ~isempty(siteForChan)
        uniqueSite(i) = siteForChan(1);
	end
end

%---
% Cumulative Hours per Week
%---
%%
% Find hourly presence
PresenceHourly = NaN(numDays*24,numChan);
idx = 1;
for i = 1:numDays
    for j = 1:24
        currCount = M(i,:,j);
        currPresence = double(M(i,:,j) > 0);
        currPresence(isnan(currCount)) = NaN;
        PresenceHourly(idx,:) = currPresence;
        idx = idx + 1;
    end
end

% Find weekly presence
numWeeks = ceil(numDays/7);
hrsPerWeek = NaN(numWeeks,numChan);
effortPerWeek = zeros(numWeeks,numChan);
inc = 24*7; % hours per week;
for i = 1:numWeeks
    currMax = min(i*inc,numHrs);
    currWeek = sum(PresenceHourly((i-1)*inc+1:currMax,:),1,'omitnan');
    currWeek(all(isnan(PresenceHourly((i-1)*inc+1:currMax,:)),1)) = NaN;
    hrsPerWeek(i,:) = currWeek;
    currEffort = sum(~isnan(PresenceHourly((i-1)*inc+1:currMax,:)),1);
    effortPerWeek(i,:) = currEffort;
end
hrsPerWeek_allChan = sum(hrsPerWeek,2,'omitnan');
hrsPerWeek_allChan(all(isnan(hrsPerWeek),2)) = NaN;

% Summary Plot
datesWeekly =  firstDay + calweeks(0:numWeeks-1);
fh = figure('Color',[1,1,1]);
ax = axes(fh);
bar(ax,datesWeekly,hrsPerWeek_allChan,1,'k','EdgeColor','w')
ax.TickDir = 'out';
ax.YLim = [0 max(hrsPerWeek_allChan)*1.15];
ax.Title.String = 'Weekly Presence - All Sites';
ax.YLabel.String = 'Site - Hrs / Wk';
xtickformat('MMM dd, yyyy')
ax.XTickLabelRotation = 30;
ax.XLim = [datesWeekly(1)-calweeks(1), datesWeekly(end)+calweeks(1)];
ax.XTick = firstDay + calweeks(0:4:numWeeks+1);
ax.XLabel.String = 'Week Beginning';
fnFull = fullfile(outpath,...
    sprintf('WeeklyHoursWithPresence-SUMMARY_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,fnFull);

%---
% Cumulative Hours per Week for each site
%---
%%
screenSize = get(0,'ScreenSize');
shim = 35 / screenSize(4);
ax = NaN(numChan,1);
fh = figure('Color',[1,1,1],'Position',screenSize);
setappdata(fh, 'SubplotDefaultAxesLocation', [0.075,0.0375,0.8375,0.9])
grey = [0.9,0.9,0.9];
grey2 = [0.6,0.6,0.6];
ylim = [0 max(max(hrsPerWeek))*1.15];
for i = 1:numChan
    
    % Plot number of hour per week presence
    currAx = subplot(numChan,1,i);
    yyaxis left
    bar(currAx,datesWeekly,hrsPerWeek(:,i),1,'k','EdgeColor','w')
    hold on;
    currAx.TickDir = 'out';
    currAx.YLabel.String = sprintf('Presence\n(h / wk)\nSite %s\n',uniqueSite{i});
    xtickformat('MMM dd, yyyy')
    currAx.XLim = [datesWeekly(1)-calweeks(1), datesWeekly(end)+calweeks(1)];
    currAx.XTick = firstDay + calweeks(0:4:numWeeks+1);
    currAx.XLabel.String = 'Week Beginning';
    if ~isequal(i,numChan)
        currAx.XTickLabel = [];
    end
    currAx.Position(4) = currAx.Position(4) + shim;
    currAx.YLim = ylim;
    currAx.YColor = 'k';
 
    % Plot recording effort
    currEffort = effortPerWeek(:,i);
    noEffort = 100 .* (currEffort == 0);
    yyaxis right
    bar(currAx,datesWeekly,noEffort,1,'FaceColor',grey,'EdgeColor',grey)
    currAx.YLabel.String = sprintf('Coverage\n(%% h / wk)');
    currAx.XLim = xlim;
    currAx.YLim = [0,115];
    currAx.YColor = 'k';
        
    % Plot proportion of effort for each week, if less that 1
    currEffort2 = 100 .* currEffort ./ inc;
    currEffort2(currEffort2==0 | currEffort2==100) = NaN;
    plot(currAx,datesWeekly,currEffort2,'o',...
        'MarkerSize',3,'MarkerFaceColor',grey2,'MarkerEdgeColor',grey2);
    currAx.YLim = [0,115];
    ax(i) = currAx;
    hold off;
end
mtit(fh,'Weekly Hours with Presence','xoff',0,'yoff',0.01);
fnFull = fullfile(outpath,...
    sprintf('WeeklyHoursWithPresence-Sites_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,fnFull);

%---
% Cumulative Days per Week
%---
%%
% Find hourly presence
PresenceDaily = NaN(numDays,numChan);
idx = 1;
for i = 1:numDays
        currCount = sum(M(i,:,:),3);
        currPresence = double(currCount > 0);
        currPresence(isnan(currCount)) = NaN;
        PresenceDaily(idx,:) = currPresence;
        idx = idx + 1;
end

% Find weekly presence
numWeeks = ceil(numDays/7);
daysPerWeek = NaN(numWeeks,numChan);
effortPerWeek = zeros(numWeeks,numChan);
inc = 7; % days per week;
for i = 1:numWeeks
    currMax = min(i*inc,numDays);
    currWeek = sum(PresenceDaily((i-1)*inc+1:currMax,:),1,'omitnan');
    currWeek(all(isnan(PresenceDaily((i-1)*inc+1:currMax,:)),1)) = NaN;
    daysPerWeek(i,:) = currWeek;
    currEffort = sum(~isnan(PresenceDaily((i-1)*inc+1:currMax,:)),1);
    effortPerWeek(i,:) = currEffort;
end
daysPerWeek_allChan = sum(daysPerWeek,2,'omitnan');
daysPerWeek_allChan(all(isnan(daysPerWeek),2)) = NaN;

% Summary Plot
datesWeekly =  firstDay + calweeks(0:numWeeks-1);
fh = figure('Color',[1,1,1]);
ax = axes(fh);
bar(ax,datesWeekly,daysPerWeek_allChan ,1,'k','EdgeColor','w')
ax.TickDir = 'out';
ax.YLim = [0 max(daysPerWeek_allChan)*1.15];
ax.Title.String = 'Weekly Presence - All Sites';
ax.YLabel.String = 'Site - Days / Wk';
xtickformat('MMM dd, yyyy')
ax.XTickLabelRotation = 30;
ax.XLim = [datesWeekly(1)-calweeks(1), datesWeekly(end)+calweeks(1)];
ax.XTick = firstDay + calweeks(0:4:numWeeks+1);
ax.XLabel.String = 'Week Beginning';
fnFull = fullfile(outpath,...
    sprintf('WeeklyDaysWithPresence-SUMMARY_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,fnFull);

%---
% Cumulative Days per Week for each site
%---
%%
screenSize = get(0,'ScreenSize');
shim = 35 / screenSize(4);
ax = NaN(numChan,1);
fh = figure('Color',[1,1,1],'Position',screenSize);
setappdata(fh, 'SubplotDefaultAxesLocation', [0.075,0.0375,0.8375,0.9])
grey = [0.9,0.9,0.9];
grey2 = [0.6,0.6,0.6];
ylim = [0 7];
for i = 1:numChan

    % Initialize axes
    currAx = subplot(numChan,1,i);

    % Plot recording effort
    currEffort = effortPerWeek(:,i);
    currEffort2 = inc - currEffort;
    bar(currAx,datesWeekly,currEffort2,1,'FaceColor',grey,'EdgeColor',grey)
    hold on;
    currAx.YLabel.String = sprintf('Coverage\n(%% d / wk)');
    currAx.XLim = xlim;
    currAx.YLim = ylim;
    currAx.YColor = 'k';

    % Plot proportion of effort for each week, if less that 1
    currEffort2 = 100 .* currEffort ./ inc;
    currEffort2(currEffort2==0 | currEffort2==100) = NaN;
    plot(currAx,datesWeekly,currEffort2,'o',...
        'MarkerSize',3,'MarkerFaceColor',grey2,'MarkerEdgeColor',grey2);
    currAx.YLim = [0,115];


    % Plot number of hour per week presence
    bar(currAx,datesWeekly,daysPerWeek(:,i),0.6,'k')
    currAx.TickDir = 'out';
    currAx.YLabel.String = sprintf('Presence\n(d / wk)\nSite %s\n',uniqueSite{i});
    xtickformat('MMM dd, yyyy')
    currAx.XLim = [datesWeekly(1)-calweeks(1), datesWeekly(end)+calweeks(1)];
    currAx.XTick = firstDay + calweeks(0:4:numWeeks+1);
    currAx.XLabel.String = 'Week Beginning';
    if ~isequal(i,numChan)
        currAx.XTickLabel = [];
    end
    currAx.Position(4) = currAx.Position(4) + shim;
    currAx.YLim = ylim;
    currAx.YColor = 'k';
    ax(i) = currAx;
    hold off;
end
mtit(fh,'Weekly Days with Presence','xoff',0,'yoff',0.01);
fnFull = fullfile(outpath,...
    sprintf('WeeklyDaysWithPresence-Sites_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,fnFull);

%---
% Monthly cumulative channel-daily presence as a percent of coverage
%---
%%

% Calculate daily presence
Mday = sum(M,3,'omitnan');
Mday = double(Mday > 0);
Mday(all(isnan(M),3)) = NaN;

% Make month vector
uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
numMonths = length(uniqueMonths);

% Count days with presence each month
monthOfDay = cellstr(datestr(firstDay:lastDay,'mmm yyyy'));
presencePct = NaN(1,numMonths);
for i = 1:length(uniqueMonths)
    currMonth = uniqueMonths{i};
    dayIDX = strcmp(monthOfDay,currMonth);
    M2month = Mday(dayIDX,:);
    
    % #days with presence each month
    rowsum = sum(M2month,2,'omitnan');
    rowsum(sum(isnan(M2month),2)==numChan) = NaN;
    if all(isnan(rowsum))
        daysWpresence = NaN;
    else
        daysWpresence = sum(rowsum>0,'omitnan');
    end
    daysWcoverage = sum(~isnan(rowsum));
    presencePct(i) = 100*daysWpresence/daysWcoverage;
end

% Plot monthly presence as precent of site-days observed
fh2 = figure('Color',[1,1,1]);
setappdata(fh2, 'SubplotDefaultAxesLocation', [0.075,0.1,0.8375,0.8])
ax2 = axes(fh2);
datesMonthly =  firstDay + calmonths(0:numMonths-1);
bar(ax2,datesMonthly,presencePct);
xtickformat('MMM yyyy')
ax2.TickLength = [0,0];
ax2.YLabel.String = sprintf('Presence\n(%% site - days observed)');
ax2.Title.String = 'Monthly Presence';
ylim = [0 max(max(presencePct))*1.15];
ax2.YLim = ylim;
fnFull = fullfile(outpath,...
    sprintf('MonthlyPresence-Sites_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh2,fnFull);

%---
% Monthly cumulative channel-daily presence as a percent of coverage by tag
%---
%% Initializations
if ~isempty(tagList)
    numTags = length(tagList);
    uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
    numMonths = length(uniqueMonths);
    presencePct = NaN(numMonths,numTags);

    % for each tag
    for j = 1:numTags

        % Set up for tag
        M = C{j};

        % Calculate daily presence
        Mday = sum(M,3,'omitnan');
        Mday = double(Mday > 0);
        Mday(all(isnan(M),3)) = NaN;

        % Make month vector
        % Count days with presence each month
        monthOfDay = cellstr(datestr(firstDay:lastDay,'mmm yyyy'));
        for i = 1:length(uniqueMonths)
            currMonth = uniqueMonths{i};
            dayIDX = strcmp(monthOfDay,currMonth);
            M2month = Mday(dayIDX,:);

            % #days with presence each month
            rowsum = sum(M2month,2,'omitnan');
            rowsum(sum(isnan(M2month),2)==numChan) = NaN;
            if all(isnan(rowsum))
                daysWpresence = NaN;
            else
                daysWpresence = sum(rowsum>0,'omitnan');
            end
            daysWcoverage = sum(~isnan(rowsum));
            presencePct(i,j) = 100*daysWpresence/daysWcoverage;
        end
    end

    % Plot monthly presence as precent of site-days observed by tag
    fh = figure('Color',[1,1,1]);
    setappdata(fh, 'SubplotDefaultAxesLocation', [0.075,0.0375,0.8375,0.9])
    ax = axes(fh);
    datesMonthly =  firstDay + calmonths(0:numMonths-1);
    bar(ax,datesMonthly,presencePct);
    xtickformat('MMM yyyy')
    ax.TickLength = [0,0];
    ax.YLabel.String = sprintf('Presence\n(%% site - days observed)');
    ax.Title.String = 'Monthly Presence - All Sites';
    ax.YLim = [0 max(max(presencePct))*1.15];
    legend(ax,tagList)
    fnFull = fullfile(outpath,...
        sprintf('MonthlyPresence-Sites_by_Sp_%s',datestr(now,'yyyymmdd_HHMMSS')));
    saveas(fh,fnFull);
end

%---
% Cumulative Days per Month for each site
%---
%%


datesMonthly =  firstDay + calmonths(0:numMonths-1);

% Calculate daily presence
Mday = sum(M,3,'omitnan');
Mday = double(Mday > 0);
Mday(all(isnan(M),3)) = NaN;

% Make month vector
uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
yr = str2double(cellstr(datestr(datenum(uniqueMonths,'mmm yyyy'),'yyyy')));
mo = str2double(cellstr(datestr(datenum(uniqueMonths,'mmm yyyy'),'mm')));
daysPerMonth = eomday(yr,mo);
numMonths = length(uniqueMonths);
monthlyPresence = NaN(numMonths,numChan);
monthlyCoverage = NaN(numMonths,numChan);

% Count days with presence each month
monthOfDay = cellstr(datestr(firstDay:lastDay,'mmm yyyy'));
for i = 1:length(uniqueMonths)
    currMonth = uniqueMonths{i};
    dayIDX = strcmp(monthOfDay,currMonth);
    monthlyPresence(i,:) = sum(Mday(dayIDX,:),1);
% %     eomday
    monthlyCoverage(i,:) = sum(~isnan(Mday(dayIDX,:)));
end
presencePct = 100*monthlyPresence ./ monthlyCoverage;
monthlyCoveragePct = 100 .* monthlyCoverage ./ daysPerMonth;
noEffort = monthlyCoveragePct == 0;
partialEffort = monthlyCoveragePct;
partialEffort(partialEffort==0 | partialEffort==100) = NaN;

screenSize = get(0,'ScreenSize');
shim = 35 / screenSize(4);
ax = NaN(numChan,1);
fh = figure('Color',[1,1,1],'Position',screenSize);
hold on;
setappdata(fh, 'SubplotDefaultAxesLocation', [0.075,0.0375,0.8375,0.9])
grey = [0.9,0.9,0.9];
grey2 = [0.6,0.6,0.6];
for i = 1:numChan
    
    % Plot number of hour per week presence
    currAx = subplot(numChan,1,i);
    yyaxis left
    bar(currAx,datesMonthly,presencePct(:,i),1,'k','EdgeColor','w')
    hold on;
    currAx.YLabel.String = sprintf('Presence\n(%% d / mo)\nSite %s\n',uniqueSite{i});
    xtickformat('MMM yyyy')
    currAx.TickLength = [0,0];
    currAx.XLim = xlim;
    currAx.XTick = firstDay + calweeks(0:4:numWeeks+1);
    if ~isequal(i,numChan)
        currAx.XTickLabel = [];
    end
    currAx.Position(4) = currAx.Position(4) + shim;
    currAx.YLim = ylim;
    currAx.YColor = 'k';
 
    % Plot recording effort
    yyaxis right
    bar(currAx,datesMonthly,noEffort(:,i),1,'FaceColor',grey,'EdgeColor','w')
    currAx.YLabel.String = sprintf('Coverage\n(%% d / mo)');
    currAx.XLim = xlim;
    currAx.YLim = [0,115];
    currAx.YColor = 'k';
        
    % Plot proportion of effort for each week, if less that 1
    plot(currAx,datesMonthly,partialEffort(:,i),'o',...
        'MarkerSize',3,'MarkerFaceColor',grey2,'MarkerEdgeColor',grey2);
    currAx.YLim = [0,115];
    ax(i) = currAx;
    hold off;
end
mtit(fh,'Monthly Percent of Days with Presence','xoff',0,'yoff',0.01);
fnFull = fullfile(outpath,...
    sprintf('MonthlyDaysWithPresence-Sites_%s',datestr(now,'yyyymmdd_HHMMSS')));
saveas(fh,fnFull);
