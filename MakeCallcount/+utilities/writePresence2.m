function writePresence2(M,inFilesFull,sched,outpath)
% Writes XLS files with callcount
%   Input
%     M: callcount matrix <numDays  x numChans>
%     inFilesFull - list of full paths to selection tables (nx1 cell array of strings)
%     sched - site, deploy date, retrieve date, begin analysis date, and 
%             end analysis date for each deployment (1x5 cell array)
%     outpath - path to write XLS to

%---
% Initializations
%---
%%
dep = cell2mat(sched{1});
site = sched{2};
chan = sched{3};
startDate = sched{4};
stopDate = sched{5};
numChan = max(chan);
uniqueSite = cell(1,numChan);
for i = 1:numChan
    siteForChan = site(chan==i);
	if ~isempty(siteForChan)
        uniqueSite(i) = siteForChan(1);
	end
end
firstDay = min(startDate);
lastDay = max(stopDate);
numDays = lastDay-firstDay+1;
numHrs = numDays*24;
sz = size(M);
[~,inFiles,ext] = cellfun(@fileparts,inFilesFull,'UniformOutput',false);
inFiles = strcat(inFiles,ext);
outfileName = fullfile(outpath,sprintf('Presence2_%s',[datestr(now,'yyyymmdd_HHMMSS'),'.xlsx']));
sheetIDX = 0;

%---
% Cumulative Hours per Week
%---
%%

% Find hourly presence
PresenceHourly = NaN(numDays*24,numChan);
idx = 1;
for i = 1:numDays
    for j = 1:24
        currCount = M(i,:,j);
        currPresence = double(M(i,:,j) > 0);
        currPresence(isnan(currCount)) = NaN;
        PresenceHourly(idx,:) = currPresence;
        idx = idx + 1;
    end
end

%---
% Cumulative Hours Per Week - Presence
%---

% Find cumulative hours presence per week
numWeeks = ceil(numDays/7);
inc = 24*7; % hours per week;
hrsPerWeek = NaN(numWeeks,numChan);
for i = 1:numWeeks
    currMax = min(i*inc,numHrs);
    currWeek = sum(PresenceHourly((i-1)*inc+1:currMax,:),1,'omitnan');
    currWeek(all(isnan(PresenceHourly((i-1)*inc+1:currMax,:)),1)) = NaN;
    hrsPerWeek(i,:) =currWeek;
end

% Add header
C = cell(numWeeks+8, sz(2)+2);
C(1,1) = {'CUMULATIVE HOURS PER WEEK'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,2) = {'PRESENCE'};

% Add channel column headers
C(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(7,1) = {'Week'};
C(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C(8:numWeeks+7, 2:end-1) = num2cell(hrsPerWeek);

% Add row and column sums
rowsum = sum(hrsPerWeek,2,'omitnan');
rowsum(all(isnan(hrsPerWeek),2)) = NaN;
colsum = sum(hrsPerWeek,1,'omitnan');
colsum(sum(isnan(hrsPerWeek),1)==numDays) = NaN;
C(8:numWeeks+7, end) = num2cell(rowsum);
C(8+numWeeks, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Hours Per Week - Coverage
%---

% Find cumulative hours coverage per week
hrsPerWeek2 = NaN(numWeeks,numChan);
for i = 1:numWeeks
    currMax = min(i*inc,numHrs);
    currWeek = sum(~isnan(PresenceHourly((i-1)*inc+1:currMax,:)),1,'omitnan');
    hrsPerWeek2(i,:) =currWeek;
end
hrsPerWeek2(hrsPerWeek2==0) = NaN;

% Add header
C2 = cell(numWeeks+8, sz(2)+2);
C2(5,2) = {'COVERAGE'};

% Add channel column headers
C2(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C2(7,1) = {'Week'};
C2(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C2(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C2(8:numWeeks+7, 2:end-1) = num2cell(hrsPerWeek2);

% Add row and column sums
rowsum = sum(hrsPerWeek2,2,'omitnan');
rowsum(all(isnan(hrsPerWeek2),2)) = NaN;
colsum = sum(hrsPerWeek2,1,'omitnan');
colsum(sum(isnan(hrsPerWeek2),1)==numDays) = NaN;
C2(8:numWeeks+7, end) = num2cell(rowsum);
C2(8+numWeeks, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Hours Per Week - Presence Proporation
%---

% Add header
C3 = cell(numWeeks+8, sz(2)+2);
C3(5,2) = {'PRESENCE PROPORTION'};

% Add channel column headers
C3(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), ''];
C3(7,1) = {'Week'};
C3(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C3(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C3(8:numWeeks+7, 2:end-1) = num2cell(hrsPerWeek ./ hrsPerWeek2);

% Concatenate presence and coverage tables
C = [C,C2,C3];

% Write output
xlswrite(outfileName,C,'HrPerWk');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);

%delete unused first worksheets
sheetIDX = sheetIDX + 1;
excel.deleteWorksheet(Excel,sheetIDX); %delete unused first worksheet

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 5;
width([1,numChan+3,numChan*2+5]) = 10;
excel.colWidth(Excel,sheetIDX,width)

% Center cells

cellIDX = false(m,n);
cellIDX(6:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(5:7,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(7:end,2+numChan) = true;
colorIDX(7:end,4+numChan*2) = true;
colorIDX(end,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Cumulative Days per Week
%---
%%

% Find hourly presence
PresenceDaily = NaN(numDays,numChan);
idx = 1;
for i = 1:numDays
        currCount = sum(M(i,:,:),3);
        currPresence = double(currCount > 0);
        currPresence(isnan(currCount)) = NaN;
        PresenceDaily(idx,:) = currPresence;
        idx = idx + 1;
end

%---
% Cumulative Hours Per Week - Presence
%---

% Find cumulative hours presence per week
numWeeks = ceil(numDays/7);
daysPerWeek = NaN(numWeeks,numChan);
for i = 1:numWeeks
    currMax = min(i*inc,numDays);
    currWeek = sum(PresenceDaily((i-1)*inc+1:currMax,:),1,'omitnan');
    currWeek(all(isnan(PresenceDaily((i-1)*inc+1:currMax,:)),1)) = NaN;
    daysPerWeek(i,:) = currWeek;
end

% Add header
C = cell(numWeeks+8, sz(2)+2);
C(1,1) = {'CUMULATIVE DAYS PER WEEK'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,2) = {'PRESENCE'};

% Add channel column headers
C(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(7,1) = {'Week'};
C(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C(8:numWeeks+7, 2:end-1) = num2cell(daysPerWeek);

% Add row and column sums
rowsum = sum(daysPerWeek,2,'omitnan');
rowsum(all(isnan(daysPerWeek),2)) = NaN;
colsum = sum(daysPerWeek,1,'omitnan');
colsum(sum(isnan(daysPerWeek),1)==numDays) = NaN;
C(8:numWeeks+7, end) = num2cell(rowsum);
C(8+numWeeks, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Days Per Week - Coverage
%---

% Find cumulative hours coverage per week
daysPerWeek2 = NaN(numWeeks,numChan);
for i = 1:numWeeks
    currMax = min(i*inc,numDays);
    currWeek = sum(~isnan(PresenceDaily((i-1)*inc+1:currMax,:)),1,'omitnan');
    daysPerWeek2(i,:) = currWeek;
end
daysPerWeek2(daysPerWeek2==0) = NaN;

% Add header
C2 = cell(numWeeks+8, sz(2)+2);
C2(5,2) = {'COVERAGE'};

% Add channel column headers
C2(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C2(7,1) = {'Week'};
C2(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C2(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C2(8:numWeeks+7, 2:end-1) = num2cell(daysPerWeek2);

% Add row and column sums
rowsum = sum(daysPerWeek2,2,'omitnan');
rowsum(all(isnan(daysPerWeek2),2)) = NaN;
colsum = sum(daysPerWeek2,1,'omitnan');
colsum(sum(isnan(daysPerWeek2),1)==numDays) = NaN;
C2(8:numWeeks+7, end) = num2cell(rowsum);
C2(8+numWeeks, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

%---
% Cumulative Hours Per Week - Presence Proporation
%---

% Add header
C3 = cell(numWeeks+8, sz(2)+2);
C3(5,2) = {'PRESENCE PROPORTION'};

% Add channel column headers
C3(6,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), ''];
C3(7,1) = {'Week'};
C3(7,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C3(8:numWeeks+7, 1) = cellstr(datestr(firstDay:7:lastDay,'mm/dd/yyyy'));

% Add body of table
C3(8:numWeeks+7, 2:end-1) = num2cell(daysPerWeek ./ daysPerWeek2);

% Concatenate presence and coverage tables
C = [C,C2,C3];

% Write output
xlswrite(outfileName,C,'DaysPerWk');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);

%delete unused first worksheets
sheetIDX = sheetIDX + 1;
excel.deleteWorksheet(Excel,sheetIDX); %delete unused first worksheet

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 5;
width([1,numChan+3,numChan*2+5]) = 10;
% % sheetIDX = 1;
excel.colWidth(Excel,sheetIDX,width)

% Center cells

cellIDX = false(m,n);
cellIDX(6:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(5:7,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(7:end,2+numChan) = true;
colorIDX(7:end,4+numChan*2) = true;
colorIDX(end,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% CONTEXT
%---
%%
% Add header
sz = size(sched);
C = cell(sz(1)+10, 5);
C(1,1) = {'CONTEXT'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,1) = {'Selection Tables Used:'};
len = length(inFiles);
C(6:len+5,2) = inFiles;
row = len + 7;
C(row,1) = {'----- Deployment Info -----'};
row = row + 1;
C(row,1:5) = {'Deployment',...
              'Site',...
              'Channel',...
              'Analysis Start',...
              'Analysis End'...
};
row = row + 1;
startDate(isnan(startDate)) = 0;
stopDate(isnan(stopDate)) = 0;
startDate2 =  cellstr(datestr(startDate,'mm/dd/yyyy'));
stopDate2 =  cellstr(datestr(stopDate,'mm/dd/yyyy'));
startDate2(strcmp(startDate2,'01/00/0000')) = {''};
stopDate2(strcmp(stopDate2,'01/00/0000')) = {''};

C(row:row-1+length(dep),1:5) = [
    num2cell(dep),...
    site,...
    num2cell(chan),...
    startDate2,...
    stopDate2...
];

% Write output
xlswrite(outfileName,C,'CONTEXT');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 12;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(10:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(1,1) = true;
boldIDX(len+7:len+8,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Save open worksheet
excel.close(Excel,Workbook)

