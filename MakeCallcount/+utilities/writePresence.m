function writePresence(M,inFilesFull,sched,outpath)
% Writes XLS files with callcount
%   Input
%     M: callcount matrix <numDays  x numChans>
%     inFilesFull - list of full paths to selection tables (nx1 cell array of strings)
%     sched - site, deploy date, retrieve date, begin analysis date, and 
%             end analysis date for each deployment (1x5 cell array)
%     outpath - path to write XLS to

%---
% Initializations
%---
%%
dep = sched{1};
site = sched{2};
chan = sched{3};
startDate = sched{4};
stopDate = sched{5};
numChan = max(chan);
uniqueSite = cell(1,numChan);
for i = 1:numChan
    siteForChan = site(chan==i);
	if ~isempty(siteForChan)
        uniqueSite(i) = siteForChan(1);
	end
end
firstDay = min(startDate);
lastDay = max(stopDate);
numDays = lastDay-firstDay+1;
sz = size(M);
[~,inFiles,ext] = cellfun(@fileparts,inFilesFull,'UniformOutput',false);
inFiles = strcat(inFiles,ext);
outfileName = fullfile(outpath,sprintf('Presence_%s',[datestr(now,'yyyymmdd_HHMMSS'),'.xlsx']));
sheetIDX = 0;

%---
% Hourly Presence
%---
%%
% Add header
C = cell(numDays*24+6, sz(2)+3);
C(1,1) = {'HOURLY PRESENCE'};
C(2,1) = {datestr(now)};
C(5,1:2) = {'Date','Hour'};

% Add channel column headers
C(4,3:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(5,3:end) = strsplit(chanStr(1:end),',');
idx = 6;

% Add date and time columns
day = (firstDay:lastDay)';
for i = 1:numDays
    currDate = {datestr(day(i),'mm/dd/yyyy')};
    for j = 1:24
        C(idx,1:2) = [currDate,j-1];
        idx = idx + 1;
    end
end

idx = 6;
for i = 1:numDays
    for j = 1:24
        currCount = M(i,:,j);
        currPresence = double(M(i,:,j) > 0);
        currPresence(isnan(currCount)) = NaN;
        C(idx,3:end-1) = num2cell(currPresence);
        idx = idx + 1;
    end
end

% Add row and column sums
MdayTime = cell2mat(C(6:5+numDays*24,3:end-1));
rowsum = sum(MdayTime,2,'omitnan');
rowsum(sum(isnan(MdayTime),2)==numChan) = NaN;
colsum = sum(MdayTime,1,'omitnan');
colsum(sum(isnan(MdayTime),1)==numDays) = NaN;
C(6:idx-1, end) = num2cell(rowsum);
C(idx, :) = [{'SUM'},{''},num2cell(colsum),sum(colsum)];

% Write output
xlswrite(outfileName,C,'Hourly Presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);

%delete unused first worksheets
sheetIDX = sheetIDX + 1;
excel.deleteWorksheet(Excel,sheetIDX); %delete unused first worksheet

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*4;
width(1) = 10;
sheetIDX = 1;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:m,:) = true;
try
    excel.alignCell(Excel,sheetIDX,cellIDX,'center')
catch
    fprintf(2,'\n\nWARNING: Unable to center cells in "Hourly Callcount" worksheet\n');
end

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4:5,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(6:end,3+numChan) = true;
colorIDX(end,:) = true;
try
    excel.fontColor(Excel,sheetIDX,colorIDX,color);
catch
    fprintf(2,'\n\nWARNING: Unable to set font color in "Hourly Callcount" worksheet\n');
end

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Daily Presence
%---
%%
% Add header
C = cell(sz(1)+6, sz(2)+2);
C(1,1) = {'DAILY PRESENCE'};
C(2,1) = {datestr(now)};

% Add channel column headers
C(4,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(5,2:end) = strsplit(chanStr(1:end),',');

% Add date and time column
C(6:sz(1)+5, 1) = cellstr(datestr(firstDay:lastDay,'mm/dd/yyyy'));

% Add body
Mday = sum(M,3,'omitnan');
Mday = double(Mday > 0);
Mday(all(isnan(M),3)) = NaN;
C(6:sz(1)+5, 2:end-1) = num2cell(Mday);

% Add row and column sums
rowsum = sum(Mday,2,'omitnan');
rowsum(sum(isnan(Mday),2)==numChan) = NaN;
colsum = sum(Mday,1,'omitnan');
colsum(sum(isnan(Mday),1)==numDays) = NaN;
C(6:sz(1)+5, end) = num2cell(rowsum);
C(sz(1)+6, :) = [{'SUM'},num2cell(colsum),sum(colsum)];

% Write output
xlswrite(outfileName,C,'Daily Presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*4;
width(1) = 10;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:m,:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4:5,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(6:end,2+numChan) = true;
colorIDX(end,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Monthly Presence
%---
%%
% Add header
C = cell(0);
C(1,1) = {'MONTHLY PRESENCE'};
C(2,1) = {datestr(now)};
C(4,1:7) = {...
    'Month',...
    sprintf('Presence\n(days)'),...
    sprintf('Coverage\n(days)'),...
    sprintf('Presence\n(%%days)'),...
    sprintf('Presence\n(site-days)'),...
    sprintf('Coverage\n(site-days)'),...
    sprintf('Presence\n(%%site-days)')...
};

% Add month column
uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
numMonths = length(uniqueMonths);
C(5:4+numMonths,1) = uniqueMonths;

% Count days with presence each month
M3 = NaN(numMonths,6);
monthOfDay = cellstr(datestr(firstDay:lastDay,'mmm yyyy'));
for i = 1:length(uniqueMonths)
    currMonth = uniqueMonths{i};
    dayIDX = strcmp(monthOfDay,currMonth);
    M2month = Mday(dayIDX,:);
    
    % #days with presence each month
    rowsum = sum(M2month,2,'omitnan');
    rowsum(sum(isnan(M2month),2)==numChan) = NaN;
    if all(isnan(rowsum))
        daysWpresence = NaN;
    else
        daysWpresence = sum(rowsum>0,'omitnan');
    end
    M3(i,1) = daysWpresence;
    daysWcoverage = sum(~isnan(rowsum));
    M3(i,2) = daysWcoverage;
    M3(i,3) = 100*daysWpresence/daysWcoverage;
    
    % #site-days with presence each month
    if all(isnan(M2month))
        siteDaysWpresence = NaN;
    else
        siteDaysWpresence = sum(sum(M2month,'omitnan'),'omitnan');
    end
    M3(i,4) = siteDaysWpresence;
    siteDaysWcoverage = sum(sum(~isnan(M2month)));
    M3(i,5) = siteDaysWcoverage;
    M3(i,6) = 100*siteDaysWpresence/siteDaysWcoverage;
end
C(5:4+numMonths,2:7) = num2cell(M3);

% Write output
xlswrite(outfileName,C,'Monthly Presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*9;
width(5:7) = 11;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:m,:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(4:end,[4,7]) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Number format for "%" columns
format = '0.00';
formatIDX = colorIDX;
excel.numberFormat(Excel,sheetIDX,formatIDX,format);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Site Presence
%---
%%
% Add header
C = cell(0);
C(1,1) = {'SITE PRESENCE'};
C(2,1) = {datestr(now)};
C(4,1:5) = {...
    'Site',...
    sprintf('Presence\n(days)'),...
    sprintf('Coverage\n(days)'),...
    sprintf('Presence\n(%%coverage days)'),...
    sprintf('Presence\n(%%presence days)')...
};
C(5:4+numChan,1) = uniqueSite';

% #days with presence at each site
M4 = NaN(numChan,4);
for i = 1:numChan
    M2site = Mday(:,i);
    colsum = sum(M2site,'omitnan');
    colsum(sum(isnan(M2site),1)==numDays) = NaN;
    if all(isnan(colsum))
        sitesWpresence = NaN;
    else
        sitesWpresence = sum(colsum,'omitnan');
    end
    M4(i,1) = sitesWpresence;
    sitesWcoverage = sum(~isnan(M2site));
    M4(i,2) = sitesWcoverage;
    M4(i,3) = 100*sitesWpresence/sitesWcoverage;
    M4(i,4) = sitesWpresence;
end
M4(:,4) = 100.*M4(:,4)./sum(M4(:,4),'omitnan');
C(5:4+numChan,2:5) = num2cell(M4);

% Write output
xlswrite(outfileName,C,'Site Presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*9;
width(4:5) = 11;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:m,:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(4:end,[4,5]) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Number format for "%" columns
format = '0.00';
formatIDX = colorIDX;
excel.numberFormat(Excel,sheetIDX,formatIDX,format);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Monthly-Site presence
%---
%%
% Add header
C = cell(sz(1)+7, sz(2)+2);
C(1,1) = {'MONTHLY-SITE PRESENCE'};
C(2,1) = {datestr(now)};
C(4,1) = {'----- PRESENCE ---------------------------------------------'};

%---
% Days with presence in each site-month
%---

% Add channel column headers
C(5,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(6,2:end) = strsplit(chanStr(1:end),',');

% Add month column
uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
numMonths = length(uniqueMonths);
row = 7;
C(row:row-1+numMonths,1) = uniqueMonths;
C(row+numMonths,1) = {'SUM'};

% Calculate #days with presence in each site-month
M5 = NaN(numMonths,numChan); % #days with presence in each site-month
M6 = NaN(numMonths,numChan); % #days analyzed in each site-month
for i = 1:length(uniqueMonths)
    currMonth = uniqueMonths{i};
    dayIDX = strcmp(monthOfDay,currMonth);
    for j = 1:numChan
        M2siteMonth = Mday(dayIDX,j);
        cellSum = sum(M2siteMonth,'omitnan');
        cellSum(all(isnan(M2siteMonth))) = NaN;
        M5(i,j) = cellSum;
        M6(i,j) = sum(~isnan(M2siteMonth));
    end
end
C(row:row-1+numMonths,2:numChan+1) = num2cell(M5);
C(row:row-1+numMonths,end) = num2cell(sum(M5,2,'omitnan')); %row sums
for i = 1:size(M5,1)
    if all(isnan(M5(i,:)))
        C(row+i-1,end) = num2cell(NaN); %row sum is NaN if all NaN in row
    end
end
row = row+numMonths;
C(row,2:end-1) = num2cell(sum(M5,1,'omitnan')); %col sums
for i = 1:size(M5,2)
    if all(isnan(M5(:,i)))
        C(row,i+1) = num2cell(NaN); %col sum is NaN if all NaN in row
    end
end
C(row,end) = num2cell(sum(sum(M5,'omitnan'),'omitnan'));
if all(all(isnan(M5)))
    C(row,end) = num2cell(NaN);
end
row = row + 3;

%---
% Days analyzed in each site-month
%---
C(row,1) = {'----- RECORD DAYS --------------------------------------------'};
row = row + 1;

% Add channel column headers
C(row,2:end-1) = uniqueSite;
row = row+1;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(row,2:end) = strsplit(chanStr(1:end),',');
row = row+1;

% Add month column
uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
numMonths = length(uniqueMonths);
C(row:row-1+numMonths,1) = uniqueMonths;
C(row+numMonths,1) = {'SUM'};

% Calculate #days coverage in each site-month
C(row:row-1+numMonths,2:numChan+1) = num2cell(M6);
C(row:row-1+numMonths,end) = num2cell(sum(M6,2,'omitnan')); %row sums
for i = 1:size(M6,1)
    if all(isnan(M6(i,:)))
        C(row+i-1,end) = num2cell(NaN); %row sum is NaN if all NaN in row
    end
end
row = row+numMonths;
C(row,2:end-1) = num2cell(sum(M6,1,'omitnan')); %col sums
for i = 1:size(M6,2)
    if all(isnan(M6(:,i)))
        C(row,i+1) = num2cell(NaN); %col sum is NaN if all NaN in row
    end
end
C(row,end) = num2cell(sum(sum(M6,'omitnan'),'omitnan'));
if all(all(isnan(M6)))
    C(row,end) = num2cell(NaN);
end
row = row+3;

%---
% Percent of analyzed days with presence in each site-month
%---
C(row,1) = {'----- DAYS WITH PRESENCE (% OF RECORD DAYS) ------------------'};
row = row + 1;

% Add channel column headers
C(row,2:end-1) = uniqueSite;
row = row + 1;
chanStr = sprintf('ch %.0f,',(1:numChan));
C(row,2:end) = strsplit(chanStr(1:end),',');
row = row + 1;

% Add month column
uniqueMonths = unique(cellstr(datestr(firstDay:lastDay,'mmm yyyy')),'stable');
numMonths = length(uniqueMonths);
C(row:row-1+numMonths,1) = uniqueMonths;
C(row:row-1+numMonths,2:numChan+1) = num2cell(100*M5./M6);
row = row+2+numMonths;

% Write output
xlswrite(outfileName,C,'Monthly-Site presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*6;
width(1) = 9;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX([5:numMonths+7,numMonths+11:2*numMonths+13,2*numMonths+17:end],:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX([4:6,(10:12)+numMonths,(16:18)+2*numMonths],:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(6:end,numChan+2) = true;
colorIDX([numMonths+7,2*numMonths+13],:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Number format for "%" columns
format = '0.00';
formatIDX = false(m,n);
formatIDX(2*numMonths+19:end,2:end-1) = true;
excel.numberFormat(Excel,sheetIDX,formatIDX,format);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Seasonal-Site presence
%---
%%
% Add header
C = cell(sz(1)+7, sz(2)+2);
C(1,1) = {'SEASONAL SITE PRESENCE'};
C(2,1) = {datestr(now)};

C(4,1) = {'----- PRESENCE ---------------------------------------------'};

%---
% Days with presence in each site-season
%---

% Add channel column headers
C(5,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(6,2:end) = strsplit(chanStr(1:end),',');
row = 7;

% Assign a season to each month
monthYear = split(unique(cellstr(datestr(firstDay:lastDay,'mm yyyy')),'stable'));
month = str2double(monthYear(:,1));
year = monthYear(:,2);
season = {'Fall'};
season(1:length(month),1) = season;
season(month < 10) = {'Summer'};
season(month < 7) = {'Spring'};
season(month < 4) = {'Winter'};
season = strcat(season,{' '},year);
uniqueSeason = unique(season,'stable');
numSeasons = length(uniqueSeason);

% Count days with presence
M7 = NaN(numSeasons,numChan);
M8 = NaN(numSeasons,numChan);
for i = 1:numSeasons
    seasonIDX = strcmp(uniqueSeason{i},season);
    sumSeason = sum(M5(seasonIDX,:),1,'omitnan');
    sumSeason(all(isnan(M5(seasonIDX,:)))) = NaN;
    M7(i,:) = sumSeason;
    
    sumSeason = sum(M6(seasonIDX,:),1,'omitnan');
    sumSeason(all(isnan(M6(seasonIDX,:)))) = NaN;
    M8(i,:) = sumSeason;
end

% Add days with presence to output
C(row:row-1+numSeasons,1) = uniqueSeason;
C(row:row-1+numSeasons,2:numChan+1) = num2cell(M7);

% Add row sums to output
sumSumSeason = sum(M7,2,'omitnan');
sumSumSeason(all(isnan(M7),2)) = NaN;
C(row:row-1+numSeasons,end) = num2cell(sumSumSeason);   

% Add column sums to output
row = row + numSeasons;
C(row,1) = {'SUM'};
chanSum = sum(M7,1,'omitnan');
chanSum(all(isnan(M7))) = NaN;
C(row,2:numChan+1) = num2cell(chanSum);
grandSum = sum(chanSum,'omitnan');
grandSum(all(isnan(chanSum))) = NaN;
C(row,end) = num2cell(grandSum);
row = row + 3;

%---
% Days with presence in each site-season
%---
C(row,1) = {'----- RECORD DAYS --------------------------------------------'};
row = row + 1;

% Add channel column headers
C(row,2:end-1) = uniqueSite;
row = row + 1;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(row,2:end) = strsplit(chanStr(1:end),',');
row = row + 1;

% Assign a season to each month
monthYear = split(unique(cellstr(datestr(firstDay:lastDay,'mm yyyy')),'stable'));
month = str2double(monthYear(:,1));
year = monthYear(:,2);
season = {'Fall'};
season(1:length(month),1) = season;
season(month < 10) = {'Summer'};
season(month < 7) = {'Spring'};
season(month < 4) = {'Winter'};
season = strcat(season,{' '},year);
uniqueSeason = unique(season,'stable');

% Add days with presence to output
C(row:row-1+numSeasons,1) = uniqueSeason;
C(row:row-1+numSeasons,2:numChan+1) = num2cell(M8);

% Add row sums to output
sumSumSeason = sum(M8,2,'omitnan');
sumSumSeason(all(isnan(M8),2)) = NaN;
C(row:row-1+numSeasons,end) = num2cell(sumSumSeason);  
row = row + numSeasons; 

% Add column sums to output
C(row,1) = {'SUM'};
chanSum = sum(M8,1,'omitnan');
chanSum(all(isnan(M8))) = NaN;
C(row,2:numChan+1) = num2cell(chanSum);
grandSum = sum(chanSum,'omitnan');
grandSum(all(isnan(chanSum))) = NaN;
C(row,end) = num2cell(grandSum);
row = row + 3;

%---
% Percent of analyzed days with presence in each site-season
%---
C(row,1) = {'----- DAYS WITH PRESENCE (% OF RECORD DAYS) ------------------'};
row = row + 1;

% Add channel column headers
C(row,2:end-1) = uniqueSite;
row = row + 1;
chanStr = sprintf('ch %.0f,',(1:numChan));
C(row,2:end) = strsplit(chanStr(1:end),',');
row = row + 1;

% Add month column
C(row:row-1+numSeasons,1) = uniqueSeason;
C(row:row-1+numSeasons,2:numChan+1) = num2cell(100*M7./M8);

% Write output
xlswrite(outfileName,C,'Seasonal Site Presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*6;
width(1) = 12;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX([5:numSeasons+7,numSeasons+11:2*numSeasons+13,2*numSeasons+17:end],:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX([4:6,(10:12)+numSeasons,(16:18)+2*numSeasons],:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(6:end,numChan+2) = true;
colorIDX([numSeasons+7,2*numSeasons+13],:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Number format for "%" columns
format = '0.00';
formatIDX = false(m,n);
formatIDX(2*numSeasons+19:end,2:end-1) = true;
excel.numberFormat(Excel,sheetIDX,formatIDX,format);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% Deployment presence
%---
%%
% Add header
C = cell(sz(1)+7, sz(2)+2);
C(1,1) = {'DEPLOYMENT PRESENCE'};
C(2,1) = {datestr(now)};
C(4,1:7) = {'Deployment',...
            sprintf('Presence\n(site-days)'),...
            sprintf('Coverage\n(site-days)'),...
            sprintf('Presence\n(%%site-days)'),...
            sprintf('Presence\n(days)'),...
            sprintf('Coverage\n(days)'),...
            sprintf('Presence\n(%%days)')...
};
dep = cell2mat(dep);
uniqueDep = unique(dep);
numDep = length(uniqueDep);
C(5:4+numDep,1) = num2cell(uniqueDep);
[m,n] = size(Mday);
presenceSiteDay = NaN(numDep,1);
coverageSiteDay = NaN(numDep,1);
presenceDay = NaN(numDep,1);
coverageDay = NaN(numDep,1);
day = firstDay:lastDay;
for i = 1:numDep
    depMask = false(m,n);
    depIDX = dep==uniqueDep(i);
    depChan = chan(depIDX);
    depStart = startDate(depIDX);
    depStop = stopDate(depIDX);
    for j = 1:length(depChan)
        currChan = depChan(j);
        dateIDX = day>=depStart(j) & day<=depStop(j);
        depMask(:,currChan) = dateIDX';
    end
    currM = Mday;
    currM(~depMask) = NaN;
    currPresenceSiteDay = sum(sum(currM,'omitnan'),'omitnan');
    currPresenceSiteDay(all(all(isnan(currM)))) = NaN;
    currCoverageSiteDay = sum(sum(~isnan(currM)));
    currPresenceDay = sum(sum(currM,2,'omitnan')>0);
    currPresenceDay(all(all(isnan(currM)))) = NaN;
    currCoverageDay = sum(sum(~isnan(currM),2)>1);
    
    presenceSiteDay(i,1) = currPresenceSiteDay;
    coverageSiteDay(i,1) = currCoverageSiteDay;    
    presenceDay(i,1) = currPresenceDay;
    coverageDay(i,1) = currCoverageDay;    
    
    C(4+i,2) = num2cell(currPresenceSiteDay);
    C(4+i,3) = num2cell(currCoverageSiteDay);
    C(4+i,4) = num2cell(100*currPresenceSiteDay/currCoverageSiteDay);
    C(4+i,5) = num2cell(currPresenceDay);
    C(4+i,6) = num2cell(currCoverageDay);
    C(4+i,7) = num2cell(100*currPresenceDay/currCoverageDay);
end
row = 5 + i;
C(row,1) = {'ALL SITES'};
sumPresenceSiteDay = sum(presenceSiteDay,'omitnan');
sumPresenceSiteDay(all(isnan(presenceSiteDay))) = NaN;
C(row,2) = num2cell(sumPresenceSiteDay);
sumCoverageSiteDay = sum(coverageSiteDay,'omitnan');
sumCoverageSiteDay(all(isnan(coverageSiteDay))) = NaN;
C(row,3) = num2cell(sumCoverageSiteDay);
C(row,4) = num2cell(100*sumPresenceSiteDay/sumCoverageSiteDay);

sumPresenceDay = sum(presenceDay,'omitnan');
sumPresenceDay(all(isnan(presenceDay))) = NaN;
C(row,5) = num2cell(sumPresenceDay);
sumCoverageDay = sum(coverageDay,'omitnan');
sumCoverageDay(all(isnan(coverageDay))) = NaN;
C(row,6) = num2cell(sumCoverageDay);
C(row,7) = num2cell(100*sumPresenceDay/sumCoverageDay);

% Write output
xlswrite(outfileName,C,'Deployment Presence');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*12;
width(1) = 12;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:end,:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(5:end,[4,7]) = true;
colorIDX(numDep+5,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Number format for "%" columns
format = '0.00';
formatIDX = false(m,n);
formatIDX(5:end,[4,7]) = true;
excel.numberFormat(Excel,sheetIDX,formatIDX,format);

% Save open worksheet
excel.close(Excel,Workbook)

%---
% CONTEXT
%---
%%

% Add header
sz = size(sched);
C = cell(sz(1)+10, 5);
C(1,1) = {'CONTEXT'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,1) = {'Selection Tables Used:'};
len = length(inFiles);
C(6:len+5,2) = inFiles;
row = len + 7;
C(row,1) = {'----- Deployment Info -----'};
row = row + 1;
C(row,1:5) = {'Deployment',...
              'Site',...
              'Channel',...
              'Analysis Start',...
              'Analysis End'...
};
row = row + 1;
startDate(isnan(startDate)) = 0;
stopDate(isnan(stopDate)) = 0;
startDate2 =  cellstr(datestr(startDate,'mm/dd/yyyy'));
stopDate2 =  cellstr(datestr(stopDate,'mm/dd/yyyy'));
startDate2(strcmp(startDate2,'01/00/0000')) = {''};
stopDate2(strcmp(stopDate2,'01/00/0000')) = {''};

C(row:row-1+length(dep),1:5) = [
    num2cell(dep),...
    site,...
    num2cell(chan),...
    startDate2,...
    stopDate2...
];

% Write output
xlswrite(outfileName,C,'CONTEXT');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 12;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(10:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(len+7:len+8,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Save open worksheet
excel.close(Excel,Workbook)

