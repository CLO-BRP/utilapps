function check_fields(C)
% Check if all required fields are present
%   Inputs
%     C - selection table (cell array of strings)

appName = 'Make Callcount';
assert(~isempty(C)&&iscell(C),appName,'Input file is not a selection table')
headers = C(1,:);
assert(~isempty(strcmp(headers,'Channel')),appName,'Channel column is missing from selection table')
assert(~(isempty(strcmp(headers,'Begin File'))&&isempty(strcmp(headers,'Begin Path'))),...
    appName,'Channel column is missing from selection table')