function assertMSP(status,name,msg)
% Similar to MATLAB "assert" function, but is easier to read for non-programmers
%
%   Inputs
%     status: false means error, true means no error
%     name: Name of software application
%     msg: Error message
    
    % If no error, don't do anything
    if status
        return;
    end

    % Put up warning dialog
    h = warndlg(msg,name);
    pos = get(h,'Position');
    set(h,'Position',[pos(1),pos(2),pos(3)*1.3,pos(4)*1.05])
    movegui(h,'center')
    th = findobj(h,'Tag','MessageBox');
    set(th,'FontSize',10,'FontName','Segue UI')

    % Trip error reporting
    bars = '--------------------------------------------------------------';
    if iscell(msg)
        msg = char(msg)';
    end
    ME.message = sprintf('\n%s\n%s\n%s\n',bars,msg,bars);
    ME.identifer = '';
    ME.stack.file = '';
    ME.stack.name = name;
    ME.stack.line = 1;
    error(ME)
