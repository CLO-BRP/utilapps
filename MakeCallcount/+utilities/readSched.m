function sched = readSched(C)
%
% Description:
%   Extracts coverage table as an nx5 cell matrix of strings and doubles 
%
% Inputs:
%   C - raw hand browse schedule from Excel worksheet (cell array of char vectors)
%
% Outputs:
%   sched - nx5 cell matrix
%       column 1 - Deployment (string)
%       column 2 - Site (string)
%       column 3 - Channel (string)
%       column 4 - start of analysis date (string)
%       column 5 - end of analysis date (string)
%
% History
%   msp2  Aug-15-2017   Initial

    % Initializations
    h = waitbar(0,'Reading recording coverage worksheet ...');
    import utilities.*
    appName = 'Make Callcount';
    
    % Pull data from table as variables
    dep = get_field(C,'Deployment');
    site = get_field(C,'Site');
    site(cellfun(@isnumeric,site)) = cellstr(num2str(cell2mat(site(cellfun(@isnumeric,site)))));
    chan = get_field(C,'Channel');
    start = get_field(C,'Analysis Start Date');
    stop = get_field(C,'Analysis End Date');
    assertMSP(~isempty(dep),appName,'"Deployment" column not found in Deployment_Info worksheet')
    assertMSP(~isempty(site),appName,'"Site" column not found in Deployment_Info worksheet')
    assertMSP(~isempty(chan),appName,'"Channel" column not found in Deployment_Info worksheet')
    assertMSP(~isempty(start),appName,'"Analysis Start Date" column not found in Deployment_Info worksheet')
    assertMSP(~isempty(stop),appName,'"Analysis End Date" column not found in Deployment_Info worksheet')
    
    %---
    % Clean up coverage table
    %---
    
    % Delete lines that have no site name
    hasSite = cellfun(@ischar,site);
    dep(~hasSite) = [];
    site(~hasSite) = [];
    chan(~hasSite) = [];
    start(~hasSite) = [];
    stop(~hasSite) = [];
    assertMSP(~isempty(site),appName,'A "Site" must be named for each line to be used in Deployment_Info worksheet')
    
    % Delete lines that have not channel number
    chan(~cellfun(@isnumeric,chan)) = {NaN};
    chan = cell2mat(chan);
    hasChan = ~isnan(chan);
    dep(~hasChan) = [];
    site(~hasChan) = [];
    chan(~hasChan) = [];
    start(~hasChan) = [];
    stop(~hasChan) = [];
    assertMSP(~isempty(chan),appName,'The "Channel" column must have a channel number for each line to be used in Deployment_Info worksheet')
    
    % Test that the same site name is used for each channel
    uniqueChan = unique(chan,'stable');
    uniqueSite = unique(site,'stable');
    len = length(uniqueSite);
    for i = 1:len
        assertMSP(all(chan==uniqueChan(i) == strcmp(site,uniqueSite(i))),appName,...
            'Channels must keep the same site for all deployments in Deployment_Info worksheet')
    end
    
    % Set to empty string cells that don't contain character arrays
    start(~cellfun(@ischar,start)) = {''};
    stop(~cellfun(@ischar,stop)) = {''};
    
    % Find MATLAB date number of all dates
    pattern = '\d{1,2}/\d{1,2}/\d{2,4}';
    m = length(site);
    startDate = NaN(m,1);
    stopDate = NaN(m,1);
    goodStart = ~cellfun(@isempty, regexp(start, {pattern}));
    startDate(goodStart) = datenum(start(goodStart));
    goodStop = ~cellfun(@isempty, regexp(stop, {pattern}));
    stopDate(goodStop) = datenum(stop(goodStop));
    
    % Delete lines that were not analyzed (ie, not included in recordings)
    badChanIDX = isnan(startDate) | isnan(stopDate);
    dep(badChanIDX) = [];
    site(badChanIDX) = [];
    chan(badChanIDX) = [];
    startDate(badChanIDX) = [];
    stopDate(badChanIDX) = [];
    
    % Combine coverage data into a cell array
    sched = {dep,site,chan,startDate,stopDate};
    assertMSP(~isequal(sched,cell(m,5)),appName,'Coverage table could note be read.')
    
    delete(h);
