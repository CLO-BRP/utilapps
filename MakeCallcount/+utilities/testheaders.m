function testheaders(inFilesFull)
% Test that selection table headers include File Offset and either Begin File or Begin Path
%
%   Input
%       inFilesFull: list of selection tables with full path (<nx1> cell)

    import utilities.*

    headers = cellfun(@readHeaders,inFilesFull,'UniformOutput',false);
    [~,inFiles, ext] = cellfun(@fileparts, inFilesFull, 'UniformOutput', false);
    inFiles = strcat(inFiles, ext);
    template = {'Begin File','Begin Path','File Offset (s)'};
    IDX = false(length(headers),length(template));
    for i = 1:length(headers)
        IDX(i,:) = ismember(template,headers{i});
    end
    IDX2 = [IDX(:,1) | IDX(:,2) , IDX(:,3)];
    inFilesErr1 = inFiles(~IDX2(:,1));
    inFilesErr2 = inFiles(~IDX2(:,2));
    err1Str = sprintf('%s\n',inFilesErr1{:});
    err2Str = sprintf('%s\n',inFilesErr2{:});
    msg = {};
    if ~isempty(err1Str)
        msg = [msg;'WARNING: Selection tables don''t have "Begin File" or "Begin Path:"\n'];
        msg = [msg;sprintf('  %s\n',err1Str)];
    end
    if ~isempty(err2Str)
        msg = [msg;sprintf('WARNING: Selection tables don''t have "File Offset (s)"\n')];
        msg = [msg;sprintf('  %s\n',err2Str)];
    end
    assertMSP(isempty(inFilesErr1)&isempty(inFilesErr2),'Make Callcount',msg)