function writeCallcount(M,inFilesFull,sched,outpath)
% Writes XLS files with callcount
%   Input
%     M: callcount matrix <numDays  x numChans>
%     inFilesFull - list of full paths to selection tables (nx1 cell array of strings)
%     sched - site, deploy date, retrieve date, begin analysis date, and 
%             end analysis date for each deployment (1x5 cell array)
%     outpath - path to write XLS file to

%---
% Initializations
%---
%%
dep = sched{1};
site = sched{2};
chan = sched{3};
startDate = sched{4};
stopDate = sched{5};
numChan = max(chan);
uniqueSite = cell(1,numChan);
for i = 1:numChan
    siteForChan = site(chan==i);
	if ~isempty(siteForChan)
        uniqueSite(i) = siteForChan(1);
	end
end
firstDay = min(startDate);
lastDay = max(stopDate);
numDays = lastDay-firstDay+1;
sz = size(M);
[~,inFiles,ext] = cellfun(@fileparts,inFilesFull,'UniformOutput',false);
inFiles = strcat(inFiles,ext);
outfileName = fullfile(outpath,sprintf('Callcount_%s',[datestr(now,'yyyymmdd_HHMMSS'),'.xlsx']));
sheetIDX = 0;

%---
% Daily Callcount
%---
%%
% Add header
C = cell(sz(1)+6, sz(2)+2);
C(1,1) = {'DAILY CALLCOUNT'};
C(2,1) = {datestr(now)};
C(4,1) = {'Date'};

% Add channel column headers
C(4,2:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(5,2:end) = strsplit(chanStr(1:end),',');

% Add date acolumn
C(6:sz(1)+5, 1) = cellstr(datestr(firstDay:lastDay,'mm/dd/yyyy'));

% Add body
Mday = sum(M,3,'omitnan');
Mday(all(isnan(M),3)) = NaN;
C(6:sz(1)+5, 2:end-1) = num2cell(Mday);

% Add row and column sums
rowsum = sum(Mday,2,'omitnan');
rowsum(sum(isnan(Mday),2)==numChan) = NaN;
colsum = sum(Mday,1,'omitnan');
colsum(sum(isnan(Mday),1)==numDays) = NaN;
C(6:sz(1)+5, end) = num2cell(rowsum);
C(6+sz(1), :) = [{'SUM'},num2cell(colsum),sum(colsum)];

% Write output
xlswrite(outfileName,C,'Daily Callcount');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);

%delete unused first worksheets
sheetIDX = sheetIDX + 1;
excel.deleteWorksheet(Excel,sheetIDX); %delete unused first worksheet

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*4;
width(1) = 10;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:m,:) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4:5,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(6:end,2+numChan) = true;
colorIDX(end,:) = true;
excel.fontColor(Excel,sheetIDX,colorIDX,color);

% Save open worksheet
excel.close(Excel,Workbook)


%---
% Hourly Callcount
%---
%%
% Add header
C = cell(numDays*24+6, sz(2)+3);
C(1,1) = {'HOURLY CALLCOUNT'};
C(2,1) = {datestr(now)};
C(5,1:2) = {'Date','Hour'};

% Add channel column headers
C(4,3:end-1) = uniqueSite;
chanStr = [sprintf('ch %.0f,',(1:numChan)), 'SUM'];
C(5,3:end) = strsplit(chanStr(1:end),',');
idx = 6;

% Add date and time columns
day = (firstDay:lastDay)';
for i = 1:numDays
    currDate = {datestr(day(i),'mm/dd/yyyy')};
    for j = 1:24
        C(idx,1:2) = [currDate,j-1];
        idx = idx + 1;
    end
end

idx = 6;
for i = 1:numDays
    for j = 1:24
        C(idx,3:end-1) = num2cell(M(i,:,j));
        idx = idx + 1;
    end
end

% Add row and column sums
MdayTime = cell2mat(C(6:5+numDays*24,3:end-1));
rowsum = sum(MdayTime,2,'omitnan');
rowsum(sum(isnan(MdayTime),2)==numChan) = NaN;
colsum = sum(MdayTime,1,'omitnan');
colsum(sum(isnan(MdayTime),1)==numDays) = NaN;
C(6:idx-1, end) = num2cell(rowsum);
C(idx, :) = [{'SUM'},{''},num2cell(colsum),sum(colsum)];

% Write output
xlswrite(outfileName,C,'Hourly Callcount');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);

% Adjust column widths
[m,n] = size(C);
width = ones(1,n).*4;
width(1) = 10;
sheetIDX = sheetIDX + 1;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(4:m,:) = true;
try
    excel.alignCell(Excel,sheetIDX,cellIDX,'center')
catch
    fprintf(2,'\n\nWARNING: Unable to center cells in "Hourly Callcount" worksheet\n');
end

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(4:5,:) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Blue font for sums
color = 'bright blue';
colorIDX = false(m,n);
colorIDX(6:end,3+numChan) = true;
colorIDX(end,:) = true;
try
    excel.fontColor(Excel,sheetIDX,colorIDX,color);
catch
    fprintf(2,'\n\nWARNING: Unable to set font color in "Hourly Callcount" worksheet\n');
end    

% Save open worksheet
excel.close(Excel,Workbook)

%---
% CONTEXT
%---
%%
% Add header
sz = size(sched);
C = cell(sz(1)+10, 5);
C(1,1) = {'CONTEXT'};
C(3,1) = {['Worksheet Created: ',datestr(now)]};
C(5,1) = {'Selection Tables Used:'};
len = length(inFiles);
C(6:len+5,2) = inFiles;
row = len + 7;
C(row,1) = {'----- Deployment Info -----'};
row = row + 1;
C(row,1:5) = {'Deployment',...
              'Site',...
              'Channel',...
              'Analysis Start',...
              'Analysis End'...
};
row = row + 1;
startDate(isnan(startDate)) = 0;
stopDate(isnan(stopDate)) = 0;
startDate2 =  cellstr(datestr(startDate,'mm/dd/yyyy'));
stopDate2 =  cellstr(datestr(stopDate,'mm/dd/yyyy'));
startDate2(strcmp(startDate2,'01/00/0000')) = {''};
stopDate2(strcmp(stopDate2,'01/00/0000')) = {''};

C(row:row-1+length(dep),1:5) = [
    dep,...
    site,...
    num2cell(chan),...
    startDate2,...
    stopDate2...
];

% Write output
xlswrite(outfileName,C,'CONTEXT');

%---
% Format worksheet
%---

% Open workbook
visible = false;
[Excel,Workbooks,Workbook] = excel.open(outfileName,visible);
sheetIDX = sheetIDX + 1;

% Adjust column widths
[m,n] = size(C);
width = ones(1,n) .* 12;
excel.colWidth(Excel,sheetIDX,width)

% Center cells
cellIDX = false(m,n);
cellIDX(10:m,1:n) = true;
excel.alignCell(Excel,sheetIDX,cellIDX,'center')

%Bold font
boldIDX = false(m,n);
boldIDX(1,1) = true;
boldIDX(1,1) = true;
boldIDX(len+7:len+8,1:n) = true;
excel.boldFont(Excel,sheetIDX,boldIDX);

% Save open worksheet
excel.close(Excel,Workbook)
