function C = readHeaders(p)
% Read headers of each TSV file
%   Input
%       p - full path of selection tables (cell array of strings)
%   Output
%       C - selection table headers (cell array of strings)

import utilities.*

% Open selection table text read mode
fid = fopen(p,'rt');
assertMSP(~isequal(fid,-1),'Make Callcounts',sprintf('Selection table could not be read:\n  %s\n',p));

% Parse selection table by cell into cell vector
C = textscan(fid,'%s',1,'Delimiter','\n');
C = regexp(C{1},'\t','split');
C = C{1};
C = reshape(C,[length(C),1])';

% Close selection table
fclose(fid);
