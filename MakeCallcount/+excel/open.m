function [Excel,Workbooks,Workbook] = open(fn,visible)
% Open Excel workbook for editing in Excel
%
%   Input
%       fn: file name of Excel workbook (string)
%       visible: logical variable where:
%           0 = not visible
%           1 = visible
%   Output
%       Excel: Excel application object
%       Workbooks: Excel instance object
%       Workbook: Excel workbook object

    Excel = actxserver('excel.application');
    Excel.Visible = visible;
    Workbooks = get(Excel,'workbooks');
    Workbook = invoke(Workbooks,'Open',fn);

