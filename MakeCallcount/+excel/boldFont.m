function boldFont(Excel,sheetIDX,cellIDX)
% Applies bold font to indexed cells
%
%   Inputs
%       Excel: Excel application object
%       sheetIDX: Excel application object
%       sheetIDX: Excel worksheet index
%       cellIDX: index indicating which cells are to be centered (<mxn>logical)
    
%   Note: Loop is required because "get(Worksheet,'Range'" is limited to
%   a string argument of 255 characters are less.
  
    Worksheets = get(Excel,'Worksheets');
    Worksheet = Worksheets.Item(sheetIDX);    
    [m,n] = find(cellIDX);
    assert(~isempty(m),'No true cells in cellIDX')
    cellRef = excel.cellRef([m,n]);
    numCells = length(m);
    first = 1;
    last = min(numCells,40);
    while first <= numCells
        cellRefStr = [sprintf('%s,',cellRef{first:last}),cellRef{last}];
        Range = get(Worksheet,'Range',cellRefStr);
        Range.Font.Bold = true;
        first = last + 1;
        last = min(numCells, first + 40);
    end
    