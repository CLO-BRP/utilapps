function deleteWorksheet(Excel,n)
% Delete worksheet n
%
%   Inputs
%       Excel: Excel application object
%       n: worksheet number

    Excel.Sheets.Item(n).Delete;