function varargout = MakeCallcount(varargin)
%% MakeCallcount MATLAB code for MakeCallcount.fig
%      MakeCallcount, by itself, creates a new REPORTTOOLS or raises the existing
%      singleton*.
%
%      H = MakeCallcount returns the handle to a new REPORTTOOLS or the handle to
%      the existing singleton*.
%
%      MakeCallcount('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REPORTTOOLS.M with the given input arguments.
%
%      MakeCallcount('Property','Value',...) creates a new REPORTTOOLS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ReportTools_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ReportTools_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ReportTools

% Last Modified by GUIDE v2.5 07-Feb-2019 12:24:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ReportTools_OpeningFcn, ...
                   'gui_OutputFcn',  @ReportTools_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% --- Executes just before ReportTools is made visible.
function ReportTools_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ReportTools (see VARARGIN)

% Disable warning when worksheet added to Excel workbook
warning('OFF','MATLAB:xlswrite:AddSheet')

% Choose default command line output for ReportTools
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ReportTools wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = ReportTools_OutputFcn(hObject, eventdata, handles)

    %Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))

    % Get default command line output from handles structure
    varargout{1} = handles.output;
    
% --------------------------------------------------------------------
function helpMenu_Callback(hObject, eventdata, handles)

    % Display help document
    open(fullfile(fileparts(mfilename('fullpath')), '+utilities', 'help.html'))
    
% --------------------------------------------------------------------
% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over coverageTable.
function coverageTable_ButtonDownFcn(hObject, eventdata, handles)

    % Import Utilities
    import utilities.*

    %--
    % Find current path for input selection tables
    %--
    data = get(hObject,'UserData');
    if isstruct(data)
        inpath = data.inpath;
    else
        inpath = pwd;
    end
    if ~isdir(inpath)
        inpath = pwd;
    end
    
    %---
    % Set file type
    %---
    ext = 'xlsx';

    %--
    % Select selection tables for input
    %--
    filterSpect = fullfile(inpath, sprintf('*.%s',ext));
    [infile, inpath] = uigetfile( ...
        filterSpect, ...
        'Select selection tables for input', ...
        'Multiselect', 'on');
    if isequal(infile, 0)
        return;
    end
    
    %---
    % Read coverage table
    %---
    infileFull = fullfile(inpath,infile);
    sheet = 'Deployment_Info';
    try
        % % [~, ~, C] = xlsread(truthSchedFullfile, sheet, '', 'basic');
        [~, ~, C] = xlsread(infileFull,sheet);
    catch
        dlg_name = 'Make Callcount';
        txt = sprintf('WARNING:\nThere is no worksheet named "%s" in\n  "%s"',...
            sheet,infile);
        fail(txt, dlg_name)
    end
    sched = readSched(C);
    data.inpath = inpath;
    data.sched = sched;
    set(hObject,'UserData',data);
    set(hObject,'String',infile);

% --------------------------------------------------------------------
function coverageTable_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    
% --------------------------------------------------------------------
% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over inFiles.
function inFiles_ButtonDownFcn(hObject, eventdata, handles)

    % Initializations
    import utilities.*
    h = waitbar(0,'Reading selection tables ...');

    %--
    % Find current path for input selection tables
    %--
    inFilesPath = char(get(hObject,'UserData'));
    test = which('isfolder');
    if ~isempty(test)
        if ~isfolder(inFilesPath)
            inFilesPath = pwd;
        else
            if ~isdir(inFilesPath)
                inFilesPath = pwd;
            end
        end
    end
    
    %---
    % Set file type
    %---
    ext = 'txt';
    
    % Select root of file tree with selection tables
    if get(handles.fileTree,'Value')
        inFilesPath = uigetdir(inFilesPath, 'Select root of file tree with selection tables');
        if inFilesPath == 0
            return;
        end
        h = waitbar(0, 'Finding files in tree.');
        inFilesFull = fastDir(inFilesPath, 'r', ext);
        [~,inFiles, ext] = cellfun(@fileparts, inFilesFull, 'UniformOutput', false);
        inFiles = strcat(inFiles, ext);
        delete(h)
        set(handles.inFiles,'UserData',inFilesFull);
        
    % Select invidual selection tables in a folder
    else

        %--
        % Select selection tables for input
        %--
        filterSpect = fullfile(inFilesPath, sprintf('*.%s',ext));
        [inFiles, inFilesPath] = uigetfile( ...
            filterSpect, ...
            'Select selection tables for input', ...
            'Multiselect', 'on');
        if isequal(inFiles, 0)
            return;
        end
        set(hObject,'UserData',inFilesPath);
    end
    if ~iscell(inFiles)
        inFiles = {inFiles};
    end
    firstInfileFull = fullfile(inFilesPath,inFiles{1});
    inFilesFull = fullfile(inFilesPath,inFiles);
    
    % Test if required measurements are in selection table
    testheaders(inFilesFull);
        
    % Populate "Score Name" and "Annotation Name" pop up menus
    fid = fopen(firstInfileFull,'rt');
    assert(~isequal(fid,-1), 'Selection table could not be read:\n  %s\n',inFiles{1});
    C = textscan(fid,'%s',1,'Delimiter','\n');
    fclose(fid);
    header = strsplit(C{1}{1},'\t');
    handles.filterNameList.String = header;
    
    % Display the names of selected selections tables in UI
    set(hObject,'String',inFiles);
    
    % Delete waitbar
    delete(h);

% --------------------------------------------------------------------
function fileTree_Callback(hObject, eventdata, handles)

    % Clear "Input Selection Tables" field
    handles.inFiles.String = {''};

% --------------------------------------------------------------------
function filterNameList_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% --------------------------------------------------------------------
function filterEnable_Callback(hObject, eventdata, handles)
    
    % Toggle filter controls visible/invisible depending on checkbox value
	if hObject.Value
        handles.filterNameList.Visible = 'On';
        handles.filterNameEditLabel.Visible = 'On';
        
        handles.filterValueEdit.Visible = 'On';
        handles.filterValueEditLabel.Visible = 'On';        
        handles.filterButton.Visible = 'On';
	else
        handles.filterNameList.Visible = 'Off';
        handles.filterNameEditLabel.Visible = 'Off';
        handles.filterValueEdit.Visible = 'Off';
        handles.filterValueEditLabel.Visible = 'Off';     
        handles.filterButton.Visible = 'Off';
	end

% --------------------------------------------------------------------
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path
    %--
    outpath = char(get(hObject,'UserData'));
    if ~isfolder(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    set(hObject,'UserData',outpath);
    [~, outpathDisplay] = fileparts(outpath);
    set(hObject,'String',outpathDisplay);

% --------------------------------------------------------------------
function runButton_Callback(hObject, eventdata, handles)
%
% TO DO
% 1. Use new date-time and calendar objects as input to MATLAB plotting
%    functions to simplify callcount calculations. I have already started
%    doing this in "plotWeeklyPresence".
% 2. Allow channel numbers that do not start with 1 (discuss with analysts)
%   option 1: Tabulate and plot blanks lines/axes for channels with no
%     deployment date range.
%   option 2: Tabulate and plot only channels for which there is a
%     deployment date range.
% 3. At present dates in tickmarks correpond to dates in the *center* 
%    of the bars. Should they correspond to the dates at the *edge* of the
%    bars? Discuss with analysts.

    % Initializations
    tic
    import utilities.*
    %---
    % User input
    %---
    
    % Recording schedule
    h = waitbar(0, 'Reading user input ...');
    data = get(handles.coverageTable,'UserData');
    if isstruct(data) && isfield(data,'sched')
        sched = data.sched;
    else
        sched = {};
    end
    assertMSP(~isempty(sched),'Make Callcount','WARNING: Please select valid recording coverage worksheet.')
    
    % Selection tables
    fileTree = get(handles.fileTree,'Value');
    if fileTree
        inFilesFull = get(handles.inFiles,'UserData');
    else
        inFiles = get(handles.inFiles,'String');
        inFilesPath = char(get(handles.inFiles,'UserData'));
        inFilesFull = fullfile(inFilesPath,inFiles);
    end
    inFilesFull = cellstr(inFilesFull);
    assertMSP(~isempty(inFilesFull{1}),'Make Callcount','WARNING: Please select input logs.')
    
    % Test if required measurements are in selection table
    testheaders(inFilesFull)
    
    % Annotation name
    filterEnable = get(handles.filterEnable,'Value');
    if filterEnable
        tagNameList = get(handles.filterNameList,'String');
        tagNameIDX = get(handles.filterNameList,'Value');
        tagName = tagNameList{tagNameIDX};
    else
        tagName = {};
    end
        
    % Output path
    outpath = char(get(handles.outpath,'UserData'));    
    assertMSP(~isempty(outpath),'Make Callcount','WARNING: Please select output folder.')

    % calc callcounts
    waitbar(0.15, h, 'Calculating callcounts ...')
    [M,C,tagList] = calcCallcount(inFilesFull,sched,tagName);
    
    % write callcount workbook
    waitbar(0.3, h, 'Writing callcount workbook ...')
    writeCallcount(M,inFilesFull,sched,outpath)
    
    % write presence workbook
    waitbar(0.45, h, 'Writing first presence workbook ...')
    writePresence(M,inFilesFull,sched,outpath)
    waitbar(0.6, h, 'Writing second presence workbook ...')
    writePresence2(M,inFilesFull,sched,outpath)
    
    % write plots
    waitbar(0.8, h, 'Writing plots ...')
    writePlots(M,sched,outpath,C,tagList)

% %     % write plots (using new date and time classes
% %     dn = [];
% %     for i = 1:length(inFilesFull)
% %         C = read_selection_table(inFilesFull{i});
% %     % %     C = filter_selection_table(sched);
% %         dn = [dn ; calcRealClockTime(C)];
% %     end
% %     dt = datetime(dn,'ConvertFrom','datenum');
% %     plotWeeklyPresence(dt,sched);
    
    % Let user know that processing is done.
    txt = sprintf('Processing complete in %s', ...
        datestr(toc/86400, 'HH:MM:SS'));
    fprintf('\n\n%s\n', txt);
    waitbar(1, h, txt)
    
    
