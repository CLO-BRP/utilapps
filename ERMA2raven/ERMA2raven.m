function varargout = ERMA2raven(varargin)
% EXPORTPAMGUARD MATLAB code for ExportPamguard.fig
%      EXPORTPAMGUARD, by itself, creates a new EXPORTPAMGUARD or raises the existing
%      singleton*.
%
%      H = EXPORTPAMGUARD returns the handle to a new EXPORTPAMGUARD or the handle to
%      the existing singleton*.
%
%      EXPORTPAMGUARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXPORTPAMGUARD.M with the given input arguments.
%
%      EXPORTPAMGUARD('Property','Value',...) creates a new EXPORTPAMGUARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ExportPamguard_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ExportPamguard_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ExportPamguard

% Last Modified by GUIDE v2.5 12-Apr-2019 15:58:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ExportPamguard_OpeningFcn, ...
                   'gui_OutputFcn',  @ExportPamguard_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before ExportPamguard is made visible.
function ExportPamguard_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for ExportPamguard
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ExportPamguard wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = ExportPamguard_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;

% ---
function inFiles_ButtonDownFcn(hObject, eventdata, handles)
    % Import Utilities
    import utilities.*

    %--
    % Find current path for input selection tables
    %--
    inFilesPath = char(handles.inFiles.UserData);
    if ~isdir(inFilesPath)
        inFilesPath = pwd;
    end

    %--
    % Select selection tables for input
    %--
    filterSpect = fullfile(inFilesPath, sprintf('*.%s','mat'));
    [inFiles, inFilesPath2] = uigetfile( ...
        filterSpect, ...
        'Select selection tables for input', ...
        'Multiselect', 'on');
    if isequal(inFiles, 0)
        return;
    end
    handles.inFiles.UserData = inFilesPath2;

    if ~iscell(inFiles)
        inFiles = {inFiles};
    end
    handles.inFiles.String = inFiles;

% ---
function listfile_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current handbrowsed schedule file
    %--
    listfileFull = char(handles.listfile.UserData);
    if ~exist(listfileFull, 'file')
        listfileFull = fullfile(pwd, 'HandbrowseSchedule.xlsx');
    end

    %--
    % Select new handbrowsed schedule file
    %--
    filterSpec =  '*.txt';
    title = 'Select listfile for input';
    [listfileFile, inPath] = uigetfile(filterSpec, title, listfileFull);
    if isequal(listfileFile, 0)
        return;
    end
    handles.listfile.String = listfileFile;
    listfileFull = fullfile(inPath, listfileFile);
    handles.listfile.UserData = listfileFull;
    handles.listfileFileTitle.TooltipString = listfileFull;
    
% ---
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path
    %--
    outpath = char(handles.outpath.UserData);
    if ~isfolder(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    handles.outpath.UserData = outpath;
    handles.outpathTitle.TooltipString = outpath;
    [~, outpathDisplay] = fileparts(outpath);
    handles.outpath.String = outpathDisplay;
    
% ---
function runButton_Callback(hObject, eventdata, handles)

    %Initializations
    import util.*
    inFiles = handles.inFiles.String;
    inFilesPath = handles.inFiles.UserData;
    inFilesFull = fullfile(inFilesPath,inFiles);
    listfileFull = handles.listfile.UserData;
    outpath = handles.outpath.UserData;
    
    %Test inputs
    assert(all(isfile(inFilesFull)),...
        sprintf('\n\n  *** At least one of the input files does not exist! ***\n'))
    assert(isfolder(outpath),...
        sprintf('\n\n  *** Output folder does not exist! ***\n'))
    assert(isfile(listfileFull),...
        sprintf('\n\n  *** List file does not exist! ***\n'))
    
    %Reading Raven list file
    h = waitbar(0.2,'Reading Raven list file ...');
    [fnSound,cumDur] = read_listfile_ERMA(listfileFull);
    
    %Export ERMA logs as Raven selection tables
    waitbar(0,h,'Exporting ERMA detection logs as a merged selection table ...')
    C = cellfun(@det2Raven,inFilesFull,'UniformOutput',false);
    C = vertcat(C{:});
    
    %Renumber selections
    id = (1:size(C,1))';
    C(:,1) = strtrim(cellstr(num2str(id)));
    
    %Add header
    header = {'Selection', ...
              'View', ...
              'Channel', ...
              'Begin Time (s)', ...
              'End Time (s)', ...
              'Low Freq (Hz)', ...
              'High Freq (Hz)', ...
              'Begin Path',...
              'Begin File',...
              'File Offset (s)',...
              'Threshold', ...
              'Amplitude', ...
              'Relative Amplitude', ...
              'ICI', ...
              '???', ...
    };
    C = [header ; C];
    
    %Migrate Begin Time and End time using list file
    waitbar(0.4,h,'Migrating Raven selection table times to list file ...')
    C = migrateTableERMA(C,fnSound,cumDur);

    % Write selection table
    waitbar(0.6,h,'Writing Raven selection table ...')
    outfileFull = fullfile(outpath,sprintf('ERMA_%s.txt',datestr(now,'yyyymmdd_HHMMSS')));
    write_selection_table(C,outfileFull)
    close(h)

    disp(' ')
    disp('****************************************************************************')
    fprintf('Finished writing: %s\n',outfileFull);
    disp('****************************************************************************')
