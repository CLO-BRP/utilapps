function write_selection_table(C, outPathFull)

        % open output file in text write mode
        fid = fopen(outPathFull,'wt');
        assert(~isequal(fid,-1),'\n\nWARNING: Selection table could not be written:\n  %s\n',outPathFull)

        % make format string for output
        [~,n] = size(C);
        f = [repmat('%s\t',1,n-1),'%s\n'];

        % write output file
        C=C';
        fprintf(fid,f,C{:,:});

        % close output file
        fclose(fid);