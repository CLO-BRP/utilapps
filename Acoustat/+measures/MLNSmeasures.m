function y = MLNSmeasures(cmd, name, clickTF, selTF, gramletTF, gramlet,params)
%MLNSmeasures	Osprey measures for the Macaulay Library of Natural Sounds
%
%   This function implements the set of measures for the Macaulay Library 
%   of Natural Sounds.  These measures, based on Acoustat (see AcoustatSet.m), 
%   are designed to be relatively independent of the background noise level, 
%   so that clear and faint vocalizations will tend to have similar
%   measurement values.  Many of the measurements here are based on
%   weighting spectrogram cells by the intensity of the sound present.
%
%   See README.txt in this directory for the calling convention and args.
%   See also AcoustatSet.m.
%
%   Note: In adding these to Osprey, I changed ./mainSet.m so that the
%   'enabled' field is initialized to all 0's, and also changed 
%   opPositionControls just after the 'Brightness and contrast sliders'
%   comment so that left has 82 added instead of 52; also changed the
%   calculation and later adjustment of s0 in opComputeSpect.
%
%   History
%     Dave Mellinger  Apr 2006  
%       Create
%     msp2            Jul 2015
%       Remove global variables.

switch(cmd)
case 'init'
  % The init structure is a global for later access.
  MLNS_params = struct(...
      'longName',{	'M1 Start Time'				...
      			'M2 End Time'				...
      			'M3 Lower Frequency'			...
      			'M4 Upper Frequency'			...
      			'M5 Duration'				...
      			'M6 Bandwidth'				...
      			'M7 Median Time'			...
      			'M8 Temporal Interquartile Range'	...
      			'M9 Temporal Concentration'		...
      			'M10 Temporal Asymmetry'		...
      			'M11 Median Frequency'			...
      			'M12 Spectral Interquartile Range'	...
      			'M13 Spectral Concentration'		...
      			'M14 Spectral Asymmetry'		...
      			'M15 Time of Peak Cell Intensity'	...
      			'M16 Relative Time of Peak Cell Intensity'	...
      			'M17 Time of Peak Overall Intensity'	...
      			'M18 Relative Time of Peak Overall Intensity'	...
      			'M19 Frequency of Peak Cell Intensity'	...
      			'M20 Frequency of Peak Overall Intensity'	...
      			'M21 Amplitude Modulation Rate'		...
      			'M22 Variation in AM Rate'		...
      			'M23 Frequency Modulation Rate'		...
      			'M24 Variation in FM Rate'		...
      			'M25 Average Cepstrum Peak Width'	...
      			'M26 Overall Entropy'			...
      			'M27 Upsweep Mean'			...
      			'M28 Upsweep Fraction'			...
      			'M29 Signal-to-Noise Ratio'		...
	      }, ...
      'screenName',{	'Start Time'		...
      			'End Time'		...
      			'Lower Freq'		...
      			'Upper Freq'		...
      			'Duration'		...
      			'Bandwidth'		...
      			'Median Time'		...
      			'Time Quart.'		...
      			'Time Concent.'		...
      			'Time Asymm.'		...
      			'Median Freq'		...
      			'Freq. Quart.'		...
      			'Freq. Concent.'	...
      			'Freq. Asymm.'		...
      			'Pk Cell T'		...
      			'Pk Cell Rel T'		...
      			'Pk Overall T'		...
      			'Pk Overall Rel T'	...
      			'Pk Cell F'		...
      			'Pk Overall F'		...
      			'AM Rate'		...
      			'AM Rate Var.'		...
      			'FM Rate'		...
      			'FM Rate Var.'		...
      			'Cepstrum Width'	...
      			'Entropy'		...
      			'Upsweep Mean'		...
      			'Upsweep Frac'		...
			'MSNR'			...
	      }, ...
      'unit', { 's'		's'		'Hz'		'Hz'...
		's'		'Hz'		's'		's'...
		's'		''		'Hz'		'Hz'...
		'Hz'		''		's'		'%'...
		's'		'%'		'Hz'		'Hz'...
		'Hz'		'?'		'Hz'		'?'...
		'Hz'		'Hz'		'Hz'		'%'...
		'dB'
              }, ...
      'type', {	'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
      		'gramlet' ...
	      }, ...
      'needSel',   num2cell('11111111111111111111111111111' - '0'), ...
      'needGram',  num2cell('11111111111111111111111111111' - '0'), ...
      'fixTime',   num2cell('00000000000000000000000000000' - '0'), ...
      ... %'enabled',   num2cell('00000000000000000000000000000' - '0'), ...
      'enabled',   num2cell('11111111111111111111111111111' - '0'), ...
      'sortIndex', num2cell(100:128) );

  y = MLNS_params;		% return value

case 'measure'

    % Give things short names to keep code relatively compact.
    bx = gramletTF;
    gpow = exp(gramlet).^2;
    binDur = 1 / params.frameRate;
    binBW  = params.binBW;
    theta = 0.90;			% threshold of energy in box

    switch(name)
    case 'M1 Start Time'
        minTI = make_envTF(gpow, theta);
        v = (minTI-1.5) * binDur + bx(1);	% convert to s
        
    case 'M2 End Time'
        [~, maxTI] = make_envTF(gpow, theta);
        v = (maxTI-0.5) * binDur + bx(1);	% convert to s

    case 'M3 Lower Frequency'
        [~, ~, minFI] = make_envTF(gpow, theta);
        v = (minFI-1.5) * binBW + bx(2);	% convert to Hz

    case 'M4 Upper Frequency'
        [~, ~, ~, maxFI] = make_envTF(gpow, theta);
        v = (maxFI-0.5) * binBW + bx(2);	% convert to Hz

    case 'M5 Duration'
        [minTI, maxTI] = make_envTF(gpow, theta);
    	rangeT = ([minTI-1.5 maxTI-0.5]) * binDur + bx(1);	% convert to s
        v = diff(rangeT);

    case 'M6 Bandwidth'
        [~, ~, minFI, maxFI] = make_envTF(gpow, theta);
        rangeF = ([minFI-1.5 maxFI-0.5]) * binBW + bx(2);	% convert to Hz
        v = diff(rangeF);
        
    case 'M7 Median Time'

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);

        % qT has the quartiles for time
        qT = cumFrac(trEnvT.', [0.25 0.50 0.75], trBx([1 3]));
        
        v = qT(2);

    case 'M8 Temporal Interquartile Range'

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);

        % qT has the quartiles for time.
        qT = cumFrac(trEnvT.', [0.25 0.50 0.75], trBx([1 3]));
        
        v = qT(3) - qT(1);

    case 'M9 Temporal Concentration'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
        trEnvT = sum(trGram, 1);		% trimmed time envelope, row vector
        
        [sortT,ixTsort] = sort(trEnvT);		% row vectors
        ixT = cumFrac(sortT.', 0.50);
        ixTMin = min(ixTsort(ceil(ixT) : end));
        ixTMax = max(ixTsort(ceil(ixT) : end));
        v = (ixTMax - ixTMin) * binDur;

    case 'M10 Temporal Asymmetry'

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);

        % qT has the quartiles for time
        qT = cumFrac(trEnvT.', [0.25 0.50 0.75], trBx([1 3]));
    
        v = (qT(1) + qT(3) - 2*qT(2)) / (qT(1) + qT(3));
        
    case 'M11 Median Frequency'

        % make trimmed envelope
        [trBx, ~, trEnvF] = trimTF(gpow, bx, binDur, binBW, theta);
        
        % qF has the quartiles for frequency.
        qF = cumFrac(trEnvF, [0.25 0.50 0.75], trBx([2 4]));
        v = qF(2);

    case 'M12 Spectral Interquartile Range'

        % make trimmed envelope
        [trBx, ~, trEnvF] = trimTF(gpow, bx, binDur, binBW, theta);
        
        % qF has the quartiles for frequency.
        qF = cumFrac(trEnvF, [0.25 0.50 0.75], trBx([2 4]));
        
        v = qF(3) - qF(1);

    case 'M13 Spectral Concentration'

        % make trimmed envelope
        [~, ~, trEnvF] = trimTF(gpow, bx, binDur, binBW, theta);
        
        [sortF,ixFsort] = sort(trEnvF);		% col vectors
        ixF = cumFrac(sortF, 0.50);
        ixFMin = min(ixFsort(ceil(ixF) : end));
        ixFMax = max(ixFsort(ceil(ixF) : end));
        %M(13) = (ixFMax - ixFMin) * binDur; 	% OLD, BAD method!
        v = (ixFMax - ixFMin) * binBW;

    case 'M14 Spectral Asymmetry'

        % make trimmed envelope
        [trBx, ~, trEnvF] = trimTF(gpow, bx, binDur, binBW, theta);
        
        % qF has the quartiles for frequency.
        qF = cumFrac(trEnvF, [0.25 0.50 0.75], trBx([2 4]));
        
        v = (qF(1) + qF(3) - 2*qF(2)) / (qF(1) + qF(3));

    case 'M15 Time of Peak Cell Intensity'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
        [~, peakIx] = max(trGram(:));
        peakOffsetT = floor((peakIx-1) / utils.nRows(trGram)) * binDur;

        % make trimmed envelope
        trBx = trimTF(gpow, bx, binDur, binBW, theta);
        
        v = peakOffsetT + trBx(1);

    case 'M16 Relative Time of Peak Cell Intensity'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
        [~,peakIx] = max(trGram(:));
        peakOffsetT = floor((peakIx-1) / utils.nRows(trGram)) * binDur;

        % make trimmed envelope
        trBx = trimTF(gpow, bx, binDur, binBW, theta);
        
        v = peakOffsetT / (trBx(3) - trBx(1)) * 100;

    case 'M17 Time of Peak Overall Intensity'

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        [~,peakIxEnvT] = max(trEnvT);
        v = (peakIxEnvT-1) * binDur + trBx(1);

    case 'M18 Relative Time of Peak Overall Intensity'

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        [~,peakIxEnvT] = max(trEnvT);
        v = (peakIxEnvT-1) * binDur / (trBx(3) - trBx(1)) * 100;

    case 'M19 Frequency of Peak Cell Intensity'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
        [~, peakIx] = max(trGram(:));
        peakOffsetF = rem(peakIx-1, utils.nRows(trGram)) * binBW;

        % make trimmed envelope
        trBx = trimTF(gpow, bx, binDur, binBW, theta);
        
        v = peakOffsetF + trBx(2);

    case 'M20 Frequency of Peak Overall Intensity'

        % make trimmed envelope
        [trBx, ~, trEnvF] = trimTF(gpow, bx, binDur, binBW, theta);
        
        [~,peakIxEnvF] = max(trEnvF);
        v = (peakIxEnvF-1) * binBW + trBx(2);

    case 'M21 Amplitude Modulation Rate'

        % make trimmed envelope
        [~, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        v = spectrumPeak(trEnvT, binDur);

    case 'M22 Variation in AM Rate'

        % make trimmed envelope
        [~, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        [~,v] = spectrumPeak(trEnvT, binDur);

    case 'M23 Frequency Modulation Rate'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        % First calculate median frequency fMed and weighted offset oMed.
        fMed = cumFrac(trGram, 0.5, trBx([2 4]));     % row vector
        oMed = (fMed - mean(fMed)) .* trEnvT.^(1/4);
        v = spectrumPeak(oMed, binDur);
    
    case 'M24 Variation in FM Rate'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        % First calculate median frequency fMed and weighted offset oMed.
        fMed = cumFrac(trGram, 0.5, trBx([2 4]));     % row vector
        oMed = (fMed - mean(fMed)) .* trEnvT.^(1/4);
        [~,v] = spectrumPeak(oMed, binDur);

    case 'M25 Average Cepstrum Peak Width'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
        trN = size(trGram, 2);
        x = fft(trGram,[],1);		% make fft() operate columnwise
        cepsWidth = zeros(1, trN);
        for i = 1 : trN
           [~,cepsWidth(i)] = spectrumPeak(x(:,i).', binBW);
        end
        v = mean(cepsWidth);

    case 'M26 Overall Entropy'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
        sortgram = sort(trGram, 1, 'descend');
        v = mean(cumFrac(sortgram, 0.5)) * binBW;

    case 'M27 Upsweep Mean'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        sumT2end = sum(trEnvT(2:end));
        
        % First calculate median frequency fMed and weighted offset oMed.
        fMed = cumFrac(trGram, 0.5, trBx([2 4]));     % row vector
        
        v = sum((fMed(2 : end) - fMed(1 : end-1)) .* trEnvT(2:end) / sumT2end);

    case 'M28 Upsweep Fraction'
        [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram

        % make trimmed envelope
        [trBx, trEnvT] = trimTF(gpow, bx, binDur, binBW, theta);
        
        sumT2end = sum(trEnvT(2:end));
        
        % First calculate median frequency fMed and weighted offset oMed.
        fMed = cumFrac(trGram, 0.5, trBx([2 4]));     % row vector
        
        v = sum((fMed(2:end) > fMed(1:end-1)) .* trEnvT(2:end) / sumT2end) * 100; % the 100 is to make a percentage

    case 'M29 Signal-to-Noise Ratio'
        y = utils.percentile(gpow(:), [0.25 1]);
        v = 10 * log10(y(2) / y(1)) / 2;

    end
    y = v;
end	% main switch

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function z = cumFrac(seq, frac, bounds)
%    Find, for instance, the point where 50% (for frac=0.5) of the energy has 
%    accumulated in the column vector (!) seq.  The 2-element vector 'bounds'
%    gives the scaled indices of the first and last elements in seq; if
%    omitted, the indices are not scaled but instead go from 1 to the length of
%    seq.  If the 50% point occurs in between two elements, interpolation is
%    done.  frac should be in the range [0,1].  Also, frac may be a vector, in
%    which case the result is a column vector of the same length.
%
%    seq may also be a matrix, in which case cumFrac operates columnwise.  
%    Then z has multiple columns, one per column of seq.

if (nargin < 3)
  bounds = [1 utils.nRows(seq)];
end
Y = linspace(bounds(1), bounds(2), utils.nRows(seq));

x = sum(seq, 1);
z = zeros(length(frac), utils.nCols(seq));
for j = 1 : utils.nCols(seq)
  % Test whether frac is before the very first point.  (There is an up/down
  % asymmetry error here.  Need to fix it!)
  ix = (frac(:) < (seq(1,j) / x(j)));
  z(ix,j) = Y(1);
  z(~ix,j) = interp1(cumsum(seq(:,j)) / x(j), Y, frac(~ix));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [rate,width] = spectrumPeak(seq, period)
% Given a sequence 'seq', find the position of the peak of its power spectrum.
% 'period' is the spacing of elements in seq.  Also find 'width', the width of
% the peak 3 dB (a factor of 0.5) down from the peak.

n = length(seq);
if (n <= 3)
  rate = 0; width = 0;
  return
end
x = abs(fft(seq)) .^ 2;
x1 = x(2 : floor(n/2));	% positive freqs only; also ignore DC at x(1)
[pk,pkIx] = max(x1);	% pkIx is off by one because of ignoring DC
rate = pkIx/n/period;	% want pkIx+1 because of ignoring DC, but -1 for MATLAB
			%    1-based indexing; these cancel

if (0)
  % Summed autocorrelation test.
  [ac,sac,sacw] = sumautocorr(seq, 1, length(seq), [0 1]);
  figure(2); 
  subplot(411); plot(seq)
  subplot(412); plot(x1)
  subplot(413); plot(2:length(ac), ac(2:end))
  subplot(414); plot(sacw)
  figure(1)
  %disp([length(seq) period])
end

% Find width of peak 3 dB down from peak.
thresh = pk / (10^(3/10));		% 3 dB down
% i0 has guard value at start (and i1 at end) in case find() returns [].
i0 = [0  (find(x(1:pkIx) < thresh))];
i1 = [(find(x(pkIx : end) < thresh)+pkIx-1)  length(x)+1];
width = (i1(1) - i0(end) + 1) * period;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta)

    envT = sum(gpow, 1);		% time envelope
    envF = sum(gpow, 2);	  	% freq envelope (spectrum)

    % M1 Start Time, M2 End Time.
    [t1,ix] = sort(envT, 'descend');
    x = find(cumsum(t1) >= sum(envT) * theta);
    minTI = min(ix(1:x(1)));		% minimum index of cells in Feature Box
    maxTI = max(ix(1:x(1)));		% maximum index of cells in Feature Box
    
    envF = max(envF, 0);			% positive entries in envF
    [f1,ix] = sort(envF, 'descend');
    x = find(cumsum(f1) >= sum(envF) * theta);
    minFI = min(ix(1:x(1)));		% minimum index of cells in Feature Box
    maxFI = max(ix(1:x(1)));		% maximum index of cells in Feature Box

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [trBx, trEnvT, trEnvF] = trimTF(gpow, bx, binDur, binBW, theta)

    [minTI, maxTI, minFI, maxFI] = make_envTF(gpow, theta);
        
    % Now that we have the T/F box, trim to that size; also make trimmed
    % envelopes. tr stands for 'trimmed'.
    trGram = gpow(minFI : maxFI, minTI : maxTI);	% trimmed gram
    trEnvT = sum(trGram, 1);		% trimmed time envelope, row vector
    trEnvF = sum(trGram, 2);		% trimmed freq envelope, col vector
    trBx = [bx(1)+(minTI-1)*binDur bx(2)+(minFI-1)*binBW ...  % [t0 f0 t1 f1]
        bx(1)+(maxTI-1)*binDur bx(2)+(maxFI-1)*binBW];
    