function y = mainSet(cmd, name, clickTF, selTF, gramletTF, gramlet, params)
%mainSet	Osprey measures based on Acoustat.
%   This implements the set of measures based on Kurt Fristrup's Acoustat
%   (Fristrup, K.M. 1992. Characterizing Acoustic Features of Marine Animal 
%   Sounds. Technical Report WHOI-92-04, Woods Hole Oceanographic Inst., 
%   Woods Hole.)  These are measures designed to be relatively independent 
%   of the background noise level, so that clear and faint vocalizations
%   will tend to have similar measurement values.  Many of the measurements
%   here are based on weighting spectrogram cells by the intensity of the
%   sound present.
%
%   See README.txt in this directory for the calling convention and args.


switch(cmd)
case 'init'
  y = struct(...
      'longName',{	'centroid time'		...
      			'centroid frequency'	...
			'time spread'		...
			'frequency spread'	...
			'amplitude modulation rate' ...
			'signal-to-noise ratio' ...
	      }, ...
      'screenName',{ 	'centr. time'	...
      			'centr. freq'	...
			'time spread'	...
			'freq spread'	...
			'AM rate'	...
			'SNR'	...   % the MLNS one is called MSNR
	      }, ...
      'unit', {		's'	...
      			'Hz'	...
			's'	...
      			'Hz'	...
			'Hz'	...
			'dB'	...
              }, ...
      'type', {		'gramlet' ...
      			'gramlet' ...
      			'gramlet' ...
      			'gramlet' ...
      			'gramlet' ...
      			'gramlet' ...
	      }, ...
      'needSel',   num2cell([1 1 1 1 1 1]), ...
      'needGram',  num2cell([1 1 1 1 1 1]), ...
      'fixTime',   num2cell([1 0 0 0 0 0]), ...
      'enabled',   num2cell([1 1 0 0 1 1]), ...
      'sortIndex', num2cell(50:55) );

case 'measure'
  % Give things short names to keep code relatively compact.
  bx = gramletTF;
  lc = clickTF;
  amp = 20 / log(10);
  sel = selTF;
  gpow = exp(gramlet).^2;
  stdCorr = 0.2887;		% correction factor for std dev

  switch(name)
  case 'centroid time'
    % Calculate centroid of each row; weight by sum of each row; take average.
    n = utils.nCols(gramlet);
    v = sum([zeros(1,n); gpow]);
    v = sum(v .* ((0.5:n) / n * (bx(3)-bx(1)) + bx(1))) / sum(v);

  case 'centroid frequency'
    % Calculate centroid of each column; weight by sum of each column; take
    % average.
    n = utils.nRows(gramlet);
    v = sum([zeros(n,1), gpow]');
    v = sum(v .* ((0.5:n) / n * (bx(4)-bx(2)) + bx(2))) / sum(v);

  case 'time spread'				% see also frequency spread
    % Standard deviation of time values, weighted by sum of each column.
    v = weightedStd((0 : utils.nCols(gramlet)-1) / params.frameRate, ...
						     sum(gpow,1).^2) / stdCorr;

  case 'frequency spread'			% like duration, for frequency
    % Calculate stdev of each column, weight by sum of each column.
    f = linspace(bx(2), bx(4), utils.nRows(gramlet));	% freq, Hz, as a row vector
    s = weightedStd(f.' * ones(1,utils.nCols(gramlet)), gpow) / stdCorr;
    si = ~isnan(s);
    v = weightedMean(s(si), sum(gpow(:,si),1));

  case 'amplitude modulation rate'
    % Calculate power spectrum of time envelope, find peak.
    E = abs(fft(sum(gpow, 1))).^2;   % roughly, power spectrum of time envelope
    % Ignore E(1) [the DC offset] and E's upper half [with the negative freqs].
    [dummy,vi] = max(E(2:ceil(length(E)/2)));
    v = ((vi+1)-1) * params.frameRate / (length(E)+1);  % correct!

  case 'signal-to-noise ratio'
    x = sort(gpow(:));
    y = utils.percentile(x, [0.25 1]);
    v = 10 * log10(y(2) / y(1)) / 2;	% /2 is because gpow is squared
    
  end	% name switch
  y = v;
end	% main switch

% msp2 guess at correct function -----------------------------------------
function wstd = weightedStd(x, w)
    wstd = zeros(1, size(x,1));
    for i = 1:size(x,1)
        wstd(i) = sqrt(var(x(i,:), w(i,:)));
    end

% msp2 guess at correct function -----------------------------------------
function wmean= weightedMean(x, w)
    wmean = mean(w.'*x,2);
