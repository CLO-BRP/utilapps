function measure_handle = valid_identifier(screenName)
% make valid MATLAB identifier from screenName

    measure_handle = Acoustat('valid_identifier', {screenName});
    
end
