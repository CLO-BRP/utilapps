function varargout = Acoustat(varargin)
% ACOUSTAT MATLAB code for Acoustat.fig

% Acoustat
%   Adds Acoustat measurements taken from Osprey's "AcoustatSet" to Raven
%   selection tables.

% NOTE
%   XBAT functions for reading sound files and creating spectrograms.
%   oDeLMA functions were not used because at present they can only produce
%   calibrated power measures for the full spectrum using the rectangular
%   window function, which has poor sprectral frequency resolution due to
%   extreme spectral leakage.
%
% History
%   msp2     Jul 2015
%     Create
%   msp2  10 Sep 2015
%     Allow batch of selection tables to complete if an empty
%       selection table is encountered.
%     When selection table with no selections is encountered, output 
%       empty selection table rather than stopping execution.
%     Make waitbar wide enough to accommodate long file names.
%   msp2  02 Nov 2015
%     Replace function calls that require optional MATLAB toolbox.
%     Resize GUI for small monitors.
%   msp2  03 Jan 2017
%     Replace calls to XBAT functions with modified Osprey functions.
%   msp2  07 Jun 2017
%     Read sound files using MATLAB audioread rather than Osprey functions
%
% MATLAB reminders
%
%      ACOUSTAT, by itself, creates a new ACOUSTAT or raises the existing
%      singleton*.
%
%      H = ACOUSTAT returns the handle to a new ACOUSTAT or the handle to
%      the existing singleton*.
%
%      ACOUSTAT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ACOUSTAT.M with the given input arguments.
%
%      ACOUSTAT('Property','Value',...) creates a new ACOUSTAT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Acoustat_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Acoustat_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".

% Last Modified by GUIDE v2.5 25-Oct-2017 17:25:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Acoustat_OpeningFcn, ...
                   'gui_OutputFcn',  @Acoustat_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Acoustat is made visible.
function Acoustat_OpeningFcn(hObject, eventdata, handles, varargin)
    
    % Add Acoustat root to MATLAB search paths
    p = fileparts(mfilename('fullpath'));
    addpath(p)
    
    % Resize GUI if small monitor
    resizeGUI(handles);
    
    % Create measurements controls in GUI
    handles = make_controls(handles);
    
    % Choose default command line output for Acoustat
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Acoustat_OutputFcn(hObject, eventdata, handles)

    % Get default command line output from handles structure
    varargout{1} = handles.output;

    
% --- Executes when user attempts to close AcoustatGUI.
function AcoustatGUI_CloseRequestFcn(hObject, eventdata, handles)

% rmpath could break pjd detection engine, so disable it

% %     % Remove Acoustat root to MATLAB search paths
% %     p = fileparts(mfilename('fullpath'));
% %     rmpath(p)

    delete(hObject);
    

function slider_Callback(hObject, eventdata, handles)

    fnST = cellstr(handles.inpath.String);
    fnList = cellstr(handles.listfile_path.String);
    value = handles.slider.Value;    
    lenST = length(fnST);
    lenList = length(fnList);
    k = 19;
    topST = max(1, (1 - value) * (lenST - k) + 1);
    topList = max(1, (1 - value) * (lenList - k) + 1);
    handles.inpath.ListboxTop = topST;
    handles.listfile_path.ListboxTop = topList;


function inpath_ButtonDownFcn(hObject, eventdata, handles)
    
    %--
    % Find current file list
    %--
    inpath = handles.inpath.UserData;
    if ~isdir(inpath)
        inpath = pwd;
    end

    %--
    % Select new input path
    %--
    filterSpect = fullfile(inpath, '*.txt');
    [inFile, inpath] = uigetfile( ...
        filterSpect, ...
        'Select handbrowsed logs', ...
        'Multiselect', 'on');
    if isequal(inFile, 0)
        return;
    end
    inFile = cellstr(inFile);
    handles.inpath.UserData = inpath;
    handles.inpath.String = inFile;
    len = length(inFile);
    if len < 1
        return;
    end
    k = 19;
    handles.inpath.ListboxTop = 1;
    handles.listfile_path.ListboxTop = 1;
    handles.inpath_label.String = sprintf('Selection Tables (n = %.0f)', len);
    handles.slider.Value = 1;
    handles.slider.SliderStep = [max(1, 1/(len-k)), max(1, 1/((len-k)/k))];
    
    
function listfile_path_ButtonDownFcn(hObject, eventdata, handles)
    
    %--
    % Find current file list
    %--
    listfile_path = handles.listfile_path.UserData;
    if ~isdir(listfile_path)
        listfile_path = pwd;
    end

    %--
    % Select new input path
    %--
    filterSpect = fullfile(listfile_path, '*.txt');
    [listfile, listfile_path] = uigetfile( ...
        filterSpect, ...
        'Select handbrowsed logs', ...
        'Multiselect', 'on');
    if isequal(listfile, 0)
        return;
    end
    listfile = cellstr(listfile);
    handles.listfile_path.UserData = listfile_path;
    handles.listfile_path.String = listfile;
    len = length(listfile);
    if len > 0
        handles.slider.Value = 1;
        handles.listfile_path.ListboxTop = 1;
        handles.inpath.ListboxTop = 1;
        handles.listfile_label.String = sprintf('Listfiles (n = %.0f)', len);
    end
    
    
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Get current outpath path
    %--
    outpath = handles.outpath.String;
    if ~isdir(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder to output selection tables with measurements');
    if isequal(outpath, 0)
        return;
    end
    handles.outpath.String = outpath;
    

function fft_size_Callback(hObject, eventdata, handles)
    trim_input(hObject)
    is_input_numeric(hObject)
    

function hop_size_Callback(hObject, eventdata, handles)
    trim_input(hObject)
    is_input_numeric(hObject)

    
% --- Executes on selection change in window_type.
function window_type_Callback(hObject, eventdata, handles)
    win_type_list = lower(handles.window_type.String');
    win_type_idx = handles.window_type.Value;
    win_type = win_type_list{win_type_idx};
    win_type = strtrim(win_type);
    if strcmp(win_type,'kaiser')
        set(handles.window_parameter, 'Enable', 'on')
        set(handles.window_parameter_label, 'Enable', 'on')
        set(handles.window_parameter, 'String', '5  ')
    else
        set(handles.window_parameter, 'Enable', 'off')
        set(handles.window_parameter_label, 'Enable', 'off')
        set(handles.window_parameter, 'String', '')
    end
    

function window_parameter_Callback(hObject, eventdata, handles)
    trim_input(hObject)

    % check for valid input
    window_parameter_str = hObject.String;
    window_parameter = str2double(window_parameter_str);
    if ~isfinite(window_parameter) || ~isreal(window_parameter) || window_parameter < 0
        window_parameter = '0.5';
        set(hObject,'String',window_parameter)
    end
    

function window_size_Callback(hObject, eventdata, handles)
    trim_input(hObject)
    is_input_numeric(hObject)
    

%--- Trim leading and trailing blanks from input
function trim_input(hObject)
    param = hObject.String;
    param_trim = strtrim(param);
    if ~strcmp(param,param_trim)
        set(hObject,'String',param_trim)
    end    

    
%--- Check if input is a number
function is_input_numeric(hObject)

    % check if input value is numeric
    param = hObject.String;
    if ~isfinite(str2double(param))
        utils.fail(sprintf('%s is not numeric',param),'WARNING')
        param = '512';
        set(hObject,'String',param)
    end
    

% --- Executes on button press in run.
function run_Callback(hObject, eventdata, handles)

    % Initialize waitbar (make 500 pixels wider than default)
    h = waitbar(0,'Reading GUI');
    pos = h.Position;
    pos(3) = pos(3) + 400;
    set(h, 'Position', pos)    
    hChild = h.Children;
    posWait = hChild.Position;
    posWait(3) = posWait(3) + 400;
    set(hChild, 'Position', posWait);
    movegui(h, 'center')
    
    %---
    % Retrieve paths from GUI
    %---
    inpath = handles.inpath.UserData;
    fn_ST = cellstr(handles.inpath.String);
    if isempty(fn_ST)
        return;
    end
    listfile_path = handles.listfile_path.UserData;
    fn_listfile = cellstr(handles.listfile_path.String);
    if isempty(fn_listfile)
        return;
    end
    outpath = handles.outpath.String;
    
    %---
    % Set up spectrogram parameters for fast_specgram call
    %---
    fft_size = handles.fft_size.String;
    parameter.fft = str2double(fft_size);
    hop_size = handles.hop_size.String;
    parameter.hop = str2double(hop_size);
    win_type_list = handles.window_type.String;
    win_type_idx = handles.window_type.Value;
    win_type = strtrim(win_type_list{win_type_idx});
    parameter.win_type = lower(win_type);
    win_param = handles.window_parameter.String;
    parameter.win_param = str2double(win_param);
    win_length =handles.window_size.String;
    parameter.win_length = str2double(win_length);
    parameter.sum_length = 1;
    parameter.sum_type = 'mean';
    
    %---
    % Set up spectrogram parameter headers for selection table fields
    %---
    fft_headers = { ...
                    'Sample Rate (Hz)', ...
                    'FFT Size', ...
                    'Hop Size', ...
                    'Window Type', ...
                    'Window Parameter', ...
                    'Window Size' ...
                  };

    %---
    % Find available measurements
    %---
    measureParams = initialize_measurements;
    screenName = utils.extractfield(measureParams, 'screenName')';
    unit = utils.extractfield(measureParams, 'unit')';
    setName = utils.extractfield(measureParams, 'setName')';
    measure_handle = valid_identifier(screenName);
    measure_header = strcat(screenName, {' ['}, unit, ']');
    num_measurements = length(screenName);
    
    %---
    % Filter measures selected in GUI
    %---
    is_selected = zeros(num_measurements, 1);
    for i = 1:num_measurements
        is_selected(i) = handles.(measure_handle{i}).Value;
    end
    is_selected = logical(is_selected);
    measure_handle = measure_handle(is_selected);
    measure_header = measure_header(is_selected);
    setName = setName(is_selected);
    num_measurements_selected = sum(is_selected);

    if ~any(is_selected)
        utils.fail('No measurements are selected in GUI.','WARNING')
        delete(h);
        return;
    end
    
    %---
    % Warn user if number of selection tables does not match number of listfiles
    %---
    num_ST = length(fn_ST);
    num_listfiles = length(fn_listfile);
    if ~isequal(num_listfiles,num_ST) && ~isequal(num_listfiles,1)
       utils.fail('Number of listfiles does not equal the number of selection tables', 'WARNING')
        delete(h);
        return;
    end
    
    %---
    % If only one listfile is selected, apply to all selection tables
    %---
    flag_listfile = true;
    if ~isequal(num_listfiles,num_ST)
        if isequal(num_listfiles,1)
            fn_listfile(1:num_ST,1) = fn_listfile;
            flag_listfile = false;
        else
            utils.fail('Please use one listfile per selection table, or one listfile for all selection tables.','WARNING')
            delete(h);
            return;
        end        
    end
    
    %---
    % Check that file name roots for selection tables matches those of listfiles
    %---
    
% %     % find string before "." in each listfile name
% %     C_listfile_dot = regexp(fn_listfile,'[.]','split','once');
% %     fn_listfile_dot = cellfun(@(x) x{1},C_listfile_dot,'uniformoutput',false);
% %     
% %     % find string before date in each listfile name
% %     C_listfile_root = regexp(fn_listfile_dot,'\d\d\d\d\d\d\d\d','split','once');
% %     listfile_root = cellfun(@(x) x{1},C_listfile_root,'uniformoutput',false);
% %     
% %     % find string before "." in each selection table name
% %     C_ST_dot = regexp(fn_ST,'[.]','split','once');
% %     fn_ST_dot = cellfun(@(x) x{1},C_ST_dot,'uniformoutput',false);
% %     
% %     % find string before date in each selection table name
% %     C_ST_root = regexp(fn_ST_dot,'\d\d\d\d\d\d\d\d','split','once');
% %     ST_root = cellfun(@(x) x{1},C_ST_root,'uniformoutput',false);
    
% %     % stop execution if root name of selection table and listfile are not the same
% %     if ~all(strcmp(listfile_root,ST_root))
% %         t1 = 'File name roots of listfiles:';
% %         t2 = sprintf('   %s\n',listfile_root{:});
% %         t3 = 'do not match file name roots of selection tables';
% %         t4 = sprintf('   %s\n',ST_root{:});
% %         t = sprintf('%s\n',t1,t2,t3,t4);
% %         utils.fail(t,'WARNING')
% %         delete(h);
% %         return;
% %     end
    
    %---
    % Check that dates in selection tables names match those of listfiles
    %---
% %     date_listfiles = regexp(fn_listfile,'\d\d\d\d\d\d\d\d','match','once');
% %     if ~isempty(date_listfiles)
% %         date_ST = regexp(fn_ST,'\d\d\d\d\d\d\d\d','match','once');
% %         if ~all(strcmp(date_listfiles,date_ST))
% %             utils.fail('Dates in selection table names do not match dates in listfile names, or are in a different order','WARNING')
% %             delete(h);
% %             return;
% %         end
% %     end

    % initializations
    cmd = 'measure';
    clickTF = [];
    
    %---
    % for each selection table
    %---
    for i = 1:num_ST
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('Processing %s\n', fn_ST{i});
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if isequal(i,1) || flag_listfile
        
            %---
            % Read Raven listfile
            %---
            waitbar((i-1)/num_ST, h, sprintf('Reading %s', fn_listfile{i}))
            curr_fn_listfile_full = fullfile(listfile_path,fn_listfile{i});
            [filename,status] = utils.read_listfile(curr_fn_listfile_full);
            if status
                return;
            end

            % Test that all files specified in listfile exist
            idx = find(cellfun(@(x) ~exist(x,'file'),filename));
            if ~isempty(idx)
                if length(idx)>5
                    idx = idx(1:5);
                end
                badFn = filename(idx);
                utils.fail('Not all files listed in listfile exist','WARNING')
                fprintf(2,'  %s\n',badFn{:});
                delete(h);
                return;
            end

            %---
            % Read sound file header
            %---
            C = cellfun(@audioinfo,filename,'UniformOutput', false);
            if isempty(C)
                utils.fail('Sound file headers cannot be read.','WARNING')
                delete(h);
                return;
            end
            M = cell2mat(C);
            NumChannels = [M.NumChannels]';
            SampleRate = [M.SampleRate]';
            TotalSamples = [M.TotalSamples]';
            CumSamples = cumsum(TotalSamples);
            CumDuration = CumSamples ./ SampleRate;
            BeginDuration = [0 ; CumDuration];
            BeginDuration(end) = [];
            BitsPerSample = M.BitsPerSample;
            
            % Test that all sound files have the same number of channels
            if length(unique(NumChannels)) > 1
                utils.fail('Sound files do not all have the same number of channels.','WARNING')
                delete(h);
                return;
            else
                NumChannels = NumChannels(1);
            end
            
            % Test that all sound files have the same sample rate
            if length(unique(SampleRate)) > 1
                utils.fail('Sound files do not all have the same sample rate.','WARNING')
                delete(h);
                return;
            else
                SampleRate = SampleRate(1);
            end
        end
        
        % Parameters for making Osprey measurements
        params.sRate = SampleRate;
        params.frameRate = SampleRate / (parameter.hop * parameter.fft);
        params.binBW = SampleRate / parameter.fft;
% %         params.totalSamples = TotalSamples;
        params.hopSize = parameter.hop;
        params.zeroPad = 0;

        %---
        % Read Raven selection table
        %---
        waitbar((i-1)/num_ST, h, sprintf('Reading %s', fn_ST{i}))
        curr_fn_ST_full = fullfile(inpath,fn_ST{i});
        [headers,body] = utils.read_selection_table(curr_fn_ST_full);
        
        if ~isempty(body)
        
            %---
            % Stop execution if selection table already has measurement user wants to add
            %--
            already_measured = headers(ismember(headers,measure_header));
            if ~isempty(already_measured)
                t=sprintf('Selection table already has one or more selected measurements:\n  %s', curr_fn_ST_full);
                utils.fail(t,'WARNING')
                return;
            end

            %---
            % Find channel for each selection
            %---
            chan = str2double(body(:,strcmp(headers,'Channel')));

            %---
            % Find time bounds for each selection
            %---
            begin_time = str2double(body(:,strcmp(headers,'Begin Time (s)')));
            end_time = str2double(body(:,strcmp(headers,'End Time (s)')));
            time = [begin_time,end_time];
            if isempty(time) || ~isequal(size(time,2),2) || ~any(any(isfinite(time))) || (min(min(time)) < 0)
                t = sprintf('Selection time bounds are missing or invalid in:\n  %s', curr_fn_ST_full);
                utils.fail(t,'WARNING')
                return;
            end
            if any(end_time > CumDuration(end))
                utils.fail('One or more selections have time bounds beyond the end of recording.','WARNING')
                delete(h);
                return;
            end

            %---
            % find frequency bounds for all selections
            %---
            low_freq = str2double(body(:,strcmp(headers,'Low Freq (Hz)')));
            high_freq = str2double(body(:,strcmp(headers,'High Freq (Hz)')));
            freq = [low_freq,high_freq];
            if isempty(freq) || ~isequal(size(freq,2),2) || ~any(any(isfinite(freq))) || (min(min(freq)) < 0)
                t = sprintf('Selection frequency bounds are missing or invalid in:\n  %s', curr_fn_ST_full);
                utils.fail(t,'WARNING')
                return;
            end

            % for each selection
            waitbar((i-1)/num_ST, h, sprintf('Adding measurements to %s', fn_ST{i}))
            num_selections = size(body,1);
            value = cell(num_selections,num_measurements_selected);
            for j = 1:num_selections
                sel = [begin_time(j) low_freq(j) end_time(j) high_freq(j)];
                
                %if selection has some duration
                if (sel(3) - sel(1)) > 0
                    t0 = sel(1);
                    t1 = sel(3);
                    f0 = sel(2);
                    f1 = sel(4);
                    ch = chan(j);

                    data = parameter.fft;
                    hop = round(parameter.hop * data);
                    pad = 0;
                    srate = params.sRate;
                    fftSize = data + pad;
% %                     fRate   = srate / hop;
                    binSize = (srate / 2) / (fftSize / 2);
                    opNSamp = CumSamples(end);
                    nframe  = floor((opNSamp - data) / hop + 1);
                
                    %---
                    % Read sound clip
                    %---
                    BeginfileIdx = find(sel(1) >= BeginDuration, 1, 'last');
                    EndfileIdx = find(sel(3) < CumDuration, 1, 'first');
                    curr_filenames = filename(BeginfileIdx:EndfileIdx);
                    start = t0 - BeginDuration(BeginfileIdx);
                    finish = t1 - BeginDuration(BeginfileIdx);

                    frm0 = max(1,         floor((srate * start - (data - 1)/2) / hop + 3/2));
                    frm1 = min(nframe,    floor((srate * finish - (data - 1)/2) / hop + 3/2));

                    % Make sure they're in order.
                    frm1 = max(frm0, frm1);
                    
                    height	= (data + pad) / 2;		% number of cells in returned array
                    win     = utils.opWinTypeF_brp(data, parameter.win_type, parameter.win_param);	 	% window function; column vec

                    % Figure out which samples to get from the sound file: s0 is the starting
                    % sample number, n is the total number of samples.
                    s0 = round((frm0-1)*hop)+1;
                    s0 = max(s0,0);
                    n = data + (frm1 - frm0) * hop;
                    switch length(curr_filenames)
                        case 0
                            utils.fail('Selection bounds exceed sound duration', 'WARNING');
                            return;
                        case 1
                            s1 = min(s0 + n, TotalSamples(EndfileIdx));
                            y = audioread(curr_filenames{1}, [s0, s1]);
                        case 2
                            s1 = min(s0 + n - TotalSamples(BeginfileIdx), ...
                                     TotalSamples(EndfileIdx));
                            y = [audioread(curr_filenames{1}, [s0, inf]);
                                 audioread(curr_filenames{1}, [1, s1])];
                        otherwise
                            s1 = min(s0 + n - TotalSamples(BeginfileIdx:EndfileIdx-1), ...
                                     TotalSamples(EndfileIdx));
                            y = audioread(curr_filenames{1}, [s0, inf]);
                            for k = 2:len(curr_filenames)-1
                                y = [y ; audioread(curr_filenames{k})];
                            end
                            y = [y ; audioread(curr_filenames{end}, [1, s1])]; 
                    end
                    sams = y(:,ch);
                    sams = sams * (2^BitsPerSample / 2 );

                    %---
                    % Create spectrogram patch
                    %---
                    spect = zeros(height, frm1-frm0+1, 1);

                    padSeq = zeros(pad, 1);
                    scale = sqrt(data / 256);		% intensity scaling factor
                    win1 = repmat(win, 1, 1);
                    f = 0;
                    numSams = size(sams,1);
                    while f<=frm1-frm0 && f*hop+data<=numSams
                        x = sams(f*hop + 1 : f*hop + data, :);
                        if (pad > 0)
                            x = x - repmat(mean(x,1), utils.nRows(x), 1); 
                        end
                        res = fft([x.*win1; padSeq]) / scale;
                        spect(:,f+1,:) = res(1:height,:);
                        f = f + 1;
                    end
                    s = warning('off', 'MATLAB:log:logOfZero');	% prevent 'log of 0' warnings
                    spect = log(abs(spect));
                    warning(s)

                    % frequency selection from Osprey opCache > makeSpect
                    bin0 = max(1,         1 + floor(f0 / binSize + 1/2));
                    bin1 = min(fftSize/2, 1 +  ceil(f1 / binSize - 1/2));
                    spect = spect(bin0:bin1,:);

                    bx = [((frm0-1)*hop+(data-1)/2)/srate  (bin0-1)*binSize ...
                          ((frm1-1)*hop+(data-1)/2)/srate  (bin1-1)*binSize];
                               
                    for k = 1:num_measurements_selected
                        name = handles.(measure_handle{k}).String;
                        fun = str2func(setName{k});
                        try
                            v = fun(cmd, name, clickTF, sel, bx, spect, params);
                        catch ME
                            v = '';
                            fprintf(2,'\n\nMeasurement: %s\n', measure_header{k});
                            fprintf(2,'  %s\n',ME.message);
                            for idx = 1:min(2,length(ME.stack))
                                fprintf(2,'  %3.0f: %s\n', ME.stack(idx).line,ME.stack(idx).name);
                            end
                        end
                        try
                            value{j,k} = num2str(v,15);
                        catch ME
                            value{j,k} = '';
                            fprintf(2,'\n\nMeasurement: %s\n', measure_header{k});
                            fprintf(2,'  %s\n',ME.message);
                            for idx = 1:min(2,length(ME.stack))
                                fprintf(2,'  %3.0f: %s\n', ME.stack(idx).line,ME.stack(idx).name);
                            end
                        end
                    end
                end
            end % for each selection

            %---
            % Format spectrogram parameters to add to selection table
            %---
            fft_values = {  ...
                            sprintf('%.0f', SampleRate ), ...
                            fft_size, ...
                            hop_size, ...
                            win_type, ...
                            win_param, ...
                            win_length ...
                         };
            fft_values = repmat(fft_values,[num_selections 1]);

            %---
            % Add spectrogram parameters and new measurements to selection table body
            %---
            body = [body fft_values value];
        
        end % if ~isempty(body)
        
        %---
        % Add spectrogram parameters and new measurements to selection table headers
        %---
        headers = [headers fft_headers measure_header'];
        
        %---
        % Append body of selection table to headers
        %---
        if ~isempty(body)
            table = [headers ; body];
        else
            table = headers;
        end
            
        %---
        % Output new selection table with measurements
        %---
        C = strsplit(fn_ST{i}, '.');
        curr_fn = C{1};
        out_fn_full = fullfile(outpath, [curr_fn '.ACOUSTAT.selections.txt']);
            
        %---
        % Output new selection table with measurements
        %---
        if exist(out_fn_full, 'file')
            [fn, pth] = uiputfile(out_fn_full);
            if isequal(fn,0) % if canceled by user
                delete(h);
                return;
            end
            out_fn_full = fullfile(pth, fn);
        end
        fid = fopen(out_fn_full ,'w');
        for j = 1:size(table,1);
            fprintf(fid, '%s\t', table{j,1:end-1});
            fprintf(fid, '%s\n', table{j,end});
        end
        fclose(fid);
    end % for each selection table
    
    %---
    % Delete waitbar
    %---
    delete(h);
    
    %---
    % Inform user processing is complete
    %---
    fprintf('\nProcessing complete: %s\n', datestr(now));

% --- Retrieve saved paths
function load_paths(handles,paths)

    inpath = '';
    listfile_path = '';
    outpath = '';
    p = fileparts(mfilename('fullpath'));
    p_data_full = fullfile(p, 'data', 'data.mat');
    if exist(p_data_full, 'file')
        data = load(p_data_full);
        inpath = data.inpath;
        listfile_path = data.listfile_path;
        outpath = data.outpath;
    end
    if ~isdir(inpath)
        inpath = pwd;
    end
    if ~isdir(listfile_path)
        listfile_path = pwd;
    end
    if ~isdir(outpath)
        outpath = pwd;
    end

    % Set into GUI
    for i = 1:length(paths)
        eval(sprintf('set(handles.%s,''String'',%s)',paths{i},paths{i}));
    end
    
% --- Create measurements controls in GUI
function handles = make_controls(handles)

    %---
    % create controls
    %---    
    
    % find dimensions of measurements panel (in characters)
    par_h = handles.measurements_panel;
    par_pos = par_h.Position;
    
    % Find available measurements
    measureParams = initialize_measurements;
    measure_name = utils.extractfield(measureParams, 'longName');
    screenName = utils.extractfield(measureParams, 'screenName')';
    
    
    measure_handle = valid_identifier(screenName);
    
    % initializations
    num_measurements = length(measure_name);
    i = 0;
    x = 0.022;
    x_inc = 0.33;
% %     y0 = par_pos(4) + 0.42;
    y0 = par_pos(4) + 0.66;
    y = y0;
% %     y_inc = 0.05;
    y_inc = 0.065;
    
    % for each control
    while i < num_measurements
        
        %---
        % create control
        %---
        i = i + 1;
        handles.(measure_handle{i}) = uicontrol( ...
            par_h, ...
            'Style','checkbox', ...
            'String', measure_name{i}, ...
            'Tag', measure_handle{i}, ...
            'FontName', 'Segue UI', ...
            'FontSize', 8, ...
            'BackgroundColor', [0.881 0.855 0.834], ...
            'Units', 'normalized' ...
        );
           
        %---
        % set control position
        %---
        y = y - y_inc;
        if y < 0
            y = y0 - y_inc;
            x = x + x_inc;
        end
        pos = [ x, ...
                y, ...
                0.3, ...
                0.05];
        set(handles.(measure_handle{i}),'Position', pos)
    end
    
    
function resizeGUI(handles)
% Resize GUI for small monitors

    %initializations
    h = handles.AcoustatGUI;

    %move GUI to upper left
    movegui(h, 'northwest')

    %stop execution if GUI fits on screen
    ScreenSize = get(0, 'ScreenSize');
    if ScreenSize(4) > 900
        return;
    end

    %set units of all GUI elements to "normalized"
    
    %reduce GUI size so it fits onscreen
    pos = h.Position;
    ratio = ScreenSize(4) / pos(4);
    pos_new = floor(pos .* ratio);
    pos_new(4) = floor(pos_new(4) * 0.95);  % add space for Windows taskbar
    set(h, 'Position', pos_new);
    
    
%-------------------------------------------------------------------------
function measure_handle = valid_identifier(screenName)
% make valid MATLAB identifier from screenName

    if iscell(screenName) && isequal(length(screenName),1) && iscell(screenName{1})
        screenName = screenName{1};
    end
    tmp = matlab.lang.makeValidName(screenName);
    measure_handle = matlab.lang.makeUniqueStrings(tmp,{},namelengthmax);

%-------------------------------------------------------------------------
function measureParams = initialize_measurements
% make list of all available measurements

    %---
    % List names of measurement sets
    %---
    set_names = {'measures.mainSet'; 'measures.AcoustatSet'; 'measures.MLNSmeasures'};
    
    measureParams = [];
    
    for i = 1:length(set_names)
        set_name = set_names{i};
        [a,b,c]  = fileparts(set_name);
        setName = c(2:end);
        fun = str2func(set_name);
        params = fun('init');
        params = params(logical([params.enabled]));
        if ~isempty(params)
            for j = 1:length(params)
                params(j).setName = set_name;
                params(j).screenName = [sprintf('%s%d_%s',setName(1:3),j,params(j).screenName)];
            end
            measureParams = [measureParams, params];
        end
    end
