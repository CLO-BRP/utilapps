function varargout = MakeListfile(varargin)
% MAKELISTFILE MATLAB code for MakeListfile.fig
%      MAKELISTFILE, by itself, creates a new MAKELISTFILE or raises the existing
%      singleton*.
%
%      H = MAKELISTFILE returns the handle to a new MAKELISTFILE or the handle to
%      the existing singleton*.
%
%      MAKELISTFILE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAKELISTFILE.M with the given input arguments.
%
%      MAKELISTFILE('Property','Value',...) creates a new MAKELISTFILE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MakeListfile_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MakeListfile_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MakeListfile

% Last Modified by GUIDE v2.5 08-Jun-2015 10:28:01

% History
%   msp2   8 June 2015  Create
%   msp2  10 July 2015  Allow paths with spaces in them.

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MakeListfile_OpeningFcn, ...
                   'gui_OutputFcn',  @MakeListfile_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before MakeListfile is made visible.
function MakeListfile_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MakeListfile (see VARARGIN)

% %     %---
% %     % Add icon to GUI
% %     %--
% %     axes(handles.logo)
% %     [im, map] = imread('Black_Raven-icon.png', 'png');
% %     % [im, map, alpha] = imread('icon.ico','ico');
% %     imshow(im);
% %     axis off
% %     axis image

    %-----------------------------------------------------
    % Retreive last Input Path and Output File and display
    %-----------------------------------------------------

    %--
    % Retrieve saved paths
    %--
    [input_path, output_path] = load_paths;
    if ~isdir(input_path)
        input_path = pwd;
    end
    if ~isdir(output_path)
        output_path = pwd;
    end

    %--
    % Select new output path
    %--
    [ ~, dir_name ] = fileparts( input_path );
    fn_out = [ dir_name, '.listfile.txt' ];
    output_path_full = fullfile( output_path, fn_out );

    %--
    % Set into GUI
    %--
    set(handles.input_path, 'String', input_path)
    set(handles.output_path, 'String', output_path_full)

    % Choose default command line output for MakeListfile
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    % UIWAIT makes MakeListfile wait for user response (see UIRESUME)
    % uiwait(handles.MakeListfile);

    %Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))
    


% --- Executes when user attempts to close MakeListfile.
function MakeListfile_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to MakeListfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
     
    %-----------------------------------------------------
    % Save displayed input path and output path
    %-----------------------------------------------------

    %--
    % Get displayed paths
    %--
    try
        input_path = get(handles.input_path, 'String');
        output_path_full = get(handles.output_path, 'String');
        output_path = fileparts(output_path_full);

        %--
        % Saved displayed paths
        %--
        p = fileparts(mfilename('fullpath'));
        p_data = fullfile(p, 'data');
        if ~isdir(p_data)
            mkdir(p_data)
        end
        p_data_full = fullfile(p_data, 'data');
        save(p_data_full, 'input_path', 'output_path');
    catch
        %do nothing
    end

    %--
    % Close GUI
    %--
    delete(hObject);
    
    %Remove application root directory from search paths
    rmpath(fileparts(mfilename('fullpath')))
    

% --- Outputs from this function are returned to the command line.
function varargout = MakeListfile_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function input_path_Callback(hObject, eventdata, handles)
% hObject    handle to input_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of input_path as text
%        str2double(get(hObject,'String')) returns contents of input_path as a double

    %--
    % Get current input path
    %--
    input_path = get(handles.input_path, 'String');

    %--
    % If new path does not exist, give use options to fix
    %--
    if ~isdir(input_path)
        txt = 'Input path does not exist';
        button1 = 'Pick new path?';
        button2 = 'Retrieve last path?';
        choice = questdlg(txt, 'WARNING', button1, button2, button1);
        switch choice

            % Select new input path
            case button1
                input_path = uigetdir(input_path, 'Select directory tree with sound files');
                if isequal(input_path, 0)
                    return;
                end
                set(handles.input_path, 'String', input_path)

            % Retrieve last path
            case button2
                input_path = load_paths;
                if isdir(input_path)
                    set(handles.input_path, 'String', input_path)
                end
                return;

            % Cancel
            case ''
                return;
        end
    end

    %-------------------------------------------------
    % Reset name of listfile to match input directory
    %-------------------------------------------------

    %--
    % Get current output path
    %--
    output_path_full = get(handles.output_path, 'String');
    output_path = fileparts(output_path_full);

    %--
    % Select new output path
    %--
    [ ~, dir_name ] = fileparts( input_path );
    fn_out = [ dir_name, '.listfile.txt' ];
    output_path_full = fullfile( output_path, fn_out );
    [ fn_out, output_path ] = uiputfile( '*.txt', 'Select listfile name and path', output_path_full );
    if isequal( fn_out, 0 )
        return;
    end
    output_path_full = fullfile( output_path, fn_out );
    set(handles.output_path, 'String', output_path_full)

    %--
    % Save displayed paths
    %--
    p = fileparts(mfilename('fullpath'));
    p_data = fullfile(p, 'data');
    if ~isdir(p_data)
        mkdir(p_data)
    end
    p_data_full = fullfile(p_data, 'data');
    save(p_data_full, 'input_path', 'output_path');

% --- Executes during object creation, after setting all properties.
function input_path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to input_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function output_path_Callback(hObject, eventdata, handles)
% hObject    handle to output_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_path as text
%        str2double(get(hObject,'String')) returns contents of output_path as a double

    %--
    % Warn user if output file already exists
    %--
    output_path_full = get(handles.input_path, 'String');
    if exist(output_path_full, 'file')
        button = questdlg(sprintf('Listfile already exists\n\nReplace?\n'), 'MakeListFile');
        if strcmp(button, 'Yes')
            return;
        end
    end

% --- Executes during object creation, after setting all properties.
function output_path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% --- Executes on button press in browse_input.
function browse_input_Callback(hObject, eventdata, handles)
% hObject    handle to browse_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    %--
    % Get current input path
    %--
    input_path = get(handles.input_path, 'String');

    %--
    % Select new input path
    %--
    input_path = uigetdir(input_path, 'Select directory tree with sound files');
    if isequal(input_path, 0)
        return;
    end
    set(handles.input_path, 'String', input_path)

    %-------------------------------------------------
    % Reset name of listfile to match input directory
    %-------------------------------------------------

    %--
    % Get current output path
    %--
    output_path_full = get(handles.output_path, 'String');
    output_path = fileparts(output_path_full);

    %--
    % Select new output path
    %--
    [ ~, dir_name ] = fileparts( input_path );
    fn_out = [ dir_name, '.listfile.txt' ];
    output_path_full = fullfile( output_path, fn_out );
    [ fn_out, output_path ] = uiputfile( '*.txt', 'Select listfile name and path', output_path_full );
    if isequal( fn_out, 0 );
        return;
    end
    output_path_full = fullfile( output_path, fn_out );
    set(handles.output_path, 'String', output_path_full)

    %--
    % Save displayed paths
    %--
    p = fileparts(mfilename('fullpath'));
    p_data = fullfile(p, 'data');
    if ~isdir(p_data)
        mkdir(p_data)
    end
    p_data_full = fullfile(p_data, 'data');
    save(p_data_full, 'input_path', 'output_path');

% --- Executes on button press in browse_output_path.
function browse_output_path_Callback(hObject, eventdata, handles)
% hObject    handle to browse_output_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    %--
    % Get current input path
    %--
    input_path = get(handles.input_path, 'String');

    %--
    % Get current output path
    %--
    out_path_full = get(handles.output_path, 'String');
    output_path = fileparts(out_path_full);

    %--
    % Select new output path
    %--
    [ ~, dir_name ] = fileparts( input_path );
    fn_out = [ dir_name, '.listfile.txt' ];
    out_path_full = fullfile( output_path, fn_out );
    [ fn_out, output_path ] = uiputfile( '*.txt', 'Select listfile name and path', out_path_full );
    if isequal( fn_out, 0 );
        return;
    end
    out_path_full = fullfile( output_path, fn_out );
    set(handles.output_path, 'String', out_path_full)

    %--
    % Save displayed paths
    %--
    p = fileparts(mfilename('fullpath'));
    p_data = fullfile(p, 'data');
    if ~isdir(p_data)
        mkdir(p_data)
    end
    p_data_full = fullfile(p_data, 'data');
    save(p_data_full, 'input_path', 'output_path');

% --- Executes on button press in make_listfile.
function make_listfile_Callback(hObject, eventdata, handles)
% hObject    handle to make_listfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
%   Create Raven listfile with all aif files in directory tree

    %--
    % Retrieve input and output paths
    %--
    input_path = get(handles.input_path, 'String');
    output_path_full = get(handles.output_path, 'String');

    %--
    % Warn user if listfile already exists
    %--
    if exist(output_path_full, 'file')
        button = questdlg(sprintf('Listfile already exists\n\nReplace?\n'), 'MakeListFile');
        if ~strcmp(button, 'Yes')
            return;
        end
    end

    % set timer
    tic

    %--
    % Report that script is running
    %--
    disp(' ')
    disp('Making Listfile')
    h = helpdlg('Running', 'MakeListFile');
    % % movegui(h, 'center');

    % Change directory to user-selected directory, saving original directory
    path_orig = cd( input_path );

    % Ask Windows to write a text file with the full path of every aif file
    % in directory tree
    cmd = sprintf( 'dir *.aif /s/b/a-d | sort > "%s"', output_path_full );
    [ status, result ] = dos( cmd );

    % Close dialog box saying script is running
    delete(h)

    %--
    % Report result
    %--
    if status    
        % display in console
        disp( ' ' )
        fprintf( 2, '\nWARNING\n' );
        fprintf( 2, '%s\n', result );

        % display in dialog box
        h = warndlg(result, 'WARNING');
    % %     movegui(h, 'WARNING');
    else
        % display in console
        disp( ' ' )
        disp( 'Processing complete' )
        fprintf( '\nRun time: %.1f seconds\n', toc );

        % display in dialog box
        h = helpdlg( sprintf( '\nRun time: %.1f seconds\n', toc ), 'Processing complete');
    % %     movegui(h, 'center');    
    end

    % Return to original directory
    cd( path_orig );

% --- Retrieve saved paths
function [input_path, output_path] = load_paths

    input_path = '';
    output_path = '';
    p = fileparts(mfilename('fullpath'));
    p_data_full = fullfile(p, 'data', 'data.mat');
    if exist(p_data_full, 'file')
        data = load(p_data_full);
        input_path = data.input_path;
        output_path = data.output_path;
    end
