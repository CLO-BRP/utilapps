function table = trim_schedule(table)

% trim_schedule - remove time stamps that do not result in gaps
% -------------------------------------------------------------
%
% input:
% ------
%  table - time stamp table
%
% Output:
% -------
%  table - edited time stamp table

% Copyright (C) 2002-2014 Cornell University
%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% NOTE: if the time differences are equal in both domains, then there are
% no gaps

% History
%   msp2  16 Sep 2013
%     Allow for tolerance in time stamps produced by Databeast.  Without 
%     this modification, XBAT will fail to calculate time stamp tables when
%     Databeast is invoked with "Do not pad recordings with zeros" invoked.

if isempty(table)
	return;
end

tolerance   = 0.00001;     %maximum observed timestamp overlap so far is 0.00000763

ix = find( ( diff( table( :, 2 )) - diff( table( :, 1 ))) < tolerance );
% ix = find(diff(table(:, 1)) >= diff(table(:, 2)));

table(ix + 1,:) = [];
