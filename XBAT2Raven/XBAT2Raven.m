function varargout = XBAT2Raven(varargin)
% XBAT2RAVEN MATLAB code for XBAT2Raven.fig
%      XBAT2RAVEN, by itself, creates a new XBAT2RAVEN or raises the existing
%      singleton*.
%
%      H = XBAT2RAVEN returns the handle to a new XBAT2RAVEN or the handle to
%      the existing singleton*.
%
%      XBAT2RAVEN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in XBAT2RAVEN.M with the given input arguments.
%
%      XBAT2RAVEN('Property','Value',...) creates a new XBAT2RAVEN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before XBAT2Raven_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to XBAT2Raven_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help XBAT2Raven

% Last Modified by GUIDE v2.5 09-Dec-2015 13:26:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @XBAT2Raven_OpeningFcn, ...
                   'gui_OutputFcn',  @XBAT2Raven_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before XBAT2Raven is made visible.
function XBAT2Raven_OpeningFcn(hObject, eventdata, handles, varargin)

    % Retrieve saved input and outut paths
    load_paths(handles,{'inpath','outpath'});
    
    % Add "utilities" directory to path, if running from source code
    if ~isdeployed
        root = fileparts(mfilename('fullpath'));
        addpath(fullfile(root,'utilities'))
    end

    % Choose default command line output for XBAT2Raven
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = XBAT2Raven_OutputFcn(hObject, eventdata, handles)

    % Get default command line output from handles structure
    varargout{1} = handles.output;

% --- Executes when user attempts to close title.
function XBAT2Raven_GUI_CloseRequestFcn(hObject, eventdata, handles)

    delete(hObject);
    
    % Remove "utilities" directory from path, if running from source code
    if ~isdeployed
        root = fileparts(mfilename('fullpath'));
        rmpath(fullfile(root,'utilities'))
    end

function inpath_Callback(hObject, eventdata, handles)

    %--
    % Get current input path
    %--
    inpath = get(handles.inpath, 'String');
    if ~isdir(inpath)
        inpath = pwd;
    end

    %--
    % Select new input path
    %--
    inpath = uigetdir(inpath, 'Select folder with selection tables');
    if isequal(inpath, 0)
        return;
    end
    set(handles.inpath, 'String', inpath)

    %--
    % Save input and output path
    %--
    save_paths(handles);


% --- Executes during object creation, after setting all properties.
function inpath_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on button press in browse_input.
function browse_input_Callback(hObject, eventdata, handles)

    %--
    % Get current input path
    %--
    inpath = get(handles.inpath, 'String');
    if ~isdir(inpath)
        inpath = pwd;
    end

    %--
    % Select new input path
    %--
    inpath = uigetdir(inpath, 'Select folder with selection tables');
    if isequal(inpath, 0)
        return;
    end
    set(handles.inpath, 'String', inpath)

    %--
    % Save input and output path
    %--
    save_paths(handles);

function outpath_Callback(hObject, eventdata, handles)

    %--
    % Get current outpath path
    %--
    outpath = get(handles.outpath, 'String');
    if ~isdir(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder to output selection tables with measurements');
    if isequal(outpath, 0)
        return;
    end
    set(handles.outpath, 'String', outpath)

    %--
    % Save input and output path
    %--
    save_paths(handles);


% --- Executes during object creation, after setting all properties.
function outpath_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on button press in browse_output.
function browse_output_Callback(hObject, eventdata, handles)

    %--
    % Get current outpath path
    %--
    outpath = get(handles.outpath, 'String');
    if ~isdir(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder to output selection tables with measurements');
    if isequal(outpath, 0)
        return;
    end
    set(handles.outpath, 'String', outpath)

    %--
    % Save input and output path
    %--
    save_paths(handles);

% --- Retrieve saved paths
function load_paths(handles,paths)

    inpath = '';
    outpath = '';
    p = fileparts(mfilename('fullpath'));
    p_data_full = fullfile(p, 'data', 'data.mat');
    if exist(p_data_full, 'file')
        data = load(p_data_full);
        inpath = data.inpath;
        outpath = data.outpath;
    end
    if ~isdir(inpath)
        inpath = pwd;
    end
    if ~isdir(outpath)
        outpath = pwd;
    end

    % Set into GUI
    for i = 1:length(paths)
        eval(sprintf('set(handles.%s,''String'',%s)',paths{i},paths{i}));
    end

% --- Save displayed input path and output path
function save_paths(handles)

    %--
    % Get displayed paths
    %--
    try
        inpath = get(handles.inpath, 'String');
        outpath = get(handles.outpath, 'String');

        %--
        % Saved displayed paths
        %--
        p = fileparts(mfilename('fullpath'));
        p_data = fullfile(p, 'data');
        if ~isdir(p_data)
            mkdir(p_data)
        end
        p_data_full = fullfile(p_data, 'data');
        save(p_data_full, 'inpath', 'outpath');
    catch
        %do nothing
    end


% --- Executes on button press in run.
function run_Callback(hObject, eventdata, handles)

    % Retrieve paths from GUI
    inpath = get(handles.inpath, 'String');
    outpath = get(handles.outpath, 'String');
          
    % Initialize waitbar (make 500 pixels wider than default)
    h = waitbar(0,'Reading GUI');
    pos = get(h,'Position');
    pos(3) = pos(3) + 400;
    set(h, 'Position', pos)    
    hChild = get(h, 'Children');
    posWait = get(hChild,'Position');
    posWait(3) = posWait(3) + 400;
    set(hChild, 'Position', posWait);
    movegui(h, 'center')
    
    %---
    % Find names of logs
    %---
    waitbar(0, h, 'Reading XBAT log names')
    if isdir(inpath)
        search_path = fullfile(inpath,'*.mat');
        listing = dir(search_path);
        listing = listing(~[listing.isdir]);
        C = struct2cell(listing);
        fn_log = C(1,:)';
        if isempty(fn_log)
            t = sprintf('No XBAT logs found in\n  %s',inpath);
            fail(t,'WARNING')
            delete(h);
            return;
        end
    else
        t = sprintf('Path not found\n  %s',inpath);
        fail(t,'WARNING')
        delete(h);
        return;
    end
    num_logs = length(fn_log);
    
    %---
    % for each logs
    %---
    for i = 1:num_logs
       
        %---
        % export XBAT log as Raven selection table
        %---
        fn_log_curr = fn_log{i};
        waitbar((i-1)/num_logs, h, sprintf('Exporting %s', fn_log_curr))
        fn_log_full = fullfile(inpath, fn_log_curr);
        
        XBAT2Raven_run(fn_log_full, outpath);
% %         XBAT2Raven_run_Kaitlin(fn_log_full, outpath); %SNR measurement
    end

    % let user know processing is done
    waitbar(1, h, 'Processing complete.')
